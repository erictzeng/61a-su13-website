### SOLUTIONS ###

# A "simple" adventure game.

# README:
# http://www-inst.eecs.berkeley.edu/~cs61a/su13/lab/lab06a/lab06a.txt

place = None
inventory = []

def adventure():
    while True:
        print(HR) # For neatness
        check_win_conditions()
        try: # In case of errors... catch them!
            place.describe()
            line = input('adventure> ')
            exp = adv_parse(line)
            output = adv_eval(exp)
            print(output)
        except (KeyboardInterrupt, EOFError, SystemExit): # If you ctrl-c or ctrl-d
            print('Bye!')
            return
        # If the player input was badly formed or if something doesn't exist
        except (SyntaxError, NameError, KeyError, TypeError, IndexError) as e:
            print('ERROR:', e)


def adv_parse(line):
    """ Parses adventure.py commands

    Handles things differently when
    the first part of the string is 'give'
    or 'ask'

    >>> adv_parse('look')
    ('look', '')
    >>> adv_parse('take rubber ducky')
    ('take', 'rubber ducky')
    >>> adv_parse('give rubber ducky to Andrew')
    ('give', 'rubber ducky', 'Andrew')
    >>> adv_parse('ask Andrew for advice about the midterm')
    ('ask', 'Andrew', 'advice about the midterm')
    """
    tokens = line.split()
    if not tokens: # empty list
        raise SyntaxError('No command given')
    token = tokens.pop(0)
    if token == 'give':
        """ YOUR CODE HERE """
        return (token, ' '.join(tokens[0:-2]), tokens[-1])
    elif token == 'ask':
        """ YOUR CODE HERE """
        return (token, tokens[0], ' '.join(tokens[2:]))
    else: # Only split out the operator, the rest must be the operand
        """ YOUR CODE HERE """
        return (token, ' '.join(tokens))


def adv_eval(exp):
    if is_operator(exp) or is_person(exp): # Use the underlying eval
        return eval(exp)                   # to find the actual function
    elif type(exp) != tuple:
        return exp
    elif exp[0] == 'give':
        """ YOUR CODE HERE """
        return give(adv_eval(exp[2]), exp[1])
    elif exp[0] == 'ask':
        """ YOUR CODE HERE """
        return ask(adv_eval(exp[1]), exp[2])
    else:
        """ YOUR CODE HERE """
        return adv_apply(adv_eval(exp[0]), adv_eval(exp[1]))

def adv_apply(operator, operand):
    """ YOUR CODE HERE """
    return operator(operand)

def is_operator(exp):
    return exp in ('look', 'go', 'take', 'give', 'ask',
                   'examine', 'help', 'stuff')

def is_person(exp):
    return exp in ('Andrew',)

def check_win_conditions():
    global place
    if place == Victory and Exam not in inventory:
        print("Hold up, you haven't taken the exam!")
        print('Returning you to the exam room...')
        place = LiKaShing
        if place == Victory and Exam in inventory:
            print("You've bested the midterm and won the game! w00t ;D")
            exit()
 
# We shouldn't define the following functions in the global frame.
# There are better ways of doing this, but for simplicity's sake,
# this is what I'm going with.

def stuff(_):
    print('You look through your stuff... you see')
    for e in inventory:
        print(e.name + ' - ' + e.description)
    return "That's all!"

def look(_):
    return place.look()

def go(direction):
    global place
    place, output = place.take_exit(direction)
    return output

def take(thing):
    obj = place.take(thing)
    if obj:
        inventory.append(obj)
        return 'You take the ' + thing
    return 'No such thing to take!'

def give(person, thing):
    if type(person) != Person:
        return "Who is {}?!".format(person)
    if person not in place.people:
        return person.name + ' not here!'
    obj = None
    for i, e in enumerate(inventory):
        if thing in e.name:
            obj = inventory.pop(i)
    if obj:
        return person.give(obj)
    return "You don't have this in your possession!"

def ask(person, message):
    if type(person) != Person:
        return "Who is {}?!".format(person)
    return person.ask(message)

def examine(thing):
    for item in inventory:
        if thing in item.name:
            return item.examine()
    return "You don't have this in your possession!"

def help(_):
    return \
"""There are 7 possible operators:
  * look
  * go [direction]
  * take [thing]
  * give [thing] to [person]
  * examine [thing in inventory]
  * ask [person] for [message]
  * help
  * stuff"""


# OOP representation of the game models:
class Person(object):
    def __init__(self, name, *things):
        self.name = name
        self.inventory = list(things)
        self.brain = lambda self, msg: self.name + ' remains silent'

    def give(self, thing):
        self.inventory.append(thing)
        return self.name + ' takes the ' + thing.name

    def ask(self, msg):
        return self.brain(self, msg)


class Location(object):
    def __init__(self, name, description, *people_and_things):
        self.name = name
        self.description = description
        self.people = []
        self.things = []
        self.exits = {}
        for obj in people_and_things:
            if type(obj) == Person:
                self.people.append(obj)
            elif type(obj) == Thing:
                self.things.append(obj)

    def describe(self):
        print(self.description)
        if self.exits:
            print('\nExits:')
            for k, v in self.exits.items():
                print('  ', k, '-', v[0].name)
        print()

    def look(self):
        result = 'You take a look around and see:\n'
        if not (self.people or self.things):
            result += 'Nothing!'
        result += '\n'.join([thing.name + ' - ' + thing.description for thing in self.things])
        result += '\n'.join([person.name for person in self.people])
        return result

    def take_exit(self, exit):
        if exit in self.exits:
            return self.exits[exit]
        return self, "Where do you think you're going?!"

    def take(self, thing):
        for i, e in enumerate(self.things):
            if thing in e.name:
                return self.things.pop(i)
            

class Thing(object):
    def __init__(self, name, description):
        self.name = name
        self.description = description

    def examine(self):
        return self.description
  



# Don't worry about anything under this line
# ==========================================
# Game Data:

HR = "\n****************************************\n"

RubberDucky = Thing('rubber ducky', "Hm. It's yellow and it's rubber and it squeaks. Fascinating.")

# Sssshhh, Easter egg...
real_input = input
def input(prompt):
    result = real_input(prompt)
    if result == 'xyzzy':
        print("Secret win! You've won the game!")
        print("Now if only life had a cheatcode...")
        exit()
    return result

HearstAve = Location('Hearst Ave.',
                     'You find yourself on the sidewalk of Hearst Ave.')

EuclidAve = Location('Euclid Ave.',
                     "You find yourself on the corner of Euclid Ave. It's not "
                       + "as noisy here!")

def andrews_brain(self, msg):
    """ A dispatch function for Andrew.

        Andrew becomes aware of self
        if you set his brain attribute accordingly
        (which is why you can use self outside of
         a class; probably not the best thing to do)
        ;)
    """
    if self.inventory.count(RubberDucky) != 2:
        return 'I miss my rubber duckies. :('
    elif self.inventory.count(RubberDucky) == 2:
        print('Andrew tells you all the secrets to 61A...')
        LiKaShing.exits['to victory'] = (Victory, 'Victory')
        LiKaShing.description = 'You arrive at the auditorium where ' +\
            'lecture is held. The room is filled with students about to ' +\
            'start the midterm!'
        return 'The path is clear for you now. You are Enlightened Go off and ace your midterm!'

Andrew = Person('Andrew')
Andrew.brain = andrews_brain

AndrewsHouse = Location("Andrew's House.",
                        'You find yourself on the steps to a small apartment.',
                        Andrew)

Campus = Location('UC Berkeley Campus',
                  'You stumble onto the main campus. There are students going'
                    + ' to and from class. You better get going too!',
                  RubberDucky)

Exam = Thing('CS 61A Midterm II', 'A 10 page bundle of paper with recursion in Scheme, OOP, environment diagrams, etc...')
LiKaShing = Location('Li Ka Shing',
                     'You arrive at the auditorium where lecture is held. The '
                       + 'room is empty.', RubberDucky, Exam)

soda_description = \
"""You arrive in 271 Soda. The room is bright
and miserable as always. A few 61A students are
scattered among the lab computers, working
on the latest project."""

SodaHall = Location('271 Soda', soda_description, RubberDucky)
place = SodaHall

SodaHall.exits['out'] = (HearstAve, 'You leave 2nd floor Soda through the exit all the cool kids use.')
HearstAve.exits['north'] = (SodaHall, 'You re-enter Soda Hall. Uggggggg')
HearstAve.exits['west'] = (EuclidAve, 'You go down the hill toward North Gate.')
HearstAve.exits['south'] = (Campus, 'You hop across the street')

EuclidAve.exits['north'] = (AndrewsHouse, 'You head up Euclid...')
EuclidAve.exits['east'] = (HearstAve, 'You go up the hill along Hearst')
EuclidAve.exits['south'] = (Campus, 'You hop across the street')

Campus.exits['north'] = (EuclidAve, 'You walk up to North Gate and end up back at the edge of campus.')
Campus.exits['west'] = (LiKaShing, 'You go down the leafy path toward West side')

LiKaShing.exits['out'] = (LiKaShing, "You can check out any time you'd like, but you can NEVER leave")

Victory = Location('Victory', 'YAAAAAAAYYYY!')

AndrewsHouse.exits['south'] = (EuclidAve, 'You head back down Euclid.')

# This is what @main does (sorta)
if __name__ == '__main__':
    try:
        import readline
    except ImportError:
        pass

    motd = """
Welcome to the micro-world of CS 61A! You have a
midterm tomorrow, and in hopes of doing well, you
quest to seek the TA, Andrew, a mythical creature who
is rumored to hold the key to besting the test."""
    print(motd)
    adventure()
