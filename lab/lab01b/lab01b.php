
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <meta name="description" content ="CS61A: Structure and Interpretation of Computer Programs" />
    <meta name="keywords" content ="CS61A, Computer Science, CS, 61A, Programming, Berkeley, EECS" />
    <meta name="author" content ="Steven Tang, Eric Tzeng, Albert Wu, Mark Miyashita, Robert Huang, Andrew Huang, Brian Hou,
                                  Leonard Truong, Jeffrey Lu, Rohan Chitnis" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">@import url("../lab_style.css");</style>
    <style type="text/css">@import url("../61a_style.css");</style>

    <title>CS 61A Summer 2013: Lab 1b</title>

    <?php
    /* So all of the PHP in this file is to allow for this nice little trick to
     * help us avoid having two versions of the questions lying around in the
     * repository, which often leads to the two versions going out of sync which
     * leads to annoyance for students.
     *
     * The idea's pretty simple for the PHP part, just simply have two dates:
     *
     *    1. The current date
     *    2. The date the solutions should be released
     *
     * Using these, we now wrap our solutions in a simple PHP if statement that
     * checks if the date is past the release date and only includes the code on
     * the page displayed (what the server gives back to the browser) if the
     * solutions are supposed to be released.
     *
     * We also use some PHP to create unique IDs for each of the show/hide
     * buttons and solution divs, which are then used in the PHP generated
     * jQuery code that we use to create the nice toggling effect.
     *
     * I apologize if the PHP/jQuery is really offensively bad, this is
     * literally the most I've written of either for a single project so far.
     * Comments/suggestions are most welcome!
     *
     * - Tom Magrino (tmagrino@berkeley.edu)
     */
    $BERKELEY_TZ = new DateTimeZone("America/Los_Angeles");
    $RELEASE_DATE = new DateTime("06/27/2013", $BERKELEY_TZ);
    $CUR_DATE = new DateTime("now", $BERKELEY_TZ);
    $q_num = 0; // Used to make unique ids for all solutions and buttons
    ?>
  </head>

  <body style="font-family: Georgia,serif;">

<h1>CS61A Lab 1b: Higher Order Functions</h1>
<h3>Week 1b, Summer 2013</h3>

<h3 class="section_title">Exercise 1: Higher Order Functions</h3>

<p>Higher order functions are functions that take a function as an input, and/or
output a function. We will be exploring many applications of higher order functions.
For each question, try to determine what Python
would print. Then check in the interactive interpreter to see if you got the
right answer.</p>

<pre class="codemargin">
&gt;&gt;&gt; def square(x):
...     return x*x

&gt;&gt;&gt; def neg(f, x):
...     return -f(x)

# Q1
&gt;&gt;&gt; neg(square, 4)
_______________
&gt;&gt;&gt; def first(x):
...	    x += 8
...	    def second(y):
...	        print('second')
...	        return x + y
...	    print('first')
...	    return second
...
# Q2
&gt;&gt;&gt; f = first(15)
_______________
# Q3
&gt;&gt;&gt; f(16)
_______________

&gt;&gt;&gt; def foo(x):
...	    def bar(y):
...	        return x + y
...	    return bar

&gt;&gt;&gt; boom = foo(23)
# Q4
&gt;&gt;&gt; boom(42)
_______________
# Q5
&gt;&gt;&gt; foo(6)(7)
_______________

&gt;&gt;&gt; func = boom
# Q6
&gt;&gt;&gt; func is boom
_______________
&gt;&gt;&gt; func = foo(23)
# Q7
&gt;&gt;&gt; func is boom
_______________
&gt;&gt;&gt; def troy():
...     abed = 0
...     while abed &lt; 10:
...         def britta():
...             return abed
...         abed += 1
...     abed = 20
...     return britta
...
&gt;&gt;&gt; annie = troy()
&gt;&gt;&gt; def shirley():
...     return annie
&gt;&gt;&gt; pierce = shirley()
# Q8
&gt;&gt;&gt; pierce()
________________
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
	  <pre class="codemargin">
We recommend you try typing these statements into the interpreter.

1) -16
2) first
3) second
   39
4) 65
5) 13
6) True
7) False
8) 20
	</pre>
	  </p>
    </div>
    <?php } ?>

<h3 class="section_title">Exercise 2: Returning Functions</h3>

<p> In your Hog project, you will have to work with strategies and functions that
make strategies. Let's start by defining what a strategy is: a function that
takes two arguments (your score and the opponent's score) and returns the number
of dice to roll. The following is a strategy function that always rolls
<code>5</code> dice, and is the computer's default strategy:</p>

<pre class="codemargin">
def default_strategy(score, op_score):
    return 5
</pre>

<p> A strategy maker is a function that defines a strategy within its body, and
returns the resulting strategy. We'll define a strategy maker that returns the
default strategy:
</p>

<pre class="codemargin">
def make_default_strategy():
    def default_strategy(score, op_score):
        return 5
    return default_strategy
</pre>

<p> Of course, a strategy that doesn't adapt to the situation is not a strategy at all!
Implement a strategy maker called <span class="code">make_weird_strategy</span>
that will take one argument called <span class="code">num_rolls</span>. The strategy it returns
will return the higher of <span class="code">num_rolls</span> or the total number of points
scored in the game thus far divided by <code>20</code>, throwing away any remainder.</p>

<pre class="codemargin">
def make_weird_strategy(num_rolls):
    "*** YOUR CODE HERE ***"
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
     <p>
	  <pre class="codemargin">
def make_weird_strategy(num_rolls):
    def weird_strategy(score, op_score):
        return max(num_rolls, (score+op_score)//20)
    return weird_strategy
	</pre>
	  </p>
    </div>
    <?php } ?>

<p> Obviously, this isn't a practical strategy, but exemplifies how a strategy maker
is written. How can you write a better strategy maker? In your project, consider the
rules of Hog as well as the situations in which it may be beneficial to roll more or
less dice.</p>

<h3 class="section_title">Exercise 3: I Heard You Liked Functions So I Put Functions In Your Functions </h3>

<p>Define a function <span class='code'>cycle</span> which takes in
three functions as arguments: <span class='code'>f1</span>, <span
class='code'>f2</span>, <span class='code'>f3</span>. <span
class='code'>cycle</span> will then return another function. The
returned function should take in an integer argument <span
class='code'>n</span> and do the following:

<ol>
  <li>Return a function that takes in an argument <span class='code'>x</span> and does the following:
  <ol>
    <li>if <span class='code'>n</span> is 0, just return <span class='code'>x</span></li>
    <li>if <span class='code'>n</span> is 1, apply the first function that is passed to <span class='code'>cycle</span> to <span class='code'>x</span>
    <li>if <span class='code'>n</span> is 2, the first function passed to <span class='code'>cycle</span> is applied to <span class='code'>x</span>, and then the second function passed to <span class='code'>cycle</span> is applied to the result of that (i.e. <span class='code'>f2(f1(x))</span>)
    <li>if <span class='code'>n</span> is 3, apply the first, then the second, then the third function (i.e. <span class='code'>f3(f2(f1(x)))</span>)
    <li>if <span class='code'>n</span> is 4, apply the first, then the second, then the third, then the first function (i.e. <span class='code'>f1(f3(f2(f1(x))))</span>)
    <li>And so forth</li>
  </ol>
</ol>
<i>Hint</i>: most of the work goes inside the most nested function.

<pre class="codemargin">
def cycle(f1, f2, f3):
    """ Returns a function that is itself a higher order function
    &gt;&gt;&gt; def add1(x):
    ...     return x + 1
    ...
    &gt;&gt;&gt; def times2(x):
    ...     return x * 2
    ...
    &gt;&gt;&gt; def add3(x):
    ...     return x + 3
    ...
    &gt;&gt;&gt; my_cycle = cycle(add1, times2, add3)
    &gt;&gt;&gt; identity = my_cycle(0)
    &gt;&gt;&gt; identity(5)
    5
    &gt;&gt;&gt; add_one_then_double = my_cycle(2)
    &gt;&gt;&gt; add_one_then_double(1)
    4
    &gt;&gt;&gt; do_all_functions = my_cycle(3)
    &gt;&gt;&gt; do_all_functions(2)
    9
    &gt;&gt;&gt; do_more_than_a_cycle = my_cycle(4)
    &gt;&gt;&gt; do_more_than_a_cycle(2)
    10
    &gt;&gt;&gt; do_two_cycles = my_cycle(6)
    &gt;&gt;&gt; do_two_cycles(1)
    19
    """

    "*** YOUR CODE HERE ***"
</pre>

	<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
<pre>

def cycle(f1, f2, f3):
     def ret_fn(n):
         def ret(x):
             i = 0
             while i &lt; n:
                 if i % 3 == 0:
                     x = f1(x)
                 elif i % 3 == 1:
                     x = f2(x)
                 else:
                     x = f3(x)
                 i += 1
             return x
         return ret
     return ret_fn
</pre>
</div>
<?php } ?>

<h3 class="section_title">Exercise 4: Environment Diagrams</h3>

<p>Try drawing environment diagrams for the following examples and predicting
what Python will output: </p>

<pre class="codemargin">
# Q1
def square(x):
    return x * x

def double(x):
    return x + x

a = square(double(4))


# Q2
x, y = 4, 3

def reassign(arg1, arg2):
    x = arg1
    y = arg2

reassign(5, 6)


# Q3
def f(x):
    f(x)

print, f = f, print
a = f(4)
b = print(4)

# Q4
def adder_maker(x):
    def adder(y):
        return x + y
    return adder

add3 = adder_maker(3)
add3(4)
sub5 = adder_maker(-5)
sub5(6)
sub5(10) == add3(2)


<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
      <pre class="codemargin">
Q1:
<iframe width="800" height="500" frameborder="0" src="http://pythontutor.com/iframe-embed.html#code=def+square(x)%3A%0A++++return+x+*+x%0A%0Adef+double(x)%3A%0A++++return+x+%2B+x%0A%0Aa+%3D+square(double(4))&cumulative=true&heapPrimitives=false&drawParentPointers=false&textReferences=false&showOnlyOutputs=false&py=3&curInstr=0"> </iframe>

Q2:
<iframe width="800" height="500" frameborder="0" src="http://pythontutor.com/iframe-embed.html#code=x,+y+%3D+4,+3%0A%0Adef+reassign(arg1,+arg2)%3A%0A++++x+%3D+arg1%0A++++y+%3D+arg2%0A%0Areassign(5,+6)&cumulative=true&heapPrimitives=false&drawParentPointers=false&textReferences=false&showOnlyOutputs=false&py=3&curInstr=0"> </iframe>

Q3:
<iframe width="800" height="500" frameborder="0" src="http://pythontutor.com/iframe-embed.html#code=def+f(x)%3A%0A++++f(x)%0A++++%0Aprint,+f+%3D+f,+print%0A%0Aa+%3D+f(4)%0Ab+%3D+print(4)&cumulative=true&heapPrimitives=false&drawParentPointers=false&textReferences=false&showOnlyOutputs=false&py=3&curInstr=0"> </iframe>

Q4:
<iframe width="800" height="500" frameborder="0" src="http://pythontutor.com/iframe-embed.html#code=def+adder_maker(x)%3A%0A++++def+adder(y)%3A%0A++++++++return+x+%2B+y%0A++++return+adder%0A%0Aadd3+%3D+adder_maker(3)%0Aadd3(4)%0Asub5+%3D+adder_maker(-5)%0Asub5(6)%0Asub5(10)+%3D%3D+add3(2)&cumulative=true&heapPrimitives=false&drawParentPointers=false&textReferences=false&showOnlyOutputs=false&py=3&curInstr=0"> </iframe>
    </pre>
      </p>
    </div>
    <?php } ?>

<p> Fin. </p>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script>
  <?php for ($i = 0; $i < $q_num; $i++) { ?>
  $("#toggleButton<?php echo $i; ?>").click(function () {
  $("#toggleText<?php echo $i; ?>").toggle();
  });
  <?php } ?>
</script>
<?php } ?>

  </body>
</html>
