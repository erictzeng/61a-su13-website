
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <meta name="description" content ="CS61A: Structure and Interpretation of Computer Programs" />
    <meta name="keywords" content ="CS61A, Computer Science, CS, 61A, Programming, Berkeley, EECS" />
    <meta name="author" content ="Steven Tang, Eric Tzeng, Albert Wu, Mark Miyashita, Robert Huang, Andrew Huang, Brian Hou,
                                  Leonard Truong, Jeffrey Lu, Rohan Chitnis" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">@import url("../lab_style.css");</style>
    <style type="text/css">@import url("../61a_style.css");</style>

    <title>CS 61A Summer 2013: Lab 2a</title>

    <?php
    /* So all of the PHP in this file is to allow for this nice little trick to
     * help us avoid having two versions of the questions lying around in the
     * repository, which often leads to the two versions going out of sync which
     * leads to annoyance for students.
     *
     * The idea's pretty simple for the PHP part, just simply have two dates:
     *
     *    1. The current date
     *    2. The date the solutions should be released
     *
     * Using these, we now wrap our solutions in a simple PHP if statement that
     * checks if the date is past the release date and only includes the code on
     * the page displayed (what the server gives back to the browser) if the
     * solutions are supposed to be released.
     *
     * We also use some PHP to create unique IDs for each of the show/hide
     * buttons and solution divs, which are then used in the PHP generated
     * jQuery code that we use to create the nice toggling effect.
     *
     * I apologize if the PHP/jQuery is really offensively bad, this is
     * literally the most I've written of either for a single project so far.
     * Comments/suggestions are most welcome!
     *
     * - Tom Magrino (tmagrino@berkeley.edu)
     */
    $BERKELEY_TZ = new DateTimeZone("America/Los_Angeles");
    $RELEASE_DATE = new DateTime("07/02/2013", $BERKELEY_TZ);
    $CUR_DATE = new DateTime("now", $BERKELEY_TZ);
    $q_num = 0; // Used to make unique ids for all solutions and buttons
    ?>
  </head>

  <body style="font-family: Georgia,serif;">

<h1>CS61A Lab 2a: Lambdas</h1>
<h2>Week 2a, Summer 2013</h2>

<h3 class="section_title">Exercise 1: Understanding Lambdas</h3>

<p>Now, we will visit the idea of <i>lambda</i> functions. These are functions that are one line, and that 
line just specifies the return value. The format of a lambda function is as follows:</p>

<pre class="codemargin">
lambda [parameters]: [return value]
</pre>

<p> One of the differences between using the def keyword and lambda expressions which
we would like to point out is that def is a <i>statement</i>, while lambda is an <i>expression</i>.
Evaluating a def statement will have a side effect; namely, it creates a new function binding
in the current environment. On the other hand, evaluating a lambda expression will not
change the environment unless we do something with this expression. For instance, we could
assign it to a variable or pass it in as a function argument. </p>

<p> Another bit of syntax that we introduce in this exercise is the conditional expression. It has the following
format:</p>

<pre class="codemargin">
[true-value] if [conditional] else [false-value]
</pre>

<p> It is almost equivalent to the if statement that you have already seen. If the conditional evaluates to True
then the entire expression evaluates to [true-value]. If the condition evaluates to false, then the entire expression
evaluates to [false-value].</p>

<p> Knowing this, fill in the blanks as to what Python would do here:</p>

<pre class="codemargin">
>>> doctor = lambda : "who"
# Q1
>>> doctor()
_______________
>>> ninth = lambda x: "Fantastic!" if x == 9 else tenth
>>> tenth = lambda y: "Allons-y!" if y == 10 else eleventh
>>> eleventh = lambda z: "Geronimo!" if z == 11 else ninth
# Q2
>>> ninth(9)
_______________
# Q3
>>> ninth(2)(10)
_______________
# Q4
>>> tenth(10)
_______________
# Q5
>>> tenth(12) is eleventh
_______________
# Q6
>>> eleventh(10)(11)(9)(11)
_______________
# Q7: Troy revisited, but with lambdas!
>>> def Troy():
...  abed = 0
...  while abed < 10:
...    britta = lambda: abed
...    abed += 1
...  abed = 20
...  return britta
...
>>> jeff = Troy()
>>> shirley = lambda : jeff
>>> pierce = shirley()
>>> pierce()
________________
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
	  <pre class="codemargin">
          1) "who"
          2) "Fantastic!"
          3) "Allons-y!"
          4) "Allons-y!"
          5) True
          6) "Geronimo!"
	  7) 20 (Note that this is the same answer as before; there are multiple ways to write the same function in computer science.)
	  </pre>
	  </p>
    </div>
    <?php } ?>


<h3 class="section_title">Exercise 2: Lambda'ed Strategies</h3>

<p> Recall the following function definitions from Lab 1b. The default strategy always returns 5:</p>

<pre class="codemargin">
def default_strategy(score, op_score):
    return 5
</pre>

<p> A strategy maker is a function that defines a strategy within its body, and
returns the resulting strategy. We had defined a strategy maker that returns the
default strategy:</p>

<pre class="codemargin">
def make_default_strategy():
    def default_strategy(score, op_score):
        return 5
    return default_strategy
</pre>

<p> You then implemented make_weird_strategy, another strategy maker:</p>

<pre class="codemargin">
def make_weird_strategy(num_rolls):
    def weird_strategy(score, op_score):
        return max(num_rolls, (score+op_score)//20)
    return weird_strategy
</pre>

<p> As an exercise, implement all 3 of these functions in one line each, using lambda expressions.</p>
<pre class="codemargin">          
>>> default_strategy = ________________
>>> make_default_strategy = _________________
>>> make_weird_strategy = ________________
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
	  <pre class="codemargin">          
          >>> default_strategy = lambda score, op_score : 5
          >>> make_default_strategy = lambda : lambda score, op_score : 5
          >>> make_weird_strategy = lambda num_rolls : lambda score, op_score : max(num_rolls, (score+op_score)//20)
	  </pre>
	  </p>
    </div>
    <?php } ?>

<h3 class="section_title">Exercise 3: Lambdas and Currying</h3>

<p> Using a lambda function, complete the mul by num function. This function should
take an argument and return a one argument function that multiplies any value passed
to it by the original number. Its body must be one line long: </p>

<pre class="codemargin">
def mul_by_num(num):
    """
    Returns a function that takes one argument and returns num times that
    argument.
    >>> x = mul_by_num(5)
    >>> y = mul_by_num(2)
    >>> x(3)
    15
    >>> y(-4)
    -8
    """
    return ________________________________________
</pre>

<p> We can transform multiple-argument functions into a chain of single-argument, higher
order functions by taking advantage of lambda expressions. This is useful when dealing
with functions that take only single-argument functions. We will see some examples of
these later on. </p>

<p> Write a function lambda_curry2 that will curry any two argument function using
lambdas. See the doctest if you're not sure what this means. </p>

<pre class="codemargin">
def lambda_curry2(func):
    """
    Returns a Curried version of a two argument function func.
    >>> x = lambda_curry2(add)
    >>> y = x(3)
    >>> y(5)
    8
    """
    return ________________________________________
</pre>




<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
	  <pre class="codemargin">          
          def mul_by_num(num):
	      return lambda num2: num * num2

	  def lambda_curry2(func):
	      return lambda arg1: lambda arg2: func(arg1, arg2)
	  </pre>
	  </p>
    </div>
    <?php } ?>


<h3 class="section_title">Exercise 4: Environment Diagrams</h3>

<p> Try drawing environment diagrams for the following code and predicting
what Python will output: </p>

<pre class="codemargin">
# Q1
a = lambda x : x * 2 + 1

def b(x):
    return x * y

y = 3
b(y)
_________

def c(x):
    y = a(x)
    return b(x) + a(x+y)
c(y)
_________


# Q2: This one is pretty tough. A carefully drawn environment diagram will be really useful.
g = lambda x: x + 3

def wow(f):
    def boom(g):
    	return f(g)
    return boom

f = wow(g)
f(2)
_________
g = lambda x: x * x
f(3)
_________
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p> Please use the online environment diagram drawer, linked at the bottom of the class webpage. </p>
      <p> <b>1.</b> 9 (for the first blank), 30 (for the second blank). </p>
      <p> <b>2.</b> 5 (for the first blank), 6 (for the second blank). Notice that the line
	   g = lambda x: x * x doesn't change what f(3) does! </p>
    </div>
    <?php } ?>


<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script>
  <?php for ($i = 0; $i < $q_num; $i++) { ?>
  $("#toggleButton<?php echo $i; ?>").click(function () {
  $("#toggleText<?php echo $i; ?>").toggle();
  });
  <?php } ?>
</script>
<?php } ?>

  </body>
</html>
