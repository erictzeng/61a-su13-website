<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <meta name="description" content ="CS61A: Structure and Interpretation of
Computer Programs" />
     <meta name="keywords" content ="CS61A, Computer Science, CS, 61A,
Programming, Steven Tang and Eric Tzeng, Berkeley, EECS" />
    <meta name="author" content ="Steven Tang, Eric Tzeng, Albert Wu, Mark
Miyashita, Robert Huang, Andrew Huang, Brian Hou, Leonard Truong, Jeffrey Lu,
Rohan Chitnis" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">@import url("../lab_style.css");</style>
    <style type="text/css">@import url("../61a_style.css");</style>

    <title>CS 61A Summer 2013: Lab 4a</title>

    <?php
    /* So all of the PHP in this file is to allow for this nice little trick to
     * help us avoid having two versions of the questions lying around in the
     * repository, which often leads to the two versions going out of sync which
     * leads to annoyance for students.
     *
     * The idea's pretty simple for the PHP part, just simply have two dates:
     *
     *    1. The current date
     *    2. The date the solutions should be released
     *
     * Using these, we now wrap our solutions in a simple PHP if statement that
     * checks if the date is past the release date and only includes the code on
     * the page displayed (what the server gives back to the browser) if the
     * solutions are supposed to be released.
     *
     * We also use some PHP to create unique IDs for each of the show/hide
     * buttons and solution divs, which are then used in the PHP generated
     * jQuery code that we use to create the nice toggling effect.
     *
     * I apologize if the PHP/jQuery is really offensively bad, this is
     * literally the most I've written of either for a single project so far.
     * Comments/suggestions are most welcome!
     *
     * - Tom Magrino (tmagrino@berkeley.edu)
     */
    $BERKELEY_TZ = new DateTimeZone("America/Los_Angeles");
    $RELEASE_DATE = new DateTime("07/16/2013", $BERKELEY_TZ);
    $CUR_DATE = new DateTime("now", $BERKELEY_TZ);
    $q_num = 0; // Used to make unique ids for all solutions and buttons
    ?>
  </head>

  <body style="font-family: Georgia,serif;">

<h1>CS61A Lab 4a: Object Oriented Programming</h1>
<h3>Week 4a, 2013</h3>


<h3 class="section_title">Warmup</h3>

<p>0.) Predict the result of evaluating the following calls in the interpreter. Then try them out yourself!</p>

<pre class="codemargin">
>>> class Account(object):
...     interest = 0.02
...     def __init__(self, account_holder):
...         self.balance = 0
...         self.holder = account_holder
...     def deposit(self, amount):
...         self.balance = self.balance + amount
...         print("Yes!")
...
>>> a = Account("Billy")
>>> a.account_holder
______
</pre>
<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
  <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
  <pre>
AttributeError: 'Account' object has no attribute 'account_holder'</pre>
  </div>
<?php } ?>
<pre class="codemargin">
>>> a.holder
______
</pre>
<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
  <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
<pre>
'Billy'
</pre>
  </div>
<?php } ?>
<pre class="codemargin">
>>> class CheckingAccount(Account):
...     def __init__(self, account_holder):
...         Account.__init__(self, account_holder)
...     def deposit(self, amount):
...         Account.deposit(self, amount)
...         print("Have a nice day!")
...
>>> c = CheckingAccount("Eric")
>>> a.deposit(30)
______
</pre>
<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
  <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
  <pre class="codemargin">
Yes!
  </pre>
  </div>
<?php } ?>
<pre class="codemargin">
>>> c.deposit(30)
______
</pre>
<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
  <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
  <pre class="codemargin">
Yes!
Have a nice day!
  </pre>
  </div>
<?php } ?>

<p>1.) Consider the following basic definition of a <span class="code">Person</span> class:</pre>

<pre class="codemargin">
class Person(object):

    def __init__(self, name):
        self.name = name

    def say(self, stuff):
        return stuff

    def ask(self, stuff):
        return self.say("Would you please " + stuff)

    def greet(self):
        return self.say("Hello, my name is " + self.name)
</pre>

<p> Modify this class to add a <span class="code">repeat</span> method, which
repeats the last thing said. Here's an example of its use:

<pre class="codemargin">
>>> steven = Person("Steven")
>>> steven.repeat()       # starts at whatever value you'd like
"I squirreled it away before it could catch on fire."
>>> steven.say("Hello")
"Hello"
>>> steven.repeat()
"Hello"
>>> steven.greet()
"Hello, my name is Steven"
>>> steven.repeat()
"Hello, my name is Steven"
>>> steven.ask("preserve abstraction barriers")
"Would you please preserve abstraction barriers"
>>> steven.repeat()
"Would you please preserve abstraction barriers"
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
  <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
  <pre class="codemargin">
class Person(object):

    def __init__(self, name):
        self.name = name
        self.previous = "I squirreled it away before it could catch on fire"

    def say(self, stuff):
        self.previous = stuff
        return stuff

    def ask(self, stuff):
        return self.say("Would you please " + stuff)

    def greet(self):
        return self.say("Hello, my name is " + self.name)

    def repeat(self):
        return self.say(self.previous)
  </pre>
  </div>
<?php } ?>

<p>2.) Suppose now that we wanted to define a class
called <span class="code">DoubleTalker</span> to represent people who always say
things twice:

<pre class="codemargin">
>>> steven = DoubleTalker("Steven")
>>> steven.say("hello")
"hello hello"
>>> steven.say("the sky is falling")
"the sky is falling the sky is falling"
</pre>

Consider the following three definitions for <span class="code">DoubleTalker</span>:

<pre class="codemargin">
class DoubleTalker(Person):
    def __init__(self, name):
        Person.__init__(self, name)
    def say(self, stuff):
        return Person.say(self, stuff) + " " + self.repeat()

class DoubleTalker(Person):
    def __init__(self, name):
        Person.__init__(self, name)
    def say(self, stuff):
        return stuff + " " + stuff

class DoubleTalker(Person):
    def __init__(self, name):
        Person.__init__(self, name)
    def say(self, stuff):
        return Person.say(self, stuff + " " + stuff)
</pre>

<p>Determine which of these definitions work as intended. Also determine for which
of the methods the three versions would respond differently. (Don't forget about
the <span class="code">repeat</span> method!)</p>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
  <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
  The last one works as intended.  For the first and second ones, calling <span class="code">repeat</span> would fail.
  </div>
<?php } ?>

<p>4.) Here are the <span class="code">Account</span>
and <span class="code">CheckingAccount</span> classes from lecture:</p>

<pre class="codemargin">
class Account(object):
    """A bank account that allows deposits and withdrawals."""

    interest = 0.02

    def __init__(self, account_holder):
        self.balance = 0
        self.holder = account_holder

    def deposit(self, amount):
        """Increase the account balance by amount and return the new balance."""
        self.balance = self.balance + amount
        return self.balance

    def withdraw(self, amount):
        """Decrease the account balance by amount and return the new balance."""
        if amount > self.balance:
            return 'Insufficient funds'
        self.balance = self.balance - amount
        return self.balance


class CheckingAccount(Account):
    """A bank account that charges for withdrawals."""

    withdraw_fee = 1
    interest = 0.01

    def withdraw(self, amount):
        return Account.withdraw(self, amount + self.withdraw_fee)
</pre>

<p>Modify the code so that both classes have a new
attribute, <span class="code">transactions</span>, that is a list keeping track
of any transactions performed. For example:

<pre class="codemargin">
>>> eric_account = Account(“Eric”)
>>> eric_account.deposit(1000000)   # depositing my paycheck for the week
1000000
>>> eric_account.transactions
[(‘deposit’, 1000000)]
>>> eric_account.withdraw(100)      # buying dinner
999900
>>> eric_account.transactions
[(‘deposit’, 1000000), (‘withdraw’, 100)]
</pre>

<p>Don't repeat code if you can help it; use inheritance!</p>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
  <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
  <pre class="codemargin">
class Account(object):
    """A bank account that allows deposits and withdrawals."""

    interest = 0.02

    def __init__(self, account_holder):
        self.balance = 0
        self.holder = account_holder
        self.transactions = []

    def deposit(self, amount):
        """Increase the account balance by amount and return the new balance."""
        self.transactions.append(('deposit', amount))
        self.balance = self.balance + amount
        return self.balance

    def withdraw(self, amount):
        """Decrease the account balance by amount and return the new balance."""
        self.transactions.append(('withdraw', amount))
        if amount > self.balance:
            return 'Insufficient funds'
        self.balance = self.balance - amount
        return self.balance


class CheckingAccount(Account):
    """A bank account that charges for withdrawals."""

    withdraw_fee = 1
    interest = 0.01

    def withdraw(self, amount):
        return Account.withdraw(self, amount + self.withdraw_fee)
</pre>
</div>
<?php } ?>

<p>5.) We'd like to be able to cash checks, so let's add
a <span class="code">deposit_check</span> method to
our <span class="code">CheckingAccount</span> class. It will take
a <span class="code">Check</span> object as an argument, and check to see if
the <span class="code">payable_to</span> attribute matches
the <span class="code">CheckingAccount</span>'s holder. If so, it marks
the <span class="code">Check</span> as deposited, and adds the amount specified
to the <span class="code">CheckingAccount</span>'s total. Here's an example:

<pre class="codemargin">
>>> check = Check(“Steven”, 42)  # 42 dollars, payable to Steven
>>> steven_account = CheckingAccount(“Steven”)
>>> eric_account = CheckingAccount(“Eric”)
>>> eric_account.deposit_check(check)  # trying to steal steven’s money
The police have been notified.
>>> eric_account.balance
0
>>> check.deposited
False
>>> steven_account.balance
0
>>> steven_account.deposit_check(check)
42
>>> check.deposited
True
>>> steven_account.deposit_check(check)  # can't cash check twice
The police have been notified.
</pre>

<p>Write an appropriate <span class="code">Check</span> class, and add
the <span class="code">deposit_check</span> method to
the <span class="code">CheckingAccount</span> class. Make sure not to copy and
paste code! Use inheritance whenever possible.</p>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
  <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
  <pre class="codemargin">
class Account(object):
    """A bank account that allows deposits and withdrawals."""

    interest = 0.02

    def __init__(self, account_holder):
        self.balance = 0
        self.holder = account_holder
        self.transactions = []

    def deposit(self, amount):
        """Increase the account balance by amount and return the new balance."""
        self.transactions.append(('deposit', amount))
        self.balance = self.balance + amount
        return self.balance

    def withdraw(self, amount):
        """Decrease the account balance by amount and return the new balance."""
        self.transactions.append(('withdraw', amount))
        if amount > self.balance:
            return 'Insufficient funds'
        self.balance = self.balance - amount
        return self.balance


class CheckingAccount(Account):
    """A bank account that charges for withdrawals."""

    withdraw_fee = 1
    interest = 0.01

    def withdraw(self, amount):
        return Account.withdraw(self, amount + self.withdraw_fee)
		
    def deposit_check(self, check):
        if check.payable_to != self.holder or check.deposited:
            print("The police have been notified")
        else:
            self.deposit(check.amount)
            check.deposited = True
		
class Check(object):
    def __init__(self, payable_to, amount):
        self.payable_to = payable_to
        self.amount = amount
        self.deposited = False

</pre>
</div>
<?php } ?>

<p>6.) If you finish the lab early, feel free to get started on the next project!</p>
<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <script src="http://code.jquery.com/jquery-latest.js"></script>
<script>
<?php for ($i = 0; $i < $q_num; $i++) { ?>
$("#toggleButton<?php echo $i; ?>").click(function () {
  $("#toggleText<?php echo $i; ?>").toggle();
});
<?php } ?>
</script>
    <?php } ?>
</body>
</html>
