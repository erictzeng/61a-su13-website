<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <meta name="description" content ="CS61A: Structure and Interpretation of Computer Programs" />
    <meta name="keywords" content ="CS61A, Computer Science, CS, 61A, Programming, Berkeley, EECS" />
  <meta name="author" content ="Steven Tang, Eric Tzeng, Albert Wu, Mark Miyashita, Robert Huang, Andrew Huang, Brian Hou, Leonard Truong, Jeffrey Lu, Rohan Chitnis" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">@import url("https://inst.eecs.berkeley.edu/~cs61a/su12/lab/lab_style.css");</style>

    <title>CS 61A Summer 2012: Lab 8a</title>

    <?php
    /* So all of the PHP in this file is to allow for this nice little trick to
     * help us avoid having two versions of the questions lying around in the
     * repository, which often leads to the two versions going out of sync which
     * leads to annoyance for students.
     *
     * The idea's pretty simple for the PHP part, just simply have two dates:
     *
     *    1. The current date
     *    2. The date the solutions should be released
     *
     * Using these, we now wrap our solutions in a simple PHP if statement that
     * checks if the date is past the release date and only includes the code on
     * the page displayed (what the server gives back to the browser) if the
     * solutions are supposed to be released.
     *
     * We also use some PHP to create unique IDs for each of the show/hide
     * buttons and solution divs, which are then used in the PHP generated
     * jQuery code that we use to create the nice toggling effect.
     *
     * I apologize if the PHP/jQuery is really offensively bad, this is
     * literally the most I've written of either for a single project so far.
     * Comments/suggestions are most welcome!
     *
     * - Tom Magrino (tmagrino@berkeley.edu)
     */
    $BERKELEY_TZ = new DateTimeZone("America/Los_Angeles");
    $RELEASE_DATE = new DateTime("08/13/2013", $BERKELEY_TZ);
    $CUR_DATE = new DateTime("now", $BERKELEY_TZ);
    $q_num = 0; // Used to make unique ids for all solutions and buttons
    ?>
  </head>

  <body style="font-family: Georgia,serif;">

    <h1>CS61A Lab 8a: MapReduce</h1>
    <h3>Week 8b, 2013</h3>
    <hr />

<h3 class=section_title>Starter Files</h3>

<p>To get the files necessary for this lab (such as <tt>mr.py</tt>),
let's copy the relevant lab files:</p>

<pre class='codemargin'>cd
cp -r ~cs61a/public_html/su13/lab/lab08a/lab8a .
cd lab8a
</pre>

<h3 class=section_title>Introduction: MapReduce</h3>

<p>In this lab, we'll be working with MapReduce, a programming
paradigm developed by Google, which allows a programmer to process
large amounts of data in parallel on many computers.</p>

<p>Any computation in MapReduce consists primarily of two components:
the <b>mapper</b> and the <b>reducer</b>.</p>

<p>The <b>mapper</b> takes an input file, and outputs a series of
key-value pairs, like:</p>

<pre class='codemargin'>age 29
name cecilia
job gradstudent
salary 42
</pre>

<p>In the example above, the key-value pairs are:</p>

<pre class='codemargin'>(age -> 29), (name -> cecilia), (job -> gradstudent), (salary -> 42)</pre>

<p>The <b>reducer</b> takes the (sorted) output from the mapper, and
outputs a single value for each key. The mapper's output will be
sorted according to the key.</p>

<p>The entire MapReduce pipeline can be summarized by the following
diagram:</p>

<img width=50% class=figure src="mapreduce_diag.png" alt="Mapreduce Diagram"/>
<p class=figure_caption>The MapReduce Pipeline</p>

<p>We will first start off with a non-parallelized version of
MapReduce (using your Unix terminal) to introduce the concept of
mappers and reducers. We will then move onto Hadoop, an open-source
implementation of MapReduce that parallelizes the framework across
a cluster of machines.</p>

<h3 class=section_title>Serial MapReduce: An Introduction with
Line-Counting</h3>

<p>Before we parallelize the MapReduce process, let's first get
familiar with our mappers and reducers. Our first exercise will
be to count the number of lines in all of Shakespeare's plays.</p>

<p>On our servers, we happen to have all of Shakespeare's plays in
text format. For instance, if you're so inclined, feel free to read a
few phrases from "Romeo and Juliet":<br/>

<pre class='codemargin'>emacs ~cs61a/lib/shakespeare/romeo_and_juliet.txt &amp;</pre>

<p>Or how about..."The Tempest"?</p>

<pre class='codemargin'>emacs ~cs61a/lib/shakespeare/the_tempest.txt &amp;</pre>

<p>Anyways, we'd like to be able to count all the lines in all of his
plays. Choose a Shakespeare play (say, <tt>the_tempest.txt</tt>) and
copy it into your current directory by doing:</p>

<pre class='codemargin'>cp ~cs61a/lib/shakespeare/the_tempest.txt .</pre>

<p>To formulate this as a MapReduce problem, we need to define an
appropriate <tt>mapper</tt> and <tt>reducer</tt> function.</p>

<p>One way to do this is to have the mapper create a key-value pair
for every line in each play, whose key is always the word 'line', and
whose value is always 1.</p>

<p>The reducer would then simply be a simple sum of all the values, as
this picture illustrates:</p>

<img width=80% class=figure src="mapreduce_linecount.png" alt="Linecount example"/>
<p class=figure_caption>Line-counting in MapReduce</p>

<p>Let's implement each feature (mapper, reducer) separately, then see
how each piece fits together.</p>

<h3 class=section_title>The Mapper: line_count.py</h3>

<p>In your current directory should be a file <tt>line_count.py</tt>
with the following body:</p>

<h4 class=section_title>line_count.py</h4>
<pre class='codemargin'>
#!/usr/bin/env python3

import sys
from ucb import main
from mr import emit

@main
def run():
    for line in sys.stdin:
        emit('line', 1)
</pre>

<p><tt>line_count.py</tt> is the mapper, which takes input from
<tt>stdin</tt> (i.e. '<i>standard in</i>') and outputs one key-value
pair for each line to <tt>stdout</tt> (i.e. '<i>standard out</i>',
which is typically the terminal output).</p>

<p>Let's try running <tt>line_count.py</tt> by feeding it
<tt>the_tempest.txt</tt>. The question is, how do we give
<tt>the_tempest.txt</tt> to <tt>line_count.py</tt> via <tt>stdin</tt>?
We'll use the Unix pipe '<tt>|</tt>' feature (<u>Note</u>: the 'pipe'
key '<tt>|</tt>' isn't lowercase 'L', it's (typically)
Shift+Backslash):</p>

<p><b>Note</b>: You will probably have to tell Unix to treat
<tt>line_count.py</tt> as an executable by issuing the following
command:</p>

<pre class='codemargin'>chmod +x line_count.py</pre>

<p>Once you've made <tt>line_count.py</tt> executable, type in the
following command:</p>

<pre class='codemargin'>cat the_tempest.txt | ./line_count.py</pre>

<p>Recall that the <tt>cat</tt> program will display the contents of a
given file to <tt>stdout</tt>.</p>

<p>If you've completed <tt>line_count.py</tt> correctly, your terminal
output should be full of key-value pairs, looking something like:</p>

<pre class='codemargin'>'line' 1
'line' 1
'line' 1
...
'line' 1</pre>

<p>What pipe-ing does in Unix is take the output of one program (in
this case, the <tt>cat</tt> program), and 'pipe' it into the input to
another program (typically via <tt>stdin</tt>). This technique of
piping programs together is ubiquitous in Unix-style programming, and
is a sign of modular programming. The idea is: if you can write
modular programs, then it will be easy to accomplish tasks by chaining
together multiple programs. We'll do more with this idea in a moment.
</p>

<h3 class=section_title>The Reducer: sum.py</h3>

<p>In your current directory should be the file <tt>sum.py</tt>. The
body of this file should be:</p>

<h4 class=section_title>sum.py</h4>
<pre class='codemargin'>
#!/usr/bin/env python3

import sys
from ucb import main
from mr import values_by_key, emit

@main
def run():
    for key, value_iterator in values_by_key(sys.stdin):
        emit(key, sum(value_iterator))
</pre>

<p>This is the reducer, which reads in sorted key-value pairs from
<tt>stdin</tt>, and outputs a single value for each key. In this case,
<tt>sum.py</tt> will return the <tt>sum</tt> of all the values for a
given key. In other words, the reducer is <i>reducing</i> the values
of a given key into a single value.</p>

<p>The <tt>emit</tt> procedure takes in two arguments: a <tt>key</tt>,
and a <tt>reduced_value</tt>, and performs the necessary bookkeeping
so that Hadoop is aware that we are combining all key-value pairs from
the mapper (here, <tt>stdin</tt>) with the key <tt>key</tt> into the
single value <tt>reduced_value</tt>.</p>

<p>For the purposes of this simple line-counter, since the
<b>mapper</b> only returns one type of key ('<tt>line</tt>'), the
<b>reducer</b> will also only return one value - basically the total
number of key-value pairs.</p>

<h3 class=section_title>Putting it all together</h3>

<p>Now that we have the <b>mapper</b> and the <b>reducer</b> defined,
let's put it all together in the (simplified) MapReduce framework:</p>

<img width=50% class=figure src="map_sort_reduce.png" alt="map sort reduce"/>
<p class=figure_caption>The MapReduce Flow</p>

<p><b>Note</b>: You will probably have to tell Unix to treat
<tt>sum.py</tt> as an executable by issuing the following command:</p>

<pre class='codemargin'>chmod +x sum.py</pre>

<p>Once you've done this, issue the following Unix command:</p>

<pre class='codemargin'>cat the_tempest.txt | ./line_count.py | sort | ./sum.py </pre>

<p>Notice that we're using the Unix program <tt>sort</tt>, which is a
'built-in' Unix program. As you'd expect, <tt>sort</tt> will, given a
file, sort the lines of the file - by default, it will sort it
alphabetically.</p>

<p>Take a moment and make sure you understand how the above Unix
command is exactly the MapReduce framework (Map -> Sort -> Reduce).
What's neat is that, in a very simple manner, we executed the
MapReduce idea of using mappers and reducers to solve a problem.
However, the main benefit of using the MapReduce idea is to take
advantage of distributed computing - don't worry, we'll do that soon!
</p>

<h3 class=section_title>Exercises</h3> <br/>

<h4 class=section_title>Question 1</h4>

<p>Use the MapReduce framework (i.e. Map -> Sort -> Reduce) to count
the number of times the following (common) words occur:</p>

<pre class='codemargin'>the
he
she
it
thee</pre>

<p>A question to ponder is: will you need to create a new mapper, a 
new reducer, or both?</p>

    <?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
You should only need to create a new mapper - this mapper will look
through each word of each line, and create a new key-value pair if the
word matches one of the words we're looking for. The key will be the
word itself, and the value will be 1.
Then, all that the reducer has to do is add up all values - this is
done via <tt>sum.py</tt>. Here's the Unix command:

<pre class='codemargin'># cat the_tempest.txt | ./q1_mapper.py | sort | ./sum.py
'he'    81
'it'    133
'she'   30
'the'   484
'thee'  100</pre>
</p>

<tt>Q1_mapper</tt>
<pre class='codemargin'>
#!/usr/bin/env python3

import sys
from ucb import main
from mr import emit

@main
def run():
    interesting_words = ('the', 'he', 'she', 'thee', 'it')
    for line in sys.stdin:
        for word in line.split():
            wd_clean = word.strip().lower()
            if wd_clean in interesting_words:
                emit(wd_clean, 1)
</pre>
</p>
    </div>
    <?php } ?>

<h3 class=section_title>MapReduce with Hadoop</h3>

<p>
To use the distributed-computing power, you'll need to SSH into
the <tt>icluster</tt> servers (Hadoop is installed only on these machines).
To do this, issue the following terminal command:

<pre class='codemargin'># ssh -X cs61a-xx@icluster1.eecs.berkeley.edu</pre>

where 'xx' is replaced with your login. You will be prompted to log in.
</p>

<p>
Then, some environment variables need to be set - issue the following Unix command:

<pre class='codemargin'>source lab8a_envvars</pre>

<!--<u>Note</u>: For some reason, to run some of the commonly-used Unix commands on <tt>icluster1</tt>
(such has <tt>mv</tt>, <tt>rm</tt>, etc), you may need to fully specify the
filepath of the program, like:

<pre class='codemargin'># /bin/mv myfile.py newmyfile.py</pre> -->

<p>We have provided a way to practice making calls to the MapReduce
framework (using the Hadoop implementation). The provided file
<tt>mr.py</tt> will take care of the details of communicating with
Hadoop via Python. Here are a list of commands that you can give to
<tt>mr.py</tt>:</p>

<p><u>Note</u>: Some terminology. The Hadoop framework, for its own reasons,
maintains its own filesystem separate from the filesystems your
instructional accounts are on. As such, the following Hadoop filesystem
commands are performed with respect to your Hadoop filesystem. </p>


<h4 class=section_title>cat</h4>
<pre class='codemargin'> #  python3 mr.py cat OUTPUT_DIR </pre>

<p>This command prints out the contents of all files in one of the
directories on the Hadoop FileSystem owned by you (given by <tt>OUTPUT_DIR</tt>).
</p>

<h4 class=section_title>ls</h4>
<pre class='codemargin'> # python3 mr.py ls </pre>

<p>This command lists the contents of all output directories on the Hadoop
FileSystem. </p>

<h4 class=section_title>rm</h4>
<pre class='codemargin'> # python3 mr.py rm OUTPUT_DIR </pre>

<p>This command will remove an output directory (and all files within it)
on the Hadoop FileSystem.
Use this with caution - remember, there's no 'undo'!</p>

<h4 class=section_title>run</h4>
<pre class='codemargin'> # python3 mr.py run MAPPER REDUCER INPUT_DIR OUTPUT_DIR </pre>
<p> This command will run a MapReduce job of your choosing, where: <br/>
  <ul>
    <li><tt>MAPPER</tt>   --   a <tt>.py</tt> file that contains the mapper function, i.e. <tt>line_count.py</tt></li>
    <li><tt>REDUCER</tt>  --   a <tt>.py</tt> file that contains the reducer function, i.e. <tt>sum.py</tt></li>
    <li><tt>INPUT_DIR</tt> -- the input file, i.e. <tt>../cs61a/shakespeare</tt></li>
    <li><tt>OUTPUT_DIR</tt> -- the name of the directory where you would like the results of the MapReduce
                               job to be dumped into, i.e. <tt>myjob1</tt></li>
  </ul>
</p>

<h3 class=section_title>Example: Line-counting with Hadoop</h3>

Now, make sure that your <tt>line_count.py</tt>, <tt>sum.py</tt>, and
<tt>mr.py</tt> are in the current directory, then issue the command:

<pre class='codemargin'># python3 mr.py run line_count.py sum.py ../shakespeare.txt mylinecount </pre>

Your terminal should then be flooded with the busy output of Hadoop doing
its thing. Once it's finished, you'll want to examine the Hadoop results!
To do this, first call <tt>mr.py</tt>'s <tt>ls</tt> command to see the
contents of your Hadoop directory:

<pre class='codemargin'> # python3 mr.py ls </pre>

You should see a directory listing for your <tt>mylinecount</tt> job. To
view the results of this job, we'll use <tt>mr.py</tt>'s <tt>cat</tt> command:

<pre class='codemargin'> # python3 mr.py cat mylinecount/part-00000 </pre>

As an interesting reference point, one TA ran this MapReduce job on a
lightly-loaded <tt>icluster1</tt>, but totally in serial, meaning that each
map job had to be done sequentially. The total <tt>line_count</tt> job
took on the order of 5-8 minutes. How much faster was it to run it
with Hadoop using distributed computing, where the work can be done in
parallel?

</p>

<!--
<h3 class=section_title>Additional Inputs</h3>

<p>
In addition to <tt>shakespeare</tt>, we also have a few other input data available
on hadoop:

<pre class='codemargin'>/beatles-songs -- The complete discography of the Beatles
/emails -- A (large) collection of e-mails
/emails2 -- Another (large) collection of e-mails
/sample-emails -- A (small) collection of e-mails</pre>

For instance, to count the number of lines in <tt>/emails</tt>, I could do:

<pre class='codemargin'> # python3 mr.py run line_count.py sum.py /emails email_linecount </pre>
</p>
-->
<h3 class=section_title>Exercises </h3><br/>
<h4 class=section_title>Question 2: </h4>

<p> Take your solution from <b>Question 1</b> and run it through the distributed
MapReduce (i.e. by using <tt>mr.py</tt>) to discover the number of
occurrences of the following words in the entirety of Shakespeare's works:

<pre class='codemargin'>the
he
she
it
thee</pre>
</p>

    <?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
<p>The Unix command is:

<pre class='codemargin'># python3 mr.py run q1_mapper.py sum.py ../shakespeare q2
# python3 mr.py cat q2
'he'	6267
'it'	7736
'she'	2222
'the'	26785
'thee'	3103</pre>
</p>
    </div>
    <?php } ?>

<h4 class=section_title> Question 3a: </h4>

<p>
One common MapReduce application is a distributed word count. Given a large body of text, such as
the works of Shakespeare, we want to find out which words are the most common. <br/>
Write a MapReduce program that returns each word in a body of text paired with the number of times
it is used. For example, calling your solution with <tt>../shakespeare</tt> should
output something like: </p>
<pre class='codemargin'>the 300
was 249
thee 132
...
</pre>
<p><i><u>Note</u>: These aren't the actual numbers. </i></p>

<p>You probably will need to write a mapper function. Will you have to
write a new reducer function, or can you re-use a previously-used
reducer? </p>

    <?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
<h4>The Mapper:</h4>
<pre class='codemargin'>
#!/usr/bin/env python3

import sys
import string
from ucb import main
from mr import emit

def is_word(wd):
    if wd:
        for letter in string.ascii_lowercase:
            if letter in wd:
                return True
    return False

@main
def run():
    for line in sys.stdin:
        for word in line.split():
            wd_clean = word.strip().lower()
            if is_word(wd_clean):
                emit(wd_clean, 1)
</pre>
<h4>The Reducer:</h4>
You would re-use <tt>sum.py</tt>.
    </div>
    <?php } ?>



<h3>Working with the Trends Project</h3>
<p>We've included a portion of the trends project in the file that
    you copied at the beginning of the lab in the files "trends.py" and
    "data.py".  We're going to calculate the total sentiment of each of Shakespeare's
    plays much the same way that we calculated the total sentiment of a tweet
    in the trends project.</p>
<p>In order to do this, we need to create a new mapper.  The skeleton for this new mapper is
    in the file sentiment_mapper.py.  Fill in the function definition so that it emits the
    average sentiment of each line fed to it.</p>
<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
        <pre class='codemargin'>
#!/usr/bin/env python3

import sys
from ucb import main
from mr import emit

from trends import extract_words, get_word_sentiment, has_sentiment, sentiment_value
from mr import get_file

@main
def run():
    for line in sys.stdin:
        total = 0
        count = 0
        for word in extract_words(line):
            s = get_word_sentiment(word)
            if has_sentiment(s):
                total += sentiment_value(s)
                count += 1
        if count > 0:
            emit(get_file(), total/count)
        else:
            emit(get_file(), 0)

        </pre>
    </div>
<?php } ?>
<p>Note that we need to provide our code with the big sentiments.csv file. We've already stored this for you on the distributed file
    system that Hadoop uses.  To make sure the file is available to our code, we use the "run_with_cache" command instead of the "run" command
    which allows us to provide one additional parameter: the path (on the virtual file system) to the cache file which contains the sentiments.
    Don't worry too much about this part--it's just the specifics of our implementation.
</p>
<p>long story short, we will use the following command to run this map reduce task:</p>
<pre class='codemargin'>
    python3 mr.py run_with_cache sentiment_mapper.py sum.py ../shakespeare.txt MY_OUTFILE ../sentiments.csv#sentiments.csv
</pre>

<h3>More Fun Exercises!</h3>

<h4 class=section_title> Question 3b: </h4>
<p>Now, we will determine the most commonly used word. Write a Python
script file <tt>most_common_word.py</tt> that, given the output of the program you wrote in part 3A
(via <tt>stdin</tt>), returns the most commonly
used word. The usage should look like (assuming you named the Hadoop
job output <tt>wordcounts</tt>):
<pre class='codemargin'># python3 mr.py cat wordcounts | python3 most_common_word.py </pre>
</p>

    <?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
<h4>most_common_word.py</h4>
<pre class='codemargin'>
#!/usr/bin/env python3

import sys
from ucb import main

"""
Usage:

# python3 mr.py cat q3a | python3 q3b.py
"""

def argmax(counter):
    return sorted(counter.items(), key=lambda pair: pair[1])[-1]

@main
def run():
    wd_counts = {}
    for line in sys.stdin:
        word, count = line.split()
        count = int(count)
        wd_counts[word] = count
    print("Most commonly used word:", argmax(wd_counts))
    return
</pre>
    </div>
    <?php } ?>


<h4 class=section_title> Question 3c: </h4>
<p>Now, write a Python script file that, given the MapReduce output from
<b>Q3A</b> (via <tt>stdin</tt>), outputs all words used only once, in
alphabetical order. Finally, output the results into a text file
<tt>singles.txt</tt>. The Unix command should look like this:
<pre class='codemargin'># python3 mr.py cat wordcounts | python3 get_singles.py | sort > singles.txt </pre>
</p>

    <?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
<h4>get_singles.py</h4>
<pre class='codemargin'>
#!/usr/bin/env python3

import sys
from ucb import main

"""
Usage:
# python3 mr.py cat wordcounts | python3 get_singles.py | sort > singles.txt

"""

@main
def run():
    for line in sys.stdin:
        word, num = line.split()
        num = int(num)
        if int(num) == 1:
            print(word)
</pre>
    </div>
    <?php } ?>


<h4 class=section_title> Question 4a:</h4>
<p> In this question, you will discover write a MapReduce program that,
given a phrase, outputs which play the phrase came from. <br/>

Then, use your solution to figure out which play each of the following
famous Shakespeare phrases came from:

<pre class='codemargin'>
pomp and circumstance                       foregone conclusion
full circle                                 strange bedfellows
neither rime nor reason                     spotless reputation
one fell swoop                              seen better days
it smells to heaven                         a sorry sight
</pre>

<u>Hint</u>: In your mapper, you'll want to use the
<tt>get_file()</tt> helper function, which is defined in the
<tt>mr.py</tt> file.
<tt>get_file()</tt> returns the name of the file that the mapper is
currently
processing - for <tt>../shakespeare</tt>, the filenames will be play
names. To import <tt>get_file</tt>, include the following line at
the top of your Python script:

<pre class='codemargin'>
from mr import get_file
</pre>

Also, you might want to look at the included <tt>set.py</tt> reducer
which reduces
the values of each key into a <tt>set</tt> (i.e. removing duplicates).

    <?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
<h4>The Mapper</h4>
<pre class='codemargin'>
#!/usr/bin/env python3

import sys
import string
from ucb import main
from mr import emit, get_file

phrases = ('pomp and circumstance', 'foregone conclusion', 'full circle',
           'the makings of', 'method in the madness', 'neither rhyme nor reason',
           'one fell swoop', 'seen better days', 'it smells to heaven',
           'a sorry sight', 'spotless reputation', 'strange bedfellows')

def strip_punctuation(s):
    exclude = set(string.punctuation)
    return ''.join(char for char in s if char not in exclude)

def remove_erroneous_spaces(s):
    return " ".join(s.split())

@main
def run():
    for line in sys.stdin:
        line_lower = remove_erroneous_spaces(strip_punctuation(line)).lower()
        for phrase in phrases:
            if phrase in line_lower:
                emit(phrase, get_file())
</pre>
<h4>The Reducer</h4>
Use <tt>set.py</tt>.
    </div>
    <?php } ?>

</p>

<br/><br/>

<p>
Fin.
</p>

    <?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script>
      <?php for ($i = 0; $i < $q_num; $i++) { ?>
    $("#toggleButton<?php echo $i; ?>").click(function () {
      $("#toggleText<?php echo $i; ?>").toggle();
    });
      <?php } ?>
    </script>
    <?php } ?>
  </body>
</html>
