"""Starter file for CS 61A Summer 2013 Lab 2b"""

#######################
# Rational Number ADT #
#######################

# Q5 and 6

def make_rat(num, den):
    """Creates a rational number, given a numerator and denominator."""

def num(rat):
    """Extracts the numerator from a rational number."""

def den(rat):
    """Extracts the denominator from a rational number."""

# Q1

def add_rat(a, b):
    """Adds two rational numbers A and B. For example,
    (3 / 4) + (5 / 3) = (29 / 12)

    >>> a, b = make_rat(3, 4), make_rat(5, 3)
    >>> c = add_rat(a, b)
    >>> num(c)
    29
    >>> den(c)
    12
    """
    "*** YOUR CODE HERE ***"

def sub_rat(a, b):
    """Subtracts two rational numbers A and B. For example,
    (3 / 4) - (5 / 3) = (-11 / 12)

    >>> a, b = make_rat(3, 4), make_rat(5, 3)
    >>> c = sub_rat(a, b)
    >>> num(c)
    -11
    >>> den(c)
    12
    """
    "*** YOUR CODE HERE ***"

# Q2

def mul_rat(a, b):
    """Multiplies two rational numbers A and B. For example,
    (3 / 4) * (5 / 3) = (15 / 12)

    >>> a, b = make_rat(3, 4), make_rat(5, 3)
    >>> c = mul_rat(a, b)
    >>> num(c)
    15
    >>> den(c)
    12
    """
    "*** YOUR CODE HERE ***"

def div_rat(a, b):
    """Divides two rational numbers A and B. Keep in mind that A / B
    is equivalent to A * (1 / B). For example,
    (3 / 4) / (5 / 3) = (9 / 20)

    >>> a, b = make_rat(3, 4), make_rat(5, 3)
    >>> c = div_rat(a, b)
    >>> num(c)
    9
    >>> den(c)
    20
    """
    "*** YOUR CODE HERE ***"

# Q3

def eq_rat(a, b):
    """Returns True if two rational numbers A and B are equal. For
    example, (2 / 3) = (6 / 9), so eq_rat would return True.

    >>> a, b = make_rat(2, 3), make_rat(6, 9)
    >>> eq_rat(a, b)
    True
    >>> c, d = make_rat(1, 4), make_rat(1, 2)
    >>> eq_rat(c, d)
    False
    """
    "*** YOUR CODE HERE ***"

# Q4

def factorial(n):
    if n == 0:
        return 1
    return n * factorial(n - 1)

def approx_e(terms=100):
    """Approximates e to the specified number of TERMS.

    >>> num(approx_e(0))
    1
    >>> num(approx_e(1))
    2
    >>> result = approx_e(2)
    >>> num(result)
    5
    >>> den(result)
    2
    """
    "*** YOUR CODE HERE ***"

##############
# Vector ADT #
##############

# constructor

def make_vector(*elements):
    """Creates an arbitrarily long vector."""
    return elements

# selectors

def get_length(vector):
    """Returns the length of the vector, i.e. the number of elements
    in the vector."""
    return len(vector)

def get_elem(vector, i):
    """Returns the ith element in the vector."""
    return vector[i]

# Q7

def dot(a, b):
    """Returns the dot product of two Vectors A and B."""
    assert len(a) == len(b), "Vectors aren't aligned"
    i, product = 0, 0
    while i < len(a):
        product += a[i] * b[i]
    return product

def largest_elem(a):
    """Returns the largest element in the Vector A."""
    return max(a)
