
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <meta name="description" content ="CS61A: Structure and Interpretation of Computer Programs" />
    <meta name="keywords" content ="CS61A, Computer Science, CS, 61A, Programming, Berkeley, EECS" />
    <meta name="author" content ="Steven Tang, Eric Tzeng, Albert Wu, Mark Miyashita, Robert Huang, Andrew Huang, Brian Hou,
                                  Leonard Truong, Jeffrey Lu, Rohan Chitnis" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">@import url("../lab_style.css");</style>
    <style type="text/css">@import url("../61a_style.css");</style>

    <title>CS 61A Summer 2013: Lab 2b</title>

    <?php
    /* So all of the PHP in this file is to allow for this nice little trick to
     * help us avoid having two versions of the questions lying around in the
     * repository, which often leads to the two versions going out of sync which
     * leads to annoyance for students.
     *
     * The idea's pretty simple for the PHP part, just simply have two dates:
     *
     *    1. The current date
     *    2. The date the solutions should be released
     *
     * Using these, we now wrap our solutions in a simple PHP if statement that
     * checks if the date is past the release date and only includes the code on
     * the page displayed (what the server gives back to the browser) if the
     * solutions are supposed to be released.
     *
     * We also use some PHP to create unique IDs for each of the show/hide
     * buttons and solution divs, which are then used in the PHP generated
     * jQuery code that we use to create the nice toggling effect.
     *
     * I apologize if the PHP/jQuery is really offensively bad, this is
     * literally the most I've written of either for a single project so far.
     * Comments/suggestions are most welcome!
     *
     * - Tom Magrino (tmagrino@berkeley.edu)
     */
    $BERKELEY_TZ = new DateTimeZone("America/Los_Angeles");
    $RELEASE_DATE = new DateTime("07/04/2013", $BERKELEY_TZ);
    $CUR_DATE = new DateTime("now", $BERKELEY_TZ);
    $q_num = 0; // Used to make unique ids for all solutions and buttons
    ?>
  </head>

  <body style="font-family: Georgia,serif;">

<h1>CS61A Lab 2b: Data Abstraction</h1>
<h3>Week 2b, Summer 2013</h3>

<h3 class="section_title">Starter File</h3>

<p>We've provided you with a starter file that contains the code you
will be using throughout this lab. You can get it by logging onto
your class account and running the command:</p>

<pre class='codemargin'>
cp ~cs61a/lib/lab/lab02b/lab2b.py .
</pre>

<p>Don't forget the dot at the end!</p>

<p>Once you copy the file to your own account, you can open it up in
your favorite text editor or Emacs, instead of typing all your code
directly into the Python interpreter. This way, you can save your
progress and quickly revise code.</p>

<h3 class="section_title">Exercise 1: What would Python print?</h3>

<p>Try to figure out what Python will display after the following
lines are entered. Then type them into the interpreter to check your
answers.</p>

<pre class="codemargin">
&gt;&gt;&gt; x = (4 + 5, 9 // 2)
&gt;&gt;&gt; x           # Q1
______
&gt;&gt;&gt; x[0]        # Q2
______
&gt;&gt;&gt; x[1]        # Q3
______
&gt;&gt;&gt; x[2]        # Q4
______
&gt;&gt;&gt; len(x)      # Q5
______

&gt;&gt;&gt; y = (1,)
&gt;&gt;&gt; z = (x, y)
&gt;&gt;&gt; z           # Q6
______
&gt;&gt;&gt; z[0]        # Q7
______
&gt;&gt;&gt; z[0][0]     # Q8
______
&gt;&gt;&gt; z[1][0]     # Q9
______
&gt;&gt;&gt; len(z)      # Q10
______

&gt;&gt;&gt; x + y       # Q11
______
&gt;&gt;&gt; len(x + y)  # Q12
______
&gt;&gt;&gt; (8, 3)[0]   # Q13
______
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
	  <pre class="codemargin">
We recommend you try typing these statements into the interpreter.

1.)  (9, 4)
2.)  9
3.)  4
4.)  IndexError
5.)  2

6.)  ((9, 4), (1,))
7.)  (9, 4)
8.)  9
9.)  1
10.) 2

11.) (9, 4, 1)
12.) 3
13.) 8
	</pre>
	  </p>
    </div>
    <?php } ?>

<h3 class="section_title">Exercise 2: Using Abstractions</h3>

<p>One day, you and a friend decide to work on a programming project --
implement a Rational Number interface. Recall that a
<a href='https://en.wikipedia.org/wiki/Rational_number'>rational
number</a> is any number that can be expressed as <span class='code'>p
/ q</span>, where <span class='code'>p</span> and
<span class='code'>q</span> are integers.</p>

<p>Your friend gets to work implementing an <i>abstract data type</i>
(ADT for short) to represent rational numbers. In the meantime, you
will be writing functions that perform mathematical operations on
rational numbers. You and your friend agree on a set of
<i>constructors</i> and <i>selectors</i>:

<pre class='codemargin'>
def make_rat(num, den):
    """Creates a rational number, given a numerator and denominator."""

def num(rat):
    """Extracts the numerator from a rational number."""

def den(rat):
    """Extracts the denominator from a rational number."""
</pre>

<p><span class='code'>make_rat</span> is the <i>constructor</i>, and
<span class='code'>num</span> and <span class='code'>den</span> are
the <i>selectors</i>.</p>

<p>Notice that the functions don't have any bodies yet, so you won't
know <i>how</i> they work -- but that's okay! As long as you know what
each function <i>is supposed to do</i>, you can write your code. This
is called <b>data abstraction</b>.</p>

<p>Finish questions 1 through 4 first. We'll then provide you a way
to test your code</p>

<p><strong>Problem 1</strong>: Implement addition and subtraction for
rational numbers:</p>

<pre class='codemargin'>
def add_rat(a, b):
    """Adds two rational numbers A and B. For example,
    (3 / 4) + (5 / 3) = (29 / 12)

    &gt;&gt;&gt; a, b = make_rat(3, 4), make_rat(5, 3)
    &gt;&gt;&gt; c = add_rat(a, b)
    &gt;&gt;&gt; num(c)
    29
    &gt;&gt;&gt; den(c)
    12
    """
    "*** YOUR CODE HERE ***"

def sub_rat(a, b):
    """Subtracts two rational numbers A and B. For example,
    (3 / 4) - (5 / 3) = (-11 / 12)

    &gt;&gt;&gt; a, b = make_rat(3, 4), make_rat(5, 3)
    &gt;&gt;&gt; c = sub_rat(a, b)
    &gt;&gt;&gt; num(c)
    -11
    &gt;&gt;&gt; den(c)
    12
    """
    "*** YOUR CODE HERE ***"
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
	  <pre class="codemargin">
def add_rat(a, b):
    new_num = num(a) * den(b) + num(b) * den(a)
    new_den = den(a) * den(b)
    return make_rat(new_num, new_den)

def sub_rat(a, b):
    neg = make_rat(-num(b), den(b))
    return add_rat(a, neg)

# Alternate solution
def sub_rat(a, b):
    new_num = num(a) * den(b) - num(b) * den(a)
    new_den = den(a) * den(b)
    return make_rat(new_num, new_den)
	</pre>
    </div>
    <?php } ?>

<p><strong>Problem 2</strong>: Similarly, implement multiplication and
division for rational numbers:</p>

<pre class='codemargin'>
def mul_rat(a, b):
    """Multiplies two rational numbers A and B. For example,
    (3 / 4) * (5 / 3) = (15 / 12)

    &gt;&gt;&gt; a, b = make_rat(3, 4), make_rat(5, 3)
    &gt;&gt;&gt; c = mul_rat(a, b)
    &gt;&gt;&gt; num(c)
    15
    &gt;&gt;&gt; den(c)
    12
    """
    "*** YOUR CODE HERE ***"

def div_rat(a, b):
    """Divides two rational numbers A and B. Keep in mind that A / B
    is equivalent to A * (1 / B). For example,
    (3 / 4) / (5 / 3) = (9 / 20)

    &gt;&gt;&gt; a, b = make_rat(3, 4), make_rat(5, 2)
    &gt;&gt;&gt; c = div_rat(a, b)
    &gt;&gt;&gt; num(c)
    9
    &gt;&gt;&gt; den(c)
    20
    """
    "*** YOUR CODE HERE ***"
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
	  <pre class="codemargin">
def mul_rat(a, b):
    new_num = num(a) * num(b)
    new_den = den(a) * den(b)
    return make_rat(new_num, new_den)

def div_rat(a, b):
    new_num = num(a) * den(b)
    new_den = num(b) * den(a)
    return make_rat(new_num, new_den)

# Alternate solution
def div_rat(a, b):
    inverse = make_rat(den(b), num(b))
    return mul_rat(a, inverse)
	</pre>
    </div>
    <?php } ?>

<p><strong>Problem 3</strong>: Implement a function
<span class='code'>eq_rat</span>, which checks if two rational numbers
are equal. This isn't as straightforward as it seems; for example,
<span class='code'>(2 / 3)</span> is equal to
<span class='code'>(6 / 9)</span>.

<pre class='codemargin'>
def eq_rat(a, b):
    """Returns True if two rational numbers A and B are equal. For
    example, (2 / 3) = (6 / 9), so eq_rat would return True.

    &gt;&gt;&gt; a, b = make_rat(2, 3), make_rat(6, 9)
    &gt;&gt;&gt; eq_rat(a, b)
    True
    &gt;&gt;&gt; c, d = make_rat(1, 4), make_rat(1, 2)
    &gt;&gt;&gt; eq_rat(c, d)
    False
    """
    "*** YOUR CODE HERE ***"
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
	  <pre class="codemargin">
def eq_rat(a, b):
    return num(a) * den(b) == num(b) * den(a)
	</pre>
    </div>
    <?php } ?>

<p><strong>Problem 4</strong>: Finally, implement an approximation of
the irrational number <i>e</i> (approximately 2.718). We can represent
<i>e</i> as an infinite series of rational numbers:</p>

<img src='e-series.png'>

<p>where the <i>!</i> symbol denotes factorial. The function
<span class='code'>approx_e</span> should approximate <i>e</i> to
a specified number of terms in the series described above (the 0th
term is 1 / 0!). You should <b>return the answer as a rational number
object</b>.

<p><i>Hint</i>: you can use the functions you defined above.</p>

<pre class='codemargin'>
def factorial(n):
    if n == 0:
        return 1
    return n * factorial(n - 1)

def approx_e(terms=100):
    """Approximates e to the specified number of TERMS.

    &gt;&gt;&gt; num(approx_e(0))
    1
    &gt;&gt;&gt; num(approx_e(1))
    2
    &gt;&gt;&gt; result = approx_e(2)
    &gt;&gt;&gt; num(result)
    5
    &gt;&gt;&gt; den(result)
    2
    """
    "*** YOUR CODE HERE ***"
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
	  <pre class="codemargin">
def approx_e(terms=100):
    total = make_rat(0, 1)
    i = 0
    while i &lt;= terms:
        next = make_rat(1, factorial(i))
        total = add_rat(total, next)
        i += 1
    return total
	</pre>
    </div>
    <?php } ?>

<h3 class="section_title">Exercise 3: Defining Abstractions</h3>

<p>Your friend finally finished implementing the constructors and
selectors for the Rational Number ADT -- you can use the following
code to check your answers to Exercise 2.

<pre class='codemargin'>
def make_rat(num, den):
    """Creates a rational number, given a numerator and denominator."""
    return lambda x, y: (lambda: den + x, lambda: num + y)

def num(rat):
    """Extracts the numerator from a rational number."""
    return rat(2, 3)[1]() - 3

def den(rat):
    """Extracts the denominator from a rational number."""
    return rat(8, 5)[0]() - 8
</pre>

<p><strong>Problem 5</strong>: the code your friend wrote will work,
but it is very hard to understand (luckily, you respected data
abstraction, so your own code doesn't depend on how your friend's
works). Help your friend write a more readable set of constructors
and selectors.</p>

<p>Also, in <span class='code'>make_rat</span>, make sure to add some
<span class='code'>assert</span> statements to check that
<span class='code'>num</span> and <span class='code'>den</span> are
indeed integers.</p>

<pre class='codemargin'>
def make_rat(num, den):
    "*** YOUR CODE HERE ***"

def num(rat):
    "*** YOUR CODE HERE ***"

def den(rat):
    "*** YOUR CODE HERE ***"
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
  <p>Anything that is consistent will work here. Here's an example:</p>
  <pre class="codemargin">
def make_rat(num, den):
    assert type(num) == int, 'Numerator is not an integer'
    assert type(den) == int, 'Denominator is not an integer'
    return (num, den)

def num(rat):
    return rat[0]

def den(rat):
    return rat[1]
	</pre>
    </div>
    <?php } ?>

<p>Now check your solutions again to Exercise 2. Do they still work? If
you didn't violate data abstraction, they should!</p>

<p><strong>Problem 6</strong>: there's an improvement that could be
made to the constructor, <span class='code'>make_rat</span>. For
example, consider the following rational:</p>

<pre class='codemargin'>
&gt;&gt;&gt; a = make_rat(6, 9)
&gt;&gt;&gt; num(a)
6
&gt;&gt;&gt; den(a)
9
</pre>

<p>We would like to <i>simplify</i> the rational number so that it
becomes (2 / 3). Rewrite the <span class='code'>make_rat</span>
function so that it does this. Don't forget that the numerator
and denominator should still be integers!</p>

<p><i>Hint</i>: you can write your own Greatest Common Divisor
function, or you can use Python's built-in
<span class='code'>gcd</span> function.</p>

<pre class='codemargin'>
from fractions import gcd

def make_rat(num, den):
    "*** YOUR CODE HERE ***"
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
  <p>Anything that is consistent will work here. Here's an example:</p>
  <pre class="codemargin">
from fractions import gcd

def make_rat(num, den):
    assert type(num) == int, 'Numerator is not an integer'
    assert type(den) == int, 'Denominator is not an integer'
    divisor = gcd(num, den)
    return (num // divisor, den // divisor)
	</pre>
    </div>
    <?php } ?>

<p>Again, your code from Exercise 2 should still work, even after these
changes.</p>

<h3 class="section_title">Exercise 4: Abstraction Violations</h3>

<p>Now we will fix some data abstraction violations. A <i>data
abstraction violation</i> occurs when a piece of code does not
use the constructors and selectors of an ADT. Consider the following
vector ADT:

<pre class='codemargin'>
# constructor

def make_vector(*elements):
    """Creates an arbitrarily long vector."""
    return elements

# selectors

def get_length(vector):
    """Returns the length of the vector, i.e. the number of elements
    in the vector."""
    return len(vector)

def get_elem(vector, i):
    """Returns the ith element in the vector."""
    return vector[i]
</pre>

<p>Given that the three functions above are the only constructors and
selectors, the following function
<span class='code'>last</span> contains two abstraction violations:

<pre class='codemargin'>
def last(vector):
    """Returns the last element in a vector."""
    length = len(vector)
    return vector[length - 1]
</pre>

<p>The first violation is <span class='code'>len(vector)</span>.
The Vector ADT is not guaranteed to be implemented as a tuple, or even
a Python sequence, so we can't use the built-in
<span class='code'>len</span> function, which only works on Python
sequences.</p>

<p>The second violation is
<span class='code'>vector[length - 1]</span>. Even if we correctly
get the length, we can't just index (use square brackets notation with)
the Vector as if it were a tuple -- we aren't guaranteed that it'll
be implemented as a tuple.</p>

<p>The best way to spot abstraction violations is to ignore the
implementation of the constructors and selectors. If you want to take
it one step further, you can imagine the constructors and selectors
were implemented with lambdas:</p>

<pre class='codemargin'>
# constructor

def make_vector(*elements):
    """Creates an arbitrarily long vector."""
    return lambda: elements

# selectors

def get_length(vector):
    """Returns the length of the vector, i.e. the number of elements
    in the vector."""
    return len(vector())

def get_elem(vector, i):
    """Returns the ith element in the vector."""
    return vector()[i]
</pre>

<p>Now, the <span class='code'>last</span> function above clearly won't
work, because it violates abstraction!</p>

<p><strong>Problem 7</strong>: For the following functions, spot as
many abstraction violations as you can, and rewrite the code to
fix it. Use the Vector ADT defined above.</p>

<pre class='codemargin'>
def dot(a, b):
    """Returns the dot product of two Vectors A and B."""
    assert len(a) == len(b), "Vectors aren't aligned"
    i, product = 0, 0
    while i &lt; len(a):
        product += a[i] * b[i]
    return product

def largest_elem(a):
    """Returns the largest element in the Vector A."""
    return max(a)
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
  <pre class="codemargin">
def dot(a, b):
    """Returns the dot product of two Vectors A and B."""
    assert length(a) == length(b), "Vectors aren't aligned"  # length
    i, product = 0, 0
    while i &lt; length(a):     # length
        product += get_elem(a, i) * get_elem(b, i)  # get_elem
    return product

def largest_elem(a):
    """Returns the largest element in the Vector A."""
    largest, i = None, 0
    while i &lt; length(a):
        if largest is None:
            largest = get_elem(a, i)
        else:
            largest = max(largest, get_elem(a, i))
        i += 1
    return largest
	</pre>
    </div>
    <?php } ?>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script>
  <?php for ($i = 0; $i < $q_num; $i++) { ?>
  $("#toggleButton<?php echo $i; ?>").click(function () {
  $("#toggleText<?php echo $i; ?>").toggle();
  });
  <?php } ?>
</script>
<?php } ?>

  </body>
</html>
