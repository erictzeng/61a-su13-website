"""Starter file for CS 61A Summer 2013 Lab 5a"""

class Rlist(object):
    """A recursive list consisting of a first element and the rest.

    >>> s = Rlist(1, Rlist(2, Rlist(3)))
    >>> len(s)
    3
    >>> s[0]
    1
    >>> s[1]
    2
    >>> s[2]
    3
    """
    class EmptyList(object):
        def __len__(self):
            return 0
    empty = EmptyList()

    def __init__(self, first, rest=empty):
        self.first = first
        self.rest = rest

    def __repr__(self):
        f = repr(self.first)
        if self.rest is Rlist.empty:
            return 'Rlist({0})'.format(f)
        else:
            return 'Rlist({0}, {1})'.format(f, repr(self.rest))

    def __len__(self):
        return 1 + len(self.rest)

    def __getitem__(self, i):
        if i == 0:
            return self.first
        return self.rest[i - 1]

def map_rlist(fn, s):
    """Return an Rlist resulting from mapping fn over the elements of s.

    >>> s = Rlist(1, Rlist(2, Rlist(3)))
    >>> map_rlist(lambda x: x * x, s)
    Rlist(1, Rlist(4, Rlist(9)))
    """
    if s is Rlist.empty:
        return s
    return Rlist(fn(s.first), map_rlist(fn, s.rest))

def make_long_rlist(n):
    s = Rlist.empty
    while n != 0:
        s = Rlist(n, s)
        n -= 1
    return s

def fib_iter(n):
    cur, next = 0, 1
    for _ in range(n):
        cur, next = next, cur + next
    return cur

def fib_recur(n):
    if n < 2:
        return n
    return fib_recur(n-1) + fib_recur(n-2)

def make_fib_recur_memo():
    memo = {}
    def fib_recur_memo(n):
        if n in memo:
            return memo[n]
        if n < 2:
            result = n
        else:
            result = fib_recur_memo(n-1) + fib_recur_memo(n-2)
        memo[n] = result
        return result
    return fib_recur_memo

def main():
    import timeit
    for i in range(100, 1000, 100):
        setup = 'from lab05a_timer import Rlist, map_rlist, make_long_rlist; s = make_long_rlist({})'.format(i)
        t = timeit.Timer('map_rlist(lambda x: x*2, s)', setup=setup)
        print('map_rlist on an rlist of length {} took {} seconds'.format(i,
            t.timeit(number=100) / 100))

    for i in range(10, 50, 5):
        t = timeit.Timer('fib_iter({})'.format(i),
                setup='from lab05a_timer import fib_iter')
        print('fib_iter({}) took {} seconds'.format(i,
            t.timeit(number=5) / 5))

    for i in range(10, 35, 5):
        t = timeit.Timer('fib_recur({})'.format(i),
                setup='from lab05a_timer import fib_recur')
        print('fib_recur({}) took {} seconds'.format(i,
            t.timeit(number=2) / 2))

    for i in range(10, 50, 5):
        t = timeit.Timer('fib_recur_memo({})'.format(i),
                setup='from lab05a_timer import make_fib_recur_memo; fib_recur_memo = make_fib_recur_memo()')
        print('fib_recur_memo({}) took {} seconds'.format(i,
            t.timeit(number=5) / 5))

if __name__ == '__main__':
    main()
