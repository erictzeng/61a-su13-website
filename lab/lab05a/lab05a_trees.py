from tree import *

t = Tree(4,
         Tree(2, Tree(8, Tree(7)),
              Tree(3, Tree(1), Tree(6))),
         Tree(1, Tree(5),
              Tree(3, Tree(2), Tree(9))))

def max_of_tree(tree):
    """YOUR CODE HERE"""

def size_of_tree(tree):
    """YOUR CODE HERE"""

def deep_tree_reverse(tree):
    """YOUR CODE HERE"""

def filter_tree(tree, pred):
    """YOUR CODE HERE"""

bst_a = Tree(3, Tree(1, Tree(0)),
                Tree(5, Tree(4)))
bst_b = Tree(1, Tree(3, Tree(0)),
                Tree(5, Tree(4)))
bst_c = Tree(6, Tree(5, Tree(4, Tree(3, Tree(2, Tree(1))))))
bst_d = Tree(5, Tree(4, Tree(1, Tree(2))),
                Tree(9, Tree(6, Tree(8, Tree(7)))))

def get_min(bst):
    while bst.left:
        bst = bst.left
    return bst.entry

def get_max(bst):
    while bst.right:
        bst = bst.right
    return bst.entry

def is_valid_bst(bst):
    """YOUR CODE HERE"""

def bst_find(bst, elem):
    """YOUR CODE HERE"""

def bst_count_num_find(bst, max):
    """YOUR CODE HERE"""

def bst_count_num_recr(bst, max):
    """YOUR CODE HERE"""

def is_right_bigger(bst):
    """YOUR CODE HERE"""
