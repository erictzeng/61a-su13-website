<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta name="description" content ="CS61A: Structure and Interpretation of Computer Programs" />
  <meta name="keywords" content ="CS61A, Computer Science, CS, 61A, Programming, Berkeley, EECS" />
  <meta name="author" content ="Steven Tang, Eric Tzeng, Albert Wu, Mark Miyashita, Robert Huang, Andrew Huang, Brian Hou, Leonard Truong, Jeffrey Lu, Rohan Chitnis" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <style type="text/css">@import url("../lab_style.css");</style>
  <style type="text/css">@import url("../61a_style.css");</style>

  <title>CS 61A Summer 2013: Lab 05a</title>

  <?php
  /* So all of the PHP in this file is to allow for this nice little trick to
   * help us avoid having two versions of the questions lying around in the
   * repository, which often leads to the two versions going out of sync which
   * leads to annoyance for students.
   *
   * The idea's pretty simple for the PHP part, just simply have two dates:
   *
   *    1. The current date
   *    2. The date the solutions should be released
   *
   * Using these, we now wrap our solutions in a simple PHP if statement that
   * checks if the date is past the release date and only includes the code on
   * the page displayed (what the server gives back to the browser) if the
   * solutions are supposed to be released.
   *
   * We also use some PHP to create unique IDs for each of the show/hide
   * buttons and solution divs, which are then used in the PHP generated
   * jQuery code that we use to create the nice toggling effect.
   *
   * I apologize if the PHP/jQuery is really offensively bad, this is
   * literally the most I've written of either for a single project so far.
   * Comments/suggestions are most welcome!
   *
   * - Tom Magrino (tmagrino@berkeley.edu)
   */
  $BERKELEY_TZ = new DateTimeZone("America/Los_Angeles");
  $RELEASE_DATE = new DateTime("07/23/2013", $BERKELEY_TZ);
  $CUR_DATE = new DateTime("now", $BERKELEY_TZ);
  $q_num = 0; // Used to make unique ids for all solutions and buttons
  ?>
</head>

<body style="font-family: Georgia,serif;">

<h1>CS61A Lab 5a: Orders of Growth and Tree Data Structures</h1>
<h3>July 22, 2013</h3>
<p>
This lab has some orders of growth examples, sample tree data structures for you
to use, as well as a template to fill in for the questions. To get this template
file, execute in your terminal:
</p>
<pre class="codemargin">
cp ~cs61a/public_html/su13/lab/lab05a/lab05a*.py .
</pre>

<h2>Orders of Growth</h2>
<p>
Computer scientists are often interested in the efficiency of a program &ndash;
how much more expensive it is to run some procedure with a larger input. In
other words, as the size of the input grows, how do the speed of the procedure
and the space its process occupies grow?
</p>

<p>
To give a useful idea of efficiency, we use the Big-Theta notation. For example,
if <span class="code">foo</span>'s running time is in
&Theta;(n<sup>2</sup>), the running time, <span class="code">R(n)</span> will
grow proportionally to the square of the size of the input.
</p>

<p>
We will see more examples of this in discussion, but for now, let's take a look
at the running times of some code. The <span class="code">lab05a_timer.py</span>
file has some code and also a function that will test the running times of
various procedures: mapping onto an <span class="code">Rlist</span> and also
calculating the Fibonacci numbers. Run the file by executing:
</p>
<pre class="codemargin">
python3 lab05a_timer.py
</pre>

<p>
Try to analyze why some functions are faster than others. We will discuss this
more in depth tomorrow.
</p>

<h2>Trees</h2>
<p>
Trees are a way we have of representing a hierarchy of information. A family
tree is a good example of something with a tree structure. You have a matriarch
and a patriarch followed by all the descendants. Alternately, we may want to
organize a series of information geographically. At the very top, we have the
world, but below that we have countries, then states, then cities. We can also
decompose arithmetic operations into something much the same way.
</p>

<img src="firstTrees.png">

<p>
The name "tree" comes from the branching structure of the pictures, like
real trees in nature except that they’re drawn with the root at the top and the
leaves at the bottom.
</p>

<h4 class="section_title">Terminology</h4>
<pre class="codemargin">
node     - a point in the tree. In these pictures, each node includes a label (value at each node)
root     - the node at the top. Every tree has one root node
children - the nodes directly beneath it. Arity is the number of children that node has.
leaf     - a node that has no children. (Arity of 0!)
</pre>

<h2>Binary Trees</h2>

<p>
In this course, we will only be working with binary trees, where each node as at
most two children. For a general binary tree, order does not matter.
Additionally, the tree does not have to be balanced. It can be as lopsided as
one long chain.
</p>

<p>
Take a moment to study our implementation of binary trees. The implementation of
trees is in the file <span class='code'>
~cs61a/public_html/su13/lab/lab05a/tree.py</span>.
</p>

<h3 class="section_title">Question 1:</h3>
<p>Define the function <span class="code">max_of_tree</span> which takes in a
<span class="code">tree</span> as an argument and returns the max of all of the
values of each node in the tree. </p>
<pre class="codemargin">
&gt;&gt;&gt; t
Tree(4, Tree(2, Tree(8, Tree(7), None), Tree(3, Tree(1), Tree(6))), Tree(1, Tree(5), Tree(3, Tree(2), Tree(9))))
&gt;&gt;&gt; max_of_tree(t)
9
</pre>
<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
  <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
    <p>
      <pre>
def max_of_tree(tree):
    if not tree:
        return None
    return max(filter(lambda t: t is not None, (tree.entry,
                                                max_of_tree(self.left),
                                                max_of_tree(self.right))))
      </pre>
    </p>
  </div>
<?php } ?>

<h3 class="section_title">Question 2:</h3>
<p>Define the function <span class="code">size_of_tree</span> which takes in a
<span class="code">tree</span> as an argument and returns the number of
non-empty nodes in the tree.</p>
<pre class="codemargin">
&gt;&gt;&gt; t
Tree(4, Tree(2, Tree(8, Tree(7), None), Tree(3, Tree(1), Tree(6))), Tree(1, Tree(5), Tree(3, Tree(2), Tree(9))))
&gt;&gt;&gt; size_of_tree(t)
12
</pre>
<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
  <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
    <p>
      <pre>
def size_of_tree(tree):
    if not tree:
        return 0
    return 1 + size_of_tree(tree.left) + size_of_tree(tree.right)
      </pre>
    </p>
  </div>
<?php } ?>

<h3 class="section_title">Question 3:</h3>
<p>
Define the function <span class="code">deep_tree_reverse</span>, which takes a
tree and reverses the given order.
</p>
<pre class="code">
&gt;&gt;&gt; a = t.copy()
&gt;&gt;&gt; deep_tree_reverse(a)
&gt;&gt;&gt; a
Tree(4, Tree(1, Tree(3, Tree(9), Tree(2)), Tree(5)), Tree(2, Tree(3, Tree(6), Tree(1)), Tree(8, None, Tree(7))))
</pre>
<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
  <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
    <p>
      <pre>
def deep_tree_reverse(tree):
    if tree:
        tree.left, tree.right = tree.right, tree.left
        deep_tree_reverse(tree.left)
        deep_tree_reverse(tree.right)
      </pre>
    </p>
  </div>
<?php } ?>

<h3 class="section_title">Question 4:</h3>
<p>
Define the function <span class="code">filter_tree</span> which takes in a <span
class="code">tree</span> as an argument and returns the same tree, but with
items included or excluded based on the pred argument.
</p>
<p>
Note that there is ambiguity about what excluding a tree means. For this
function, when you exclude a subtree, you exclude all of its children as well.
</p>
<pre>
&gt;&gt;&gt; a = t.copy()
&gt;&gt;&gt; filter_tree(a, lambda x: x % 2 == 0)
Tree(4, Tree(2, Tree(8), None), None)
&gt;&gt;&gt; a = t.copy()
&gt;&gt;&gt; filter_tree(a, lambda x : x &gt; 2)
Tree(4)
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
  <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
    <p>
      <pre>
def filter_tree(pred, tree):
    if tree and pred(tree.entry):
        left = filter_tree(pred, tree.left)
        right = filter_tree(pred, tree.right)
        return Tree(tree.entry, left, right)
      </pre>
    </p>
  </div>
<?php } ?>

<h2>Binary Search Trees</h2>
<p>
Binary search trees are a special case of regular trees that are useful for
organizing data and searching through it efficiently. Binary search trees have 2
special properties apart from regular trees:
</p>
<ul>
  <li>
  At every node, the node's left children are all less than that node, and the
  node's right children are all greater than to that node.
  </li>
  <li>
  Every node has at most two children.
  </li>
</ul>
<p>
To illustrate this, first, draw these trees out on a piece of paper. Then,
determine if the trees are valid binary search trees. Some are solved for you,
and the rest are given to you as an exercise.
</p>
<p>
Note: str(bst) prints out the left subtree first, and then the right subtree.
</p>

<ol type = "a">
  <li>
    <pre class="codemargin">
VALID
&gt;&gt;&gt; bst_a
Tree(3, Tree(1, Tree(0), None), Tree(5, Tree(4), None))
  </pre>
  </li>
<li>
<pre>INVALID
&gt;&gt;&gt; bst_b
Tree(1, Tree(3, Tree(0), None), Tree(5, Tree(4), None))
    </pre>
  </li>
  <li>
    <pre>
&gt;&gt;&gt; bst_c
Tree(6, Tree(5, Tree(4, Tree(3, Tree(2, Tree(1), None), None), None), None), None)
    </pre>
  </li>
  <li>
    <pre>
&gt;&gt;&gt; bst_d
Tree(5, Tree(4, Tree(1, Tree(2), None), None), Tree(9, Tree(6, Tree(8, Tree(7), None), None), None))
    </pre>
  </li>
</ol>
<h3 class="section_title"> Question 1: </h3>
<p>
Write a function <span class="code">is_valid_bst</span>, that takes a given bst,
and returns <span class="code">True</span> if the bst is a valid binary search
tree; that is, for every node, that node's left children are less than the node,
and that node's right children are greater than that node. You might find the
<span class="code">get_min</span> and <span class="code">get_max</span>
functions helpful. Test your function with the given binary search trees from
above (these are already typed out in the <span class="code">lab9.py</span>
file) to make sure your function works properly.
</p>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
  <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
    <p>
      <pre>
def get_max(bst):
    while bst.right:
        bst = bst.right
    return bst.entry

def get_min(bst):
    while bst.left:
        bst = bst.left
    return bst.entry

def is_valid_bst(bst):
    if bst.left:
        if not is_valid_bst(bst.left):
            return False
        if get_max(bst.left) &gt; bst.entry:
            return False
    if bst.right:
        if not is_valid_bst(bst.right):
            return False
        if get_min(bst.right) &lt; bst.entry:
            return False
    return True
      </pre>
    </p>
  </div>
<?php } ?>

<h3 class="section_title"> Question 2: </h3>
<p>
Write a function <span class="code">bst_find</span> which takes two
arguments, <span class="code">bst</span> and <span class="code">elem</span>.
<span class="code">bst_find</span> should return True if elem is in the tree and
False if it is not.
</p>
<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
  <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
    <p>
      <pre class="codemargin">
def bst_find(bst, elem):
    if not bst:
        return False
    if bst.entry == elem:
        return True
    if bst.entry &gt; elem:
        return bst_find(bst.left, elem)
    return bst_find(bst.right, elem)
      </pre>
    </p>
  </div>
<?php } ?>

<h3 class="section_title"> Question 3: </h3>
<p>
Write a function <span class="code">bst_count_num</span> which takes two
arguments, <span class="code">bst</span> and <span class="code">max</span>.
<span class="code">bst_count_num</span> should return the number of items in the
tree that are less than the given max. Implement <span class="code">bst_count
</span> in the following ways:
<ol>
  <li>
  Using the bst_find function.
  </li>
  <li>
  Using recursion.
  </li>
</ol>
</p>
<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
  <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
    <p>
      <pre class="codemargin">
def bst_count_num_find(bst, max):
    min = get_min(bst)
    num = 0
    while min &lt; max:
        if bst_find(bst, min):
            num += 1
        min += 1
    return num

def bst_count_num_recr(bst, max):
    tot = 0
    if bst.left:
        tot += bst_count_num_recr(bst.left, max)
    if bst.right:
        tot += bst_count_num_recr(bst.right, max)
    if bst.entry &lt; max:
        tot += 1
    return tot
      </pre>
    </p>
  </div>
<?php } ?>

<h3 class="section_title"> Question 4: </h3>
<p>
Write a function <span class="code">is_right_bigger</span>, which takes a single
argument <span class="code">bst</span>, and returns whether or not the right
subtree in this bst is larger (has more items) than the left subtree.
<pre class="codemargin">
&gt;&gt;&gt; is_right_bigger(bst_c)
False
&gt;&gt;&gt; is_right_bigger(bst_d)
True
</pre>
</p>
<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
  <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
    <p>
      <pre>
def is_right_bigger(bst):
    if bst.left and bst.right:
        return size_of_tree(bst.right) &gt; size_of_tree(bst.left)
    return bst.left is None
      </pre>
    </p>
  </div>
<?php } ?>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <script src="http://code.jquery.com/jquery-latest.js"></script>
  <script>
    <?php for ($i = 0; $i < $q_num; $i++) { ?>
    $("#toggleButton<?php echo $i; ?>").click(function () {
      $("#toggleText<?php echo $i; ?>").toggle();
    });
    <?php } ?>
  </script>
<?php } ?>
</body>
</html>
