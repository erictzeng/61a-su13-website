<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta name="description" content ="CS61A: Structure and Interpretation of
  Computer Programs" />
  <meta name="keywords" content ="CS61A, Computer Science, CS, 61A, Programming,
  Berkeley, EECS" />
  <meta name="author" content ="Steven Tang, Eric Tzeng, Albert Wu,
  Mark Miyashita, Robert Huang, Andrew Huang, Brian Hou, Leonard Truong,
  Jeffrey Lu, Rohan Chitnis" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <style type="text/css">@import url("../lab_style.css");</style>
  <style type="text/css">@import url("../61a_style.css");</style>

    <title>CS 61A Spring 2013: Lab 06b</title>

    <?php
    /* So all of the PHP in this file is to allow for this nice little trick to
     * help us avoid having two versions of the questions lying around in the
     * repository, which often leads to the two versions going out of sync which
     * leads to annoyance for students.
     *
     * The idea's pretty simple for the PHP part, just simply have two dates:
     *
     *    1. The current date
     *    2. The date the solutions should be released
     *
     * Using these, we now wrap our solutions in a simple PHP if statement that
     * checks if the date is past the release date and only includes the code on
     * the page displayed (what the server gives back to the browser) if the
     * solutions are supposed to be released.
     *
     * We also use some PHP to create unique IDs for each of the show/hide
     * buttons and solution divs, which are then used in the PHP generated
     * jQuery code that we use to create the nice toggling effect.
     *
     * I apologize if the PHP/jQuery is really offensively bad, this is
     * literally the most I've written of either for a single project so far.
     * Comments/suggestions are most welcome!
     *
     * - Tom Magrino (tmagrino@berkeley.edu)
     */
    $BERKELEY_TZ = new DateTimeZone("America/Los_Angeles");
    $RELEASE_DATE = new DateTime("08/1/2013", $BERKELEY_TZ);
    $CUR_DATE = new DateTime("now", $BERKELEY_TZ);
    $q_num = 0; // Used to make unique ids for all solutions and buttons
    ?>
  </head>

  <body style="font-family: Georgia,serif;">

<h1>CS61A Lab 06b: Exceptions and Calc</h1>
<h3>July 31, 2013</h3>

<p>You can get the starter files by typing this into your terminal:</p>

<pre class='codemargin'>
cp -r ~cs61a/lib/lab/lab06b/starters/* .</pre>

<p>Don't forget the dot at the end!</p>

<h2 class="section_title">Exceptions!</h2>

<p>
We've already seen exceptions in discussion, but let's do a quick
review. Exceptions allow us to try a chunk of code, and then catch any
errors that might come up. If we do catch an exception, we can run
an alternative set of instructions. This construct is very useful in
many situations.</p>

<pre class='codemargin'>
try:
    &lt;try suite&gt;
except Exception as e:
    &lt;except suite&gt;
else:
    &lt;else suite&gt;
finally:
    &lt;finally suite&gt;
</pre>

<p>Notice that we can catch the exception <span class="code">as
e</span>. This assigns the name <span class="code">e</span> to the exception
object. This can be helpful when we want to give extra information on
what happened. For example, we can <span
class="code">print(e)</span> inside the <span class="code">except</span> clause.</p>

<p>Also, we have an optional <span class="code">else</span> case. The
<span class='code'>else</span> suite is executed if the <span
class='code'>try</span> suite finishes without any exceptions.</p>

<p>We also have an optional <span class="code">finally</span> clause,
which is always executed, whether or not an exception is thrown.  We
generally don't need to use the <span class="code">else</span> and
<span class="code">finally</span> controls in this class.</p>

<p> When we write exception statements, we generally don't just use
the word <span class="code">Exception</span> as above. Rather, we
figure out the specific type of exception that we want to handle, such
as <span class="code">TypeError</span> or <span class="code">ZeroDivisionError</span>.
To figure out which type of exception you are trying to handle, you can
type purposely wrong things into the interpreter (such as <span class="code">'hi' + 5</span>
or <span class="code">1 / 0</span>) and see what kind of exception Python spits out.</p>


<h2 class="section_title">Q1</h2>

<p>For practice, let's use exceptions to create a safe <span
class="code">Bank </span> (you can find it in your starter file),
which only stores numerical amounts of money. Fill in the <span
class="code">deposit</span> and <span class="code">withdraw</span>
methods. These methods should only take non-negative numerical
values. If someone tries to pass in an amount that isn't a number,
catch the appropriate exception and print a message informing them
that a numerical argument is required.</p>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
<button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
<div id="toggleText<?php echo $q_num++; ?>" style="display: none">
<pre>
def deposit(self, amount):
    try:
        if amount &lt; 0:
            print("Cannot deposit a negative amount.")
        else:
            self.__balance += amount
    except TypeError:
        print("Must deposit a numerical amount.")

def withdraw(self, amount):
    try:
        if amount &lt; 0:
            print("Cannot withdraw a negative amount.")
        else:
            self.__balance -= amount
    except TypeError:
        print("Must withdraw a numerical amount.")
</pre>
</div>
<?php } ?>

<p>Side note: we can also define our own exceptions! You will see an
example of this in project 4, where a <span
class="code">SchemeError</span> class has been defined for you.</p>

<h2 class="section_title">Calc: our second interpreter!</h2>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>

<p><b>NOTE:</b> Solutions for the rest of the problems can all be
found in these two files.</p>

<p>
<b>Solutions:</b>
<br>
<a href="solutions/calc_soln.py">calc.py</a>
<br>
<a href="solutions/scheme_reader_soln.py">scheme_reader.py</a>
</p>
<?php } ?>


<p>We're continuing our journey through the world of interpreters! We
have seen interpreters before, such as minicalc. Also, that thing you've been running
all of your Python in? That's an interpreter too! In fact, the
Python interpreter we've been using all semester long is really
nothing but a program, albeit a very complex one. You can think of it
as a program that takes strings as input, evaluates them, and prints the
result as output.</p>

<p>There's nothing magical about interpreters, though! They're
programs, just like the one's you've been writing throughout the
entire semester. In fact, it's possible to write a Python interpreter
in Python; <a href="http://pypy.org/">PyPy</a> is proof of that.
However, because Python is a complex language, writing an interpreter
for it would be a very daunting project. Today, we'll play around
with an interpreter for a calculator language, similar to but more complex
than minicalc.</p>

<p>In lecture, you were introduced to
<span class="code">calc</span>, which acts as a simple calculator. You
should have already copied the starter files (<span
class='code'>scheme_reader.py</span>, <span
class='code'>calc.py</span>, etc.) at the start of the lab.</p>

<p>You can also find the code in a zip file <a
href="starters/files.zip">here</a>. You can try running <span
class="code">calc</span> by running this command in the terminal:</p>

<pre class='codemargin'>
python3 calc.py
</pre>

<p>To exit the program, type Ctrl-D or Ctrl-C.</p>

<h2 class="section_title">Q2</h2>
<p>Trace through the code in <span class="code">calc.py</span> that
would be evaluated when you type the following into <span
class="code">calc</span>.</p>

<pre class='codemargin'>
&gt; 2
&gt; (+ 2 3)
&gt; (+ 2 3 4)
&gt; (+ 2 3)
&gt; (+ 2)
&gt; (+ 2 (* 4 5))
</pre>

<h2 class="section_title">Infix notation</h2>
<p>While prefix notation (+ 2 3) is easy for computers to interpret,
it's not very natural for humans. We'd much prefer infix notation (2 +
3). Let's implement this in our own version of calc!</p>

<p>To do this, you need to fill in the following functions, which are
in the <span class='code'>scheme_reader.py</span> file.</p>

<h2 class="section_title">Q3</h2>

<p>Implement <span class='code'>read_infix</span>. This function
takes two arguments: <span class='code'>first</span>, the first
expression in the infix expression; and <span class='code'>src</span>,
the <span class='code'>Buffer</span> of tokens that contains the rest
of the infix expression (and possibly more). For example, if we wanted to construct
<span class='code'>3 + 4 5</span> (note: the 5 will be ignored, so
it's really just <span class='code'>3 + 4</span>), we would call</p>

<pre class='codemargin'>
read_infix(3, Buffer(tokenize_lines('+ 4 5')))</pre>

<p>The return value should be an expression which is mathematically the
same as the infix notation expression (e.g. <span class='code'>+ 3
4</span>), but written using Scheme style <span
class='code'>Pairs</span>. See the doctests for simple examples.
Follow these steps:</p>

<ol>
<li>First, check if there are more tokens left in the <span
class='code'>Buffer</span> Hint: the <span class='code'>Buffer</span>
class in the <span class='code'>buffer.py</span> file has a <span
class='code'>more_on_line</span> property method. If there aren't, we
should just return nil. Also, we would need to return nil if the
<span class='code'>Buffer</span>'s current value (there's already a method that
gives you the current value!) is equal to one of two things. Think about what
these two things would be.</li>

<li>Next, figure out what the operator and second half of the infix
expression should be.</li>

<li>Finally, return a Scheme-style expression which represents the
same thing as the infix notation expression you parsed. Look at the doctests
for specific examples</li>
</ol>

<h2 class="section_title">Q4</h2>

Implement <span class='code'>next_is_op</span>. This function
returns <span class='code'>True</span> if the next token in the given
<span class='code'>Buffer</span> is an operator, and <span
class='code'>False</span> otherwise.</p>

<p><i>Hint</i>: don't forget to check if there are any tokens in the
buffer first. Also, don't remove any tokens from the <span
class='code'>Buffer</span> (i.e. don't use <span
class='code'>pop</span>; think of another method you can use).</p>

<h2 class="section_title">Q5</h2>

<p>Modify <span class='code'>scheme_read</span> to parse infix
expressions. This requires modifying two parts of the code:</p>

<ol>
<li>First, we need to determine if we're dealing with an expression
like "2 + 3". To do this, check if the first item in the <span
class='code'>Buffer</span> is a <span class='code'>float</span> or a
<span class='code'>int</span> (this part's already written). If it is, then check that the next
token is an operator. If it is, read it like an infix expression. (Try to call
methods you've already written!) Otherwise, just return the value (this part's
already written).</li>

<li><p>Next, we have to deal with the case of infix notation inside
parentheses. Without parentheses, <span class='code'>2 + 2 * 3</span>
and <span class='code'>2 * 3 + 2</span> should produce the exact same
result, but in <span class='code'>calc</span>, they don't! <span
class='code'>calc</span> doesn't implement order of operations,
because prefix notation naturally takes care of operator precedence
(why?).</p>

<p>Instead of solving the real problem, we'll implement a quick fix.
If we allow expressions to be surrounded by parentheses, we can
write expressions like <span class='code'>2 + (2 * 3)</span>, which
will evaluate to the same thing as <span class='code'>(2 * 3) +
2</span>.</p>

<p>To do this, we need to change how we parse lists. This logic should
be very similar to what you did in the previous part of <span class='code'>scheme_read</span>.
Hint: The code should be exactly the same as in the previous part, but try to figure out why!</p>
</li>

</ol>

<p>And that's it! We have infix notation! The following inputs to
<span class='code'>calc</span> should work.</p>

<pre class='codemargin'>
&gt; (+ 2 2 * 3)
8
&gt; 2 + (- 5 2)
5
&gt; (+ 1 3 * 4 (* 7 4))
41
&gt; (2 * 3) + 6
12
&gt; 6 + (2 * 3)
12</pre>

<h2 class="section_title">Defining Variables</h2>
<p>Now we're going to add the ability to define variables. For
example:</p>

<pre class='codemargin'>
&gt; (define x 3)
x
&gt; (+ x 2)
5
&gt; (* x (+ 2 x))
15</pre>

<p>For this part, we will be modifying the <span
class='code'>calc.py</span> file. Do we need to change <span
class='code'>calc_eval</span>? <span class='code'>calc_apply</span>?
Is <span class='code'>define</span> a special form?</p>

<h2 class="section_title">Q6</h2>

<p>Implement <span class='code'>do_define_form</span>. This function
takes in a <span class='code'>Pair</span> that contains 2 items: the
variable name, and the expression to which it should be assigned.
<span class='code'>do_define_form</span> should modify the global
environment, <span class='code'>env</span> (a dictionary, defined near
the top of <span class='code'>calc.py</span>) to contain
the name/value binding. It should also return the name of the variable
you're defining.</p>

<h2 class="section_title">Q7</h2>

<p>Finally, implement the lookup procedure in <span
class='code'>calc_eval</span>.
You should check if the given identifier <span class='code'>exp</span> is
in the environment. If it is, then simply return the value associated
with it. If not, you should raise an exception to signal that the user
did something wrong.</p>

<p>
And that's it! There you have basic variable declaration. Isn't that
cool!?? By the way, this was essentially one of the questions on Project 4!</p>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script>
<?php for ($i = 0; $i < $q_num; $i++) { ?>
$("#toggleButton<?php echo $i; ?>").click(function () {
$("#toggleText<?php echo $i; ?>").toggle();
});
<?php } ?>
</script>
<?php } ?>

</body>
</html>
