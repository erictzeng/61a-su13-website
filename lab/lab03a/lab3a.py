"""Starter file for CS 61A Summer 2013 Lab 3a"""


# Q2
def reverse_iter(tup):
    """Returns the reverse of the given tuple.

    >>> reverse_iter((1, 2, 3, 4))
    (4, 3, 2, 1)
    """
    "*** YOUR CODE HERE ***"

def reverse_recursive(tup):
    """Returns the reverse of the given tuple.

    >>> reverse_recursive((1, 2, 3, 4))
    (4, 3, 2, 1)
    """
    "*** YOUR CODE HERE ***"

# Q3
def merge_iter(tup1, tup2):
    """Merges two sorted tuples.

    >>> merge_iter((1, 3, 5), (2, 4, 6))
    (1, 2, 3, ,4 ,5 6)
    >>> merge_iter((), (2, 4, 6))
    (2, 4, 6)
    >>> merge_iter((1, 2, 3), ())
    (1, 2, 3)
    """
    "*** YOUR CODE HERE ***"

def merge_recursive(tup1, tup2):
    """Merges two sorted tuples.

    >>> merge_recursive((1, 3, 5), (2, 4, 6))
    (1, 2, 3, ,4 ,5 6)
    >>> merge_recursive((), (2, 4, 6))
    (2, 4, 6)
    >>> merge_recursive((1, 2, 3), ())
    (1, 2, 3)
    """
    "*** YOUR CODE HERE ***"

# Q4
def deep_len(tup):
    """Returns the deep length of the tuple.

    >>> deep_len((1, 2, 3))
    3
    >>> x = (1, (2, 3), 4)
    >>> deep_len(x)
    4
    >>> y = ((1, (1, 1)), 1, (1, 1))
    >>> deep_len(y)
    6
    """
    "*** YOUR CODE HERE ***"

############################
# RLIST ABSTRACT DATA TYPE #
############################

empty_rlist = None

def rlist(first, rest=empty_rlist):
    return (first, rest)

def first(rlist):
    return rlist[0]

def rest(rlist):
    return rlist[1]


# Q5
def tup_to_rlist(tup):
    """Converts a tuple to an rlist.

    >>> tup = (1, 2, 3, 4)
    >>> r = tup_to_rlist(tup)
    >>> first(r)
    1
    >>> first(rest(rest(r)))
    3
    >>> r = tup_to_rlist(())
    >>> r is empty_rlist
    True
    """
    "*** YOUR CODE HERE ***"

# Q6
def len_rlist(lst):
    """Returns the length of th e rlist.

    >>> lst = tup_to_rlist((1, 2, 3, 4))
    >>> len_rlist(lst)
    4
    >>> lst = tup_to_rlist(())
    >>> len_rlist(lst)
    0
    """
    "*** YOUR CODE HERE ***"

def getitem_rlist(i, lst):
    """Returns the ith item in the rlist. If the index exceeds the
    length of the rlist, return 'Error'.

    >>> lst = tup_to_rlist((1, 2, 3, 4))
    >>> getitem_rlist(0, lst)
    1
    >>> getitem_rlist(3, lst)
    4
    >>> getitem_rlist(4, lst)
    'Error'
    """
    "*** YOUR CODE HERE ***"

# Q7
# Remove the triple quotes surrounding the code below,
# then run the doctests for Q5 and Q6 to test for data abstraction
# violations.
"""
empty_rlist = lambda x: x

def rlist(first, rest=empty_rlist):
    return lambda x: first if x == 'hi' else rest

def first(lst):
    return lst('hi')

def rest(lst):
    return lst('lol')
"""
