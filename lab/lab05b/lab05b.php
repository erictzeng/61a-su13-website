<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta name="description" content ="CS61A: Structure and Interpretation of
  Computer Programs" />
  <meta name="keywords" content ="CS61A, Computer Science, CS, 61A, Programming,
  Berkeley, EECS" />
  <meta name="author" content ="Steven Tang, Eric Tzeng, Albert Wu,
  Mark Miyashita, Robert Huang, Andrew Huang, Brian Hou, Leonard Truong,
  Jeffrey Lu, Rohan Chitnis" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <style type="text/css">@import url("../lab_style.css");</style>
  <style type="text/css">@import url("../61a_style.css");</style>

  <title>CS 61A Summer 2013: Lab 05b</title>

  <?php
  /* So all of the PHP in this file is to allow for this nice little trick to
   * help us avoid having two versions of the questions lying around in the
   * repository, which often leads to the two versions going out of sync which
   * leads to annoyance for students.
   *
   * The idea's pretty simple for the PHP part, just simply have two dates:
   *
   *    1. The current date
   *    2. The date the solutions should be released
   *
   * Using these, we now wrap our solutions in a simple PHP if statement that
   * checks if the date is past the release date and only includes the code on
   * the page displayed (what the server gives back to the browser) if the
   * solutions are supposed to be released.
   *
   * We also use some PHP to create unique IDs for each of the show/hide
   * buttons and solution divs, which are then used in the PHP generated
   * jQuery code that we use to create the nice toggling effect.
   *
   * I apologize if the PHP/jQuery is really offensively bad, this is
   * literally the most I've written of either for a single project so far.
   * Comments/suggestions are most welcome!
   *
   * - Tom Magrino (tmagrino@berkeley.edu)
   */
  $BERKELEY_TZ = new DateTimeZone("America/Los_Angeles");
  $RELEASE_DATE = new DateTime("07/25/2013", $BERKELEY_TZ);
  $CUR_DATE = new DateTime("now", $BERKELEY_TZ);
  $q_num = 0; // Used to make unique ids for all solutions and buttons
  ?>
</head>

<body style="font-family: Georgia,serif;">

<h1>CS61A Lab 5b: Scheme</h1>
<h3>July 24, 2013</h3>

<p>
  Scheme is a famous functional programming language from the 1970s.  It is a
  dialect of Lisp (which stands for LISt Processing).  The first observation
  most people make is the unique syntax, which uses Polish-prefix notation and
  (often many) nested parenthesis.  (See: <a href="http://xkcd.com/297/">
  http://xkcd.com/297/</a>).  Odd as the syntax may seem, it allows us to write,
  with relative ease, something very beautiful, called a
  <a href="http://en.wikipedia.org/wiki/Meta-circular_evaluator">
  metacircular evaluator</a>.  We will see this later in the course.  Scheme
  also features first-class functions and optimized tail-recursion, which were
  relatively new features at the time.
</p>

<p>
  We have provided a starter file, which you can copy into your directory with
  the command:
<pre class="codemargin">
cp ~cs61a/lib/lab/lab05b/lab05b.scm .
</pre>
</p>

<h2>Primitives and Functions</h2>
<p>
  Let's take a look at the primitives in Scheme. Open up the Scheme interpreter
  in your terminal with the command <span class="code">stk</span>, and try
  typing in the following expressions to see what Python would print.
</p>

<pre class="codemargin">
STk> 1          ;Anything following a ';' is a comment
?
STk> 1.0
?
STk> -27
?
STk> #t         ;True
?
STk> #f         ;False
?
STk> "A string"
?
STk> 'symbol
?
STk> nil
()
</pre>

<p>
  Of course, what kind of programming language would Scheme be if it didn't have
  any functions? Scheme uses Polish prefix notation, where the operator comes
  before the operands. For example, to evaluate <span class="code">3 + 4</span>,
  we would type into the Scheme interpreter:

<pre class="codemargin">
STk> (+ 3 4)
</pre>

  Notice that to <i>call</i> a function we need to enclose it in parenthesis,
  with its arguments following. Be careful about this, as while in Python an
  extra set of parentheses won't hurt, in Scheme, it will interpret the
  parentheses as a function call. Evaluating <span class="code">(3)</span>
  results in an error because Scheme tries to call a function called
  <span class="code">3</span> that takes no arguments, which would result in an
  error.
</p>

<p>
  Let's familiarize ourselves with some of the built-in functions in Scheme. Try
  out the following in the interpreter
</p>

<pre class='codemargin'>
STk> (+ 3 5)
?
STk> (- 10 4)
?
STk> (* 7 6)
?
STk> (/ 28 2)
?
STk> (+ 1 2 3 4 5 6 7 8 9)      ;Arithmetic operators allow a variable number of arguments
?
STk> (magnitude -7)             ;Absolute Value
?
STk> (quotient 29 5)
?
STk> (remainder 29 5)
?
STk> (= 1 3)
?
STk> (&gt; 1 3)
?
STk> (&lt; 1 3)
?
STk> (or #t #f)
?
STk> (and #t #t)
?
STk> (and #t #f (/ 1 0))        ;Short-Circuiting
?
STk> (not #t)
?
STk> (define x 3)               ;Defining Variables
STk> x
?
STk> (define y (+ x 4))
STk> y
?
STk> nil
?
</pre>

<p>
  So far, we've only been using Scheme as calculator rather than a programming
  language. To write a program, we need our own functions, so let's define one.
  The syntax for defining a program in Scheme is:
</p>

<pre class='codemargin'>
(define ([<i>name</i>] [<i>args</i>])
        [<i>body</i>])
</pre>

<p>
  For example, let's define the <span class="code">double</span> function:
</p>

<pre class="codemargin">
(define (double x)
        (+ x x))
</pre>

<p>
  Try it yourself! Define a function which cubes an input. You can load your
  definitions into Scheme by entering
  <span class="code">stk -load filename.scm</span> in your terminal, or if you
  have the interpreter already opened up
  <span class="code">(load "filename.scm")</span>.
</p>

<pre class="codemargin">
(define (cube x)
        0)          ;Replace the 0 with your code
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
  <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
    <p>
      <pre>
(define (cube x)
        (* x x x))
      </pre>
    </p>
  </div>
<?php } ?>

<h2>Control and Recursion</h2>

<p>
  So far, we can't do much in our functions so let's introduce control
  statements to allow our functions to do more complex operations!  The
  if-statement has the following format:
</p>

<pre class="codemargin">
(if [<i>condition</i>]
    [<i>true_result</i>]
    [<i>false_result</i>])
</pre>

<p>
  For example, the following code written in Scheme and Python are equivalent:
</p>

<pre class='codemargin'>
;Scheme
(if (&gt; x 3)
    1
    2)

#Python
if x &gt; 3:
    1
else:
    2
</pre>

<p>
  Notice that the if-statement has no <span class="code">elif</span> case. If
  want to have multiple cases with the if-statement, you would need multiple
  branched if-statements:
</p>

<pre class="codemargin">
;Scheme
(if (&lt; x 0)
    (display "Negative")
    (if (= x 0)
        (display "Zero")
        (display "Positive"))

#Python Equivalent
if x &lt; 0:
    print('Negative')
else:
    if x == 0:
        print('Zero')
    else:
        print('Positive')
</pre>

<p>
  But this gets messy as more cases are needed, so alternatively, we also have
  the <span class="code">cond</span> statement, which has a different syntax:
</p>

<pre class="codemargin">
(cond ([<i>condition_1</i>] [<i>result_1</i>])
      ([<i>condition_2</i>] [<i>result_2</i>])
        ...
      ([<i>condition_n</i>] [<i>result_n</i>])
      (else [<i>result_else</i>]))                ;'else' is optional
</pre>

<p>
  With this, we can write control statements with multiple cases neatly and
  without needing branching if-statements.
</p>

<pre class="codemargin">
(cond ((and (&gt; x 0) (= (modulo x 2) 0)) (display "Positive Even Integer"))
      ((and (&gt; x 0) (= (modulo x 2) 1)) (display "Positive Odd Integer"))
      ((= x 0) (display "Zero"))
      ((and (&lt; x 0) (= (modulo x 2) 0)) (display "Negative Even Integer"))
      ((and (&lt; x 0) (= (modulo x 2) 1)) (display "Negative Odd Integer")))
</pre>

<p>
  Now that we have control statements in Scheme, let us revisit a familiar
  problem: factorial.  We will write it using a recursive procedure, but with
  both recursive and iterative <i>processes</i>. Read section 1.2.1 of this
  <a href="http://mitpress.mit.edu/sicp/full-text/book/book-Z-H-11.html">
  link</a> to see what we mean by this.  In short, an iterative process is
  summarized by state variables (for example, a counter and a sum), and update
  and termination rules.  The iterative <i>process</i> can still use an
  underlying recursive procedure.
  <ol>
    <li>
      Write <span class="code">factorial</span> with a recursive process.
    </li>
    <li>
      Write <span class="code">factorial</span> with an iterative process. The
      procedure should still make a recursive call!
    </li>
  </ol>
  In the iterative case, most programming languages consume memory
  proportional to the number of procedure calls.  However, Scheme "will execute
  an iterative process in constant space, even if the iterative process is
  described by a recursive procedure. An implementation with this property is
  called tail-recursive."  We see then that syntax such as "for" and "while"
  loops are not necessary in Scheme; they are merely "syntactic sugar".
</p>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
    <pre class="codemargin">
1.)
(define (factorial_recursive x)
    (if (= x 0)
        1
        (* x (factorial_recursive (- x 1)))))
2.)
(define (factorial_iterative x)
    (fact_iter_helper 1 1 x))

(define (fact_iter_helper prod count max)
    (if (> count max)
        product
        (fact_iter_helper (* prod count) (+ count 1) max)))
  </pre>
    </p>
    </div>
<?php } ?>

<h2>Lists</h2>

<p>
  In Scheme, lists are composed similarly to how Rlists that we had worked with
  in Python were implemented. Lists are made up pairs, which can point to two
  objects. To create a pair, we use the <span class="code">cons</span> function,
  which takes two arguments:
</p>

<pre class="codemargin">
STk> (define a (cons 3 5))
a
STk> a
(3 . 5)
</pre>

<p>
  Note the dot between the <span class="code">3</span> and <span class="code">5
  </span>. The dot indicates that this is a pair, rather than a sequence (as
  you'll see in a bit).
</p>

<p>
  To retrive a value from a pair, we use the <span class="code">car</span> and
  <span class="code">cdr</span> functions to retrieve the first and second
  elements in the pair.
</p>

<pre class="codemargin">
STk> (car a)
3
STk> (cdr a)
5
</pre>

<p>
  Look familiar yet? Like how Rlists are formed, lists in Scheme are formed by
  having the first element of a pair be the first element of the list, and the
  second element of the pair point to another pair containing the rest of list,
  or to <code class="span">nil</code> to signify the end of the list. For
  example, the sequence (1, 2, 3) can be represented in Scheme with the
  following line:
</p>

<pre class="codemargin">
STk> (define a (cons 1 (cons 2 (cons 3 nil))))
</pre>

<p>
  Which creates the following object in Scheme:
</p>

<img src="imgs/list1.png" class='figure'/>

<p>We can then of course retrieve values from our list with the
  <span class="code">car</span> and <span class="code">cdr</span> function.
</p>

<pre class="codemargin">
STk> a
(1 2 3)
STk> (car a)
1
STk> (cdr a)
(2 3)
STk> (car (cdr (cdr a)))
3
</pre>

<p>
  This is not the only way to create a list though. You can also use the
  <span class="code">list</span> function, as well as the quote form to form a
  list as well:
</p>

<pre class="codemargin">
STk> (list 1 2 3)
(1 2 3)
STk> '(1 2 3)
(1 2 3)
STk> '(1 . (2 . (3)))
</pre>

<p>
  There are a few other built-in functions in Scheme that are used for lists:
</p>

<pre class="codemargin">
STk> (define a '(1 2 3 4))
a
STk> (define b '(4 5 6))
b
STk> (define empty ('()))
empty
STk> (append '(1 2 3) '(4 5 6))
(1 2 3 4 5 6)
STk> (length '(1 2 3 4 5))
5
STk> (null? '(1 2 3))             ;Checks whether list is empty.
#f
STk> (null? '())
#t
STk> (null? nil)
#t
</pre>

<ol>
  <li>Create the structure as defined in the picture below.  Check to make sure
    that your solution is correct!</li>
  <img src="imgs/list2.png" class='figure'/>
  <?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
<pre class="codemargin">
(define structure (cons (cons 1 '()) (cons 2 (cons (cons 3 4) (cons 5 '())))))
</pre>
      </p>
    </div>
  <?php } ?>

  <li>Implement a function <span class="code">(remove item lst)</span> that
    takes in a list and returns a new list with <span class="code">item</span>
    removed from lst.</li>
  <?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
<pre class="codemargin">
(define (remove item lst)
    (cond ((null? lst) '())
          ((equal? item (car lst)) (remove item (cdr lst)))
          (else (cons (car lst) (remove item (cdr lst))))))
</pre>
      </p>
    </div>
  <?php } ?>

  <li>Create a function <span class="code">(filter f lst)</span>, which takes a
    predicate function and a list, and returns a new list containing only
    elements of the list that satisfy the predicate.</li>
  <?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
<pre class="codemargin">
(define (filter f lst)
    (cond ((null? lst) '())
          ((f (car lst)) (cons (car lst) (filter f (cdr lst))))
          (else (filter f (cdr lst)))))
</pre>
      </p>
    </div>
  <?php } ?>

  <li>Implement a function <span class="code">(all_satisfies lst pred)</span>
    that returns #t if all of the elements in the list satisfy pred.</li>
  <?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
<pre class="codemargin">
(define (all_satisfies lst pred)
        (= (length (filter pred lst)) (length lst)))
</pre>
      </p>
    </div>
  <?php } ?>
</ol>

<h2>More Scheme</h2>

<p>
  There's a lot to Scheme, which is perhaps too much for this lab. Scheme takes
  a bit of time to get used to, but it's really not much different from any
  other language. The important thing is to just try different things and learn
  through practice. Try typing these into the interpretter!
</p>

<pre class="codemargin">
STk> '(1 . 3)
?
STk> '(1 . (2 . (3)))
?
STk> '(1 . (2 . 3))
?
STk> (+ 1 2 3)
?
STk> (+ . (1 . (2 . (3))))
?
STk> (define (foo a b) (display b))
STk> (foo 2 3)
?
STk> (foo 2 3 4 5 6 7)
?
STk> (define (bar a . b) display b))
STk> (bar 2 3)
?
STk> (bar 4 5 6 7)
?
STk> (bar 10)
?
STk> (first "string")
?
STk> (last "string")
?
STk> (butfirst "string")
?
STk> (butlast "string")
?
STk> (begin (define x 1) (display x) (newline) (define x (+ x 1)) (display x) (newline))
?
STk> (define (filter f lst)
        (if (null? lst)
            lst
            (let ((first (car lst)) (rest (filter f (cdr lst))))
                  (if (f first)
                      (cons first rest)
                      rest))))
STk> (filter (lambda (x) (> x 5)) '(3 4 5 6 7))
?
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <script src="http://code.jquery.com/jquery-latest.js"></script>
  <script>
    <?php for ($i = 0; $i < $q_num; $i++) { ?>
    $("#toggleButton<?php echo $i; ?>").click(function () {
      $("#toggleText<?php echo $i; ?>").toggle();
    });
    <?php } ?>
  </script>
<?php } ?>
</body>
</html>
