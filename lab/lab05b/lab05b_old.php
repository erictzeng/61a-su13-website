<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <meta name="description" content ="CS61A: Structure and Interpretation of Computer Programs" />
    <meta name="keywords" content ="CS61A, Computer Science, CS, 61A, Programming, Berkeley, EECS" />
    <meta name="author" content ="Amir Kamil, Hamilton Nguyen, Joy Jeng, Keegan Mann, Stephen Martinis, Albert Wu, Julia Oh, Robert Huang, Mark Miyashita, Sharad Vikram, Soumya Basu, Richard Hwang" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">@import url("https://inst.eecs.berkeley.edu/~cs61a/su12/lab/lab_style.css");</style>

    <title>CS 61A Spring 2013: Lab 9</title>

    <?php
    /* So all of the PHP in this file is to allow for this nice little trick to
     * help us avoid having two versions of the questions lying around in the
     * repository, which often leads to the two versions going out of sync which
     * leads to annoyance for students.
     *
     * The idea's pretty simple for the PHP part, just simply have two dates:
     *
     *    1. The current date
     *    2. The date the solutions should be released
     *
     * Using these, we now wrap our solutions in a simple PHP if statement that
     * checks if the date is past the release date and only includes the code on
     * the page displayed (what the server gives back to the browser) if the
     * solutions are supposed to be released.
     *
     * We also use some PHP to create unique IDs for each of the show/hide
     * buttons and solution divs, which are then used in the PHP generated
     * jQuery code that we use to create the nice toggling effect.
     *
     * I apologize if the PHP/jQuery is really offensively bad, this is
     * literally the most I've written of either for a single project so far.
     * Comments/suggestions are most welcome!
     *
     * - Tom Magrino (tmagrino@berkeley.edu)
     */
    $BERKELEY_TZ = new DateTimeZone("America/Los_Angeles");
    $RELEASE_DATE = new DateTime("04/04/2013", $BERKELEY_TZ);
    $CUR_DATE = new DateTime("now", $BERKELEY_TZ);
    $q_num = 0; // Used to make unique ids for all solutions and buttons
    ?>
  </head>

  <body style="font-family: Georgia,serif;">

<h1>CS61A Lab 9: Scheme</h1>
<h3>Week 11, 2013</h3>
<p>You can get the starter file like this: cp ~cs61a/lib/lab/lab09/lab9.scm .</p>
<h3 class="section_title">What is Scheme</h3>
<p><img height=100 src="lambda.svg" /></p>
<p>Scheme is a famous functional programming language from the 1970s.  It is a
dialect of Lisp (which stands for LISt Processing).  The first observation
most people make is the unique syntax, which uses Polish-prefix notation and
(often many) nested parenthesis.  (See: <a href="http://xkcd.com/297/">
http://xkcd.com/297/</a>).  Odd as the syntax may seem, it allows us to write,
with relative ease, something very beautiful, called a
<a href="http://en.wikipedia.org/wiki/Meta-circular_evaluator">
metacircular evaluator</a>.  We will see this later in the course.  Scheme
also features first-class functions and optimized tail-recursion, which were
relatively new features at the time.</p>

<p>Enough with all this talk though; let's see some code.  You may interact
with the Scheme interpeter by typing <span class="code">stk</span> on a
terminal of one of the instructional machines.</p>

<p>We have primitives...</p>

<pre>
STk> 1 ;This is a comment
1

STk> "word"
"word"

STk> 'symbol
symbol

STk> #t
#t
</pre>

<p>And we have functions.<p>

<pre>
STk> +
#[closure arglist=args 196920]

STk> (+ 1 2)
3

STk> (+ (* 1 1) 2)
3
</pre>

<p>Notice that to <i>call</i> a function we need to enclose it in parenthesis,
with its arguments following.  Syntax for other familiar programming
constructs follow this format.  (Can you see why it is very easy to translate
this syntax into trees?  Try drawing out trees for a few instructions)</p>

<p>Let's familiarize ourselves with some of the built-in functions in Scheme, 
starting with arithmetic operations. Arithmetic operations are rather simple, as
they simply use require a function call with the prefixed operator, followed
by the arguments.</p>

<pre>
STk> (+ 3 5)
8
STk> (- 10 4)
6
STk> (* 7 6)
42
STk> (/ 28 2)
14
STk> (+ 1 2 3 4 5 6 7 8 9)      ;Allows a variable number of arguments
55
STk> (magnitude -7)             ;Absolute Value
7
STk> (modulo 29 5)              ;Modulus Operator
4
STk> (= 1 3)
#f
STk> (&gt; 1 3)
#f
STk> (&lt; 1 3)
#t
</pre>

<p>So far, Scheme has been just a calculator. With really strange syntax. To 
write a program, we need to functions, so let's define one. The syntax for
defining a program in Scheme is:</p>

<pre>

</pre>

<p></p>

<pre>
STk> (if (> 1 0) 1 2)
1


STk> (define (square x) (* x x))
square

STk> (square 3)
9

STk> (define square (lambda (x) (* x x)))
square

STk> (square 3)
9


STk> (define x (cons 1 (cons 2 '())))
x

STk> (car x)
1

STk> (car (cdr x))
2

# What do cons, car, and cdr remind you of?
</pre>

<p>Here we have seen if expressions, function definitions, lambdas, name
binding, list constructing, and list selecting.  Of course there are many
other things Scheme can do.  For a useful (but not exhaustive) list of
Scheme primitive functions, see
<a href="http://www.cs.berkeley.edu/~bh/ssch27/appendix-funlist.html">this</a>.
</p>

<h3 class="section_title">What Would Scheme Print?</h3>
<p>Predict what will happen and then try it out.  Make sure you understand
what is happening!  Remember, despite the different syntax, Scheme is just
another programming language, like Python.</p>

<pre>
3

(+ 2 3)

(+ 5 6 7 8)

(+)

(sqrt 16)

(+ (* 3 4) 5)

+

'+

'hello

'(+ 2 3)

'(good morning)

(first 274)

(butfirst 274)

(first 'hello)

(first hello)

(first (bf 'hello))

(+ (first 23) (last 45))

(define pi 3.14159)

pi

'pi

(+ pi 7)
(* pi pi)

(define (square x) (* x x))

(square 5)

(square (+ 2 3))

(define a 3)

(define b (+ a 1))

(+ a b (* a b))

(= a b)

(if (and (> b a) (&lt; b (* a b)))
    b
    a)

(cond ((= a 4) 6)
      ((= b 4) (+ 6 7 a))
      (else 25))

(+ 2 (if (> b a) b a))

(* (cond ((> a b) a)
         ((&lt; a b) b)
         (else -1))
   (+ a 1))

((if (&lt; a b) + -) a b)
</pre>

<p>Finally, just as in Python, you can load a Scheme file and interact
with its definitions.  Simply, type "stk -load filename.scm".  Alternatively,
enter stk and type <span class="code">(load "filename.scm")</span>. You may
exit the interpreter by typing Ctrl-D, (bye), or (exit).</p>

<p>Note: many of the following problems will be taken from
<a href="http://mitpress.mit.edu/sicp/">SICP</a>, a famous and influential
introductory computer science book from MIT.</p>

<p>Another Note: if you have any questions on syntax, please try to look it
up yourself before asking your TA.  One of the hopes of this course is that
you are exposed to the main ideas in programming.  Once you understand the key
concepts, different languages should be easy to pick up.  Many features in
Python have an analogue in Scheme; they just look different.</p>

<h3 class="section_title">Warm Up</h3>
<p>To familiarize ourselves with Scheme, let us revisit a familiar problem:
factorial.  We will write it using a recursive procedure, but with both
recursive and iterative <i>processes</i>. Read section 1.2.1 of this
<a href="http://mitpress.mit.edu/sicp/full-text/book/book-Z-H-11.html">
link</a> to see what we mean by this.  In short, an iterative process is summarized
by state variables (for example, a counter and a sum), and update and
termination rules.  The iterative <i>process</i> can still use an underlying
recursive procedure.</p>
<p>1.) Write factorial with a recursive process.</p>
<p>2.) Write factorial with an iterative process (the procedure should
still make a recursive call!).</p>
<p>In the iterative case, most programming languages consume memory
proportional to the number of procedure calls.  However, Scheme "will execute
an iterative process in constant space, even if the iterative process is
described by a recursive procedure. An implementation with this property is
called tail-recursive."  We see then that syntax such as "for" and "while"
loops are not necessary in Scheme; they are merely "syntactic sugar".</p>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
	  <pre class="codemargin">
1.)
(define (factorial_recursive x)
    (if (= x 0)
        1
        (* x (factorial_recursive (- x 1)))))
2.)
(define (factorial_iterative x)
    (fact_iter_helper 1 1 x))

(define (fact_iter_helper prod count max)
    (if (> count max)
        product
        (fact_iter_helper (* prod count) (+ count 1) max)))
	</pre>
	  </p>
    </div>
    <?php } ?>

<h3 class="section_title">Higher Order Functions</h3>
<p>In Scheme, functions are first-class, meaning they can be passed around
just like any other data type.  In particular, this means more higher order
function fun.</p>

<!-- Simpson's Rule -->
<p>1.) A common numerical calculation we use computers to approximate is
integration.  Simpson's rule is one such approximation method.  It is given by
the formula below.</p>

<p>h/3 * [y<sub>0</sub> + 4y<sub>1</sub> + 2y<sub>2</sub> + 4y<sub>3</sub> ...
+ 2y<sub>n-2</sub> + 4y<sub>n-1</sub> + y<sub>n</sub>]</p>

<p>where h = (b-a)/n, n is even, and y<sub>k</sub> = f(a + kh).</p>
<p>Implement a function that takes in f, a, b, and n, and returns the
approximation of the integration of f given by Simpson's Rule.  Test this out
with the cube function between 0 and 1, with n equal to 100.  You should get
0.25. Hint: sum (a function we provide) and let (a Scheme feature) may be
useful.  (Exercise 1.29 SICP)</p>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
	  <pre class="codemargin">
(define (simpson_approx f a b n)
  (let ((h (/ (- b a) n)))
    (define (xi i) (+ a (* i h)))
    (define (next i) (+ i 1))
    (define (term i)
      (cond ((or (= i 0) (= i n)) (f (xi i)))
            ((odd? i) (* 4 (f (xi i))))
            (else (* 2 (f (xi i))))))
    (if (even? n)
        (* (sum term 0 next n) (/ h 3)))))
	</pre>
	  </p>
    </div>
    <?php } ?>

<!-- Product -->
<p>2.) Write a function product analogous to sum, that returns the product
of values of a function over a range.  Write it recursively and iteratively.
(Exercise 1.31 SICP)</p>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
	  <pre class="codemargin">
(define (product_recursive term a next b)
    (if (> a b)
        1
        (* (term a) (product_recursive term (next a) next b))))

(define (product_iterative term a next b)
    (iter_helper term a next b 1))

(define (iter_helper term a next b result)
    (if (> a b)
        result
        (iter_helper term (next a) next b (* result (term a)))))
	</pre>
	  </p>
    </div>
    <?php } ?>

<!-- Accumulate -->
<p>3.) As you may have noticed, sum and product are both specific versions
of general accumulator functions.  Write an accumulate function with the
following function signature:</p>
<pre class="codemargin">(accumulate combiner null-value term a next b)</pre>
<p>Define sum and product in terms of accumulate. (Exercise 1.32 SICP)</p>
<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
	  <pre class="codemargin">
(define (accumulate combiner null-value term a next b)
    (if (> a b)
        null-value
        (combiner
            (term a)
            (accumulate combiner null-value term (next a) next b))))

(define (sum_by_accum term a next b) (accumulate + 0 term a next b))
(define (product_by_accum term a next b) (accumulate * 1 term a next b))
	</pre>
	  </p>
    </div>
    <?php } ?>

<h3 class="section_title">Lists</h3>
<p>The main data structure of Scheme is the humble pair!  From this, we can
form a myriad of structures, including as you hopefully noticed above, Rlists.
To create a pair, we use <span class="code">cons</span>.  To get the first we
use <span class="code">car</span>, and to get the rest we use
<span class="code">cdr</span>.  The symbol <span class="code">'()</span>
represents the empty Rlist.</p>
<!-- Make structure in picture -->
<p>1.) Create the structure in the picture below.  Check that your solution
is correct!</p>
<img height=200 src="list.png" />

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
	  <pre class="codemargin">
(define structure (cons (cons 1 '()) (cons 2 (cons (cons 3 4) (cons 5 '())))))
	</pre>
	  </p>
    </div>
    <?php } ?>

<!-- Interleave -->
<p>2.) Implement a function <span class="code">(interleave lst1 lst2)</span>
that interleaves two lists.</p>
<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
	  <pre class="codemargin">
(define (interleave lst1 lst2)
    (cond ((null? lst1) lst2)
          ((null? lst2) lst1)
          (else (cons (car lst1) (interleave lst2 (cdr lst1))))))
	</pre>
	  </p>
    </div>
    <?php } ?>

<!-- Remove -->
<p>3.) Implement a function <span class="code">(remove item lst)</span> that
takes in a list and returns a new list with item removed from lst.</p>
<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
	  <pre class="codemargin">
(define (remove item lst)
    (cond ((null? lst) '())
          ((equal? item (car lst)) (remove item (cdr lst)))
          (else (cons (car lst) (remove item (cdr lst))))))
	</pre>
	  </p>
    </div>
    <?php } ?>

<!-- All-satisfies -->
<p>4.) Implement a function <span class="code">(all_satisfies lst pred)</span>
that returns #t iff all of the elements in the list satisfy pred.</p>
<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
	  <pre class="codemargin">
(define (all_satisfies lst pred) (= (length (filter pred lst)) (length lst)))
	</pre>
	  </p>
    </div>
    <?php } ?>

<!-- Repeat -->
<p>5.) Implement a function <span class="code">(repeat_els pred lst)</span>
that repeats all elements in the lst that satisfy pred.  Use HOFs.</p>
<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
	  <pre class="codemargin">
(define (repeat_els lst pred)
    (reduce append (map (lambda (n) (if (pred n) (list n n) (list n))) lst)))
	</pre>
	  </p>
    </div>
    <?php } ?>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <script src="http://code.jquery.com/jquery-latest.js"></script>
<script>
<?php for ($i = 0; $i < $q_num; $i++) { ?>
$("#toggleButton<?php echo $i; ?>").click(function () {
  $("#toggleText<?php echo $i; ?>").toggle();
});
<?php } ?>
</script>
    <?php } ?>

</body>
</html>
