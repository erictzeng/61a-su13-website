; Your name
; TA name
; CS61A Lab 5b

; Cubes the input x
(define (cube x)
    0)

; Calculates the factorial of x recursively
(define (factorial_recursive x)
    0)

; Calculates the factorial of x iterativel
(define (factorial_iterative x)
    0)

; Lists #1
(define structure 0)

; Lists #2
(define (remove item lst)
    (cons 0 0))

; Lists #3
(define (filter f lst)
    (cons 0 0))

; Lists #4
(define (all_satisfies lst pred)
    #t)
