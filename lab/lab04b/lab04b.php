<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <meta name="description" content ="CS61A: Structure and Interpretation of
    Computer Programs"/>
    <meta name="keywords" content ="CS61A, Computer Science, CS, 61A,
    Programming, Steven Tang and Eric Tzeng, Berkeley, EECS" />
    <meta name="author" content ="Steven Tang, Eric Tzeng, Albert Wu, Mark
    Miyashita, Robert Huang, Andrew Huang, Brian Hou, Leonard Truong, Jeffrey
    Lu, Rohan Chitnis" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">@import url("../lab_style.css");</style>
    <style type="text/css">@import url("../61a_style.css");</style>

    <title>CS 61A Spring 2013: Lab 4b</title>

    <?php
    /* So all of the PHP in this file is to allow for this nice little trick to
     * help us avoid having two versions of the questions lying around in the
     * repository, which often leads to the two versions going out of sync which
     * leads to annoyance for students.
     *
     * The idea's pretty simple for the PHP part, just simply have two dates:
     *
     *    1. The current date
     *    2. The date the solutions should be released
     *
     * Using these, we now wrap our solutions in a simple PHP if statement that
     * checks if the date is past the release date and only includes the code on
     * the page displayed (what the server gives back to the browser) if the
     * solutions are supposed to be released.
     *
     * We also use some PHP to create unique IDs for each of the show/hide
     * buttons and solution divs, which are then used in the PHP generated
     * jQuery code that we use to create the nice toggling effect.
     *
     * I apologize if the PHP/jQuery is really offensively bad, this is
     * literally the most I've written of either for a single project so far.
     * Comments/suggestions are most welcome!
     *
     * - Tom Magrino (tmagrino@berkeley.edu)
     */
    $BERKELEY_TZ = new DateTimeZone("America/Los_Angeles");
    $RELEASE_DATE = new DateTime("07/18/2013", $BERKELEY_TZ);
    $CUR_DATE = new DateTime("now", $BERKELEY_TZ);
    $q_num = 0; // Used to make unique ids for all solutions and buttons
    ?>
  </head>

  <body style="font-family: Georgia,serif;">

<h1>CS61A Lab 4b: Mutable Recursive Lists and Type Dispatching</h1>

<p>You can get starter code for this lab by typing this into your
terminal:</p>

<pre class='codemargin'>
cp ~cs61a/lib/lab/lab04b/lab4b.py .
</pre>

<p>Don't forget the dot at the end!</p>

<h3 class="section_title">Recursive Lists</h3>

<p>In lecture, we introduced the OOP version of an Rlist:</p>
<pre class='codemargin'>
class Rlist(object):
    class EmptyList(object):
        def __len__(self):
            return 0

    empty = EmptyList()

    def __init__(self, first, rest=empty):
        self.first = first
        self.rest = rest
</pre>

<p>Just like before, these Rlists have a
<span class='code'>first</span> and a <span class='code'>rest</span>.
The difference is that, now, the Rlists are <b>mutable</b>.

<p>A couple of things to notice:</p>
<ol>
  <li><b>Empty Rlists</b>: to check if an Rlist is empty, compare it
  against the class variable <span class='code'>Rlist.empty</span>:
  <pre class='codemargin'>
if rlist is Rlist.empty:
    return 'This rlist is empty!'</pre>
  Don't construct another <span class='code'>EmptyList</span>!

  <li>By definition, <span class='code'>Rlist.empty</span> has a length
  of 0:
  <pre class='codemargin'>
&gt;&gt;&gt; len(Rlist.empty)
0</pre>
</ol>

<p><b>0)</b> What would Python print?</p>
<pre class='codemargin'>
&gt;&gt;&gt; Rlist(1, Rlist.empty)
______
&gt;&gt;&gt; Rlist(1)
______
&gt;&gt;&gt; Rlist()
______

&gt;&gt;&gt; rlist = Rlist(1, Rlist(2, Rlist(3)))
&gt;&gt;&gt; rlist.first
______
&gt;&gt;&gt; type(rlist.rest)
______
&gt;&gt;&gt; rlist.rest.first
______
&gt;&gt;&gt; rlist.first.rest
______
&gt;&gt;&gt; rlist.rest.rest.rest is Rlist.empty
______

&gt;&gt;&gt; rlist.first = 9001
&gt;&gt;&gt; rlist.first
______
&gt;&gt;&gt; rlist.rest = rlist.rest.rest
&gt;&gt;&gt; rlist.rest.first
______
&gt;&gt;&gt; rlist = Rlist(1)
&gt;&gt;&gt; rlist.rest = rlist
&gt;&gt;&gt; rlist.rest.rest.rest.rest.first
______
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
  <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
  <pre class="codemargin">
1) &lt;Rlist object at ...&gt;
2) &lt;Rlist object at ...&gt;
3) TypeError

4) 1
5) &lt;class 'Rlist'&gt;
6) 2
7) AttributeError
8) True

9)  9001
10) 3
11) 1
</pre>
</div>
<?php } ?>

<p><b>1)</b> Write a function <span class='code'>rlist_to_list</span>
that converts a given Rlist to a Python list.</p>

<pre class='codemargin'>
def rlist_to_list(rlist):
    """Takes an RLIST and returns a Python list with the same
    elements.

    &gt;&gt;&gt; rlist = Rlist(1, Rlist(2, Rlist(3, Rlist(4))))
    &gt;&gt;&gt; rlist_to_list(rlist)
    [1, 2, 3, 4]
    &gt;&gt;&gt; rlist_to_list(Rlist.empty)
    []
    """
    "*** YOUR CODE HERE ***"
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
  <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
  <pre class="codemargin">
# recursive
def rlist_to_list(rlist):
    if rlist is Rlist.empty:
        return []
    result, rest = [rlist.first], rlist_to_list(rlist.rest)
    result.extend(rest)
    return result

# iterative
def rlist_to_list(rlist):
    result = []
    while rlist is not Rlist.empty:
        result.append(rlist.first)
        rlist = rlist.rest
    return result
</pre>
</div>
<?php } ?>

<p><b>2)</b> In lecture, we learned about the special "underscore"
methods for classes, two of which were
<span class='code'>__len__</span>, which allows you to call the builtin
function <span class='code'>len</span> on the object, and
<span class='code'>__getitem__</span>, which allows you to use index
notation on the object. Implement both of them for the
<span class='code'>Rlist</span> class. Afterwards, the doctests for the
<span class='code'>Rlist</span> class should pass.</p>

<pre class='codemargin'>
class Rlist(object):
    """A mutable rlist class.

    &gt;&gt;&gt; r = Rlist(3, Rlist(2, Rlist(1)))
    &gt;&gt;&gt; len(r)
    3
    &gt;&gt;&gt; len(r.rest)
    2
    &gt;&gt;&gt; r[0]
    3
    &gt;&gt;&gt; r[1]
    2
    """
    # previous stuff here

    def __len__(self):
        "*** YOUR CODE HERE ***"

    def __getitem__(self, index):
        "*** YOUR CODE HERE ***"</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
  <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
  <pre class="codemargin">
# recursive
def __len__(self):
    return 1 + len(self.rest)   # figure out where the base case is

# iterative
def __len__(self):
    size, cur = 0, self
    while cur is not Rlist.empty:
        size += 1
        cur = cur.rest
    return size

# recursive
def __getitem__(self, index):
    if index == 0:
        return self.first
    elif self.rest is Rlist.empty:
        print('Index out of bounds')  # or some form of error checking
    else:
        return self.rest[index - 1]

# iterative
def __getitem__(self, index):
    cur = self
    while index &gt; 0 and cur is not Rlist.empty:
        cur = cur.rest
        index -= 1
    if cur is Rlist.empty:
        print('Index out of bounds')  # or some form of error checking
    else:
        return cur.first
</pre>
</div>
<?php } ?>

<p><b>3)</b> In addition, it would be nice if we could have a
human-readable string representation of an Rlist, so that we don't have
to stare at this all the time:</p>

<pre class='codemargin'>
&gt;&gt;&gt; Rlist(1, Rlist(2, Rlist(3)))
&lt;Rlist object at ...&gt;</pre>

<p>Implement a <span class='code'>__repr__</span> method to fix
this. Write the <span class='code'>__repr__</span> to match the
following representations:</p>

<pre class='codemargin'>
&gt;&gt;&gt; Rlist(1, Rlist(2, Rlist(3)))
Rlist(1, Rlist(2, Rlist(3)))
&gt;&gt;&gt; Rlist(1)
Rlist(1)
&gt;&gt;&gt; repr(Rlist(1))
'Rlist(1)'</pre>

<p>Remember, <span class='code'>__repr__</span> should return a string!
</p>

<pre class='codemargin'>
class Rlist(object):

    # previous stuff here

    def __repr__(self):
        "*** YOUR CODE HERE ***"</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
  <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
  <pre class="codemargin">
def __repr__(self):
    if self.rest is Rlist.empty:
        return 'Rlist(' + str(self.first) + ')'
    return 'Rlist(' + str(self.first) + ', ' + repr(self.rest) + ')'
</pre>
</div>
<?php } ?>

<p><b>4)</b> Implement a function <span class='code'>insert</span>
that takes an Rlist, a value, and an index, and inserts the value into
the Rlist at the given index. You can assume the rlist already has at
least one element. Do NOT return anything --
<span class='code'>insert</span> should mutate the rlist.</p>

<pre class='codemargin'>
def insert(rlist, value, index):
    """Insert VALUE into the the RLIST at the given INDEX.

    &gt;&gt;&gt; rlist = Rlist(1, Rlist(2, Rlist(3)))
    &gt;&gt;&gt; insert(rlist, 9001, 0)
    &gt;&gt;&gt; rlist
    Rlist(9001, Rlist(1, Rlist(2, Rlist(3))))
    &gt;&gt;&gt; insert(rlist, 100, 2)
    &gt;&gt;&gt; rlist
    Rlist(9001, Rlist(1, Rlist(100, Rlist(2, Rlist(3)))))
    """
    "*** YOUR CODE HERE ***"
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
  <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
  <pre class="codemargin">
# recursive
def insert(rlist, value, index):
    if index == 0:
        rlist.rest = Rlist(rlist.first, rlist.rest)
        rlist.first = value
    elif rlist.rest is Rlist.empty:
        print('Index out of bounds')
    else:
        insert(rlist.rest, value, index - 1)

# iterative
def insert(rlist, value, index):
    while index &gt; 0 and rlist.rest is not Rlist.empty:
        rlist = rlist.rest
        index -= 1
    if index == 0:
        rlist.rest = Rlist(rlist.first, rlist.rest)
        rlist.first = value
    else:
        print('Index out of bounds')
</pre>
</div>
<?php } ?>

<p><b>5)</b> Implement a function <span class='code'>reverse</span>
that reverses a given Rlist. <span class='code'>reverse</span> should
return the reversed Rlist (it doesn't matter if you created a new Rlist
or mutated the original).</p>

<p><b>Extra for experts</b>: Implement
<span class='code'>reverse</span> without calling the Rlist
constructor.</p>

<pre class='codemargin'>
def reverse(rlist):
    """Returns an Rlist that is the reverse of the original.

    &gt;&gt;&gt; Rlist(1).rest is Rlist.empty
    True
    &gt;&gt;&gt; rlist = Rlist(1, Rlist(2, Rlist(3)))
    &gt;&gt;&gt; reverse(rlist)
    Rlist(3, Rlist(2, Rlist(1)))
    &gt;&gt;&gt; reverse(Rlist(1))
    Rlist(1)
    """
    "*** YOUR CODE HERE ***"</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
  <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
  <pre class="codemargin">
# iterative
def reverse(rlist):
    new = Rlist(rlist.first)
    while rlist.rest is not Rlist.empty:
        rlist = rlist.rest
        new = Rlist(rlist.first, new)
    return new

# recursive (and extra for experts)
def reverse(rlist):
    if rlist.rest is not Rlist.empty:
        second, last = rlist.rest, rlist
        rlist = reverse(second)
        second.rest, last.rest = last, Rlist.empty
    return rlist
</pre>
</div>
<?php } ?>

<h3 class="section_title">Type Dispatching</h3>

<p>Recall the tag-based type-dispatching introduced in lecture:</p>

<pre class="codemargin">
def type_tag(x):
    return type_tag.tags[type(x)]

type_tag.tags = {
    &lt;type1&gt; : &lt;tag1&gt;
    &lt;type2&gt; : &lt;tag2&gt;
    ...
}</pre>

<p>To create a function that would add different types together, we
followed this pattern:</p>

<pre class='codemargin'>
type_tag.tags = {
    ComplexRI : 'com',
    ComplexRA : 'com',
    Rational  : 'rat',
}

def add(z1, z2):
    types = (type_tag(z1), type_tag(z2))
    return add.impl[types](z1, z2)

add.impl = {}
add.impl[('com', 'com')] = lambda z1, z2: ...
add.impl[('rat', 'com')] = lambda z1, z2: ...
...
</pre>

<p><b>6)</b> Using this pattern of type dispatching, write a function
<span class='code'>extend</span> that takes two sequences, either
Python lists or Rlists, and adds the elements of the second sequence
to the first (this works like <span class='code'>list.extend</span>).
In the <span class='code'>extend.impl</span> dictionary, you may use
lambdas or actual functions (that you define yourself) as the values.
</p>

<pre class='codemargin'>
def type_tag(x):
    return type_tag.tags[type(x)]

# notice that type_tag is kind of unnecessary here
type_tag.tags = {
    list  : 'list',
    Rlist : 'Rlist',
}

def extend(seq1, seq2):
    """Takes the elements of seq2 and adds them to the end of seq1.

    &gt;&gt;&gt; rlist = Rlist(4, Rlist(5, Rlist(6)))
    &gt;&gt;&gt; lst = [1, 2, 3]
    &gt;&gt;&gt; extend(lst, rlist)
    &gt;&gt;&gt; lst
    [1, 2, 3, 4, 5, 6]
    &gt;&gt;&gt; rlist
    Rlist(4, Rlist(5, Rlist(6)))
    &gt;&gt;&gt; extend(rlist, [7, 8])
    &gt;&gt;&gt; rlist
    Rlist(4, Rlist(5, Rlist(6, Rlist(7, Rlist(8)))))
    &gt;&gt;&gt; extend(lst, [7, 8, 9])
    &gt;&gt;&gt; lst
    [1, 2, 3, 4, 5, 6, 7, 8, 9]
    """
    "*** YOUR CODE HERE ***"

extend.impl = {
  ('list', 'list')   : "*** YOUR CODE HERE ***" ,
  ('list', 'Rlist')  : "*** YOUR CODE HERE ***" ,
  ('Rlist', 'list')  : "*** YOUR CODE HERE ***" ,
  ('Rlist', 'Rlist') : "*** YOUR CODE HERE ***" ,
}
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
  <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
  <pre class="codemargin">
def extend(seq1, seq2):
    types = (type_tag(seq1), type_tag(seq2))
    extend.impl[types](seq1, seq2)

def ex_list_rlist(lst, rlist):
    lst.extend(rlist_to_list(rlist))

def ex_rlist_list(rlist, lst):
    if len(lst) == 0:
        return
    elif rlist.rest is Rlist.empty:
        rlist.rest, lst = Rlist(lst[0]), lst[1:]
    ex_rlist_list(rlist.rest, lst)

def ex_rlist_rlist(rlist1, rlist2):
    if rlist1.rest is Rlist.empty:
        rlist1.rest = rlist2
    else:
        ex_rlist_rlist(rlist1.rest, rlist2)

extend.impl = {
  ('list', 'list')   : lambda lst1, lst2: lst1.extend(lst2),
  ('list', 'Rlist')  : ex_list_rlist,
  ('Rlist', 'list')  : ex_rlist_list,
  ('Rlist', 'Rlist') : ex_rlist_rlist,
}
</pre>
</div>
<?php } ?>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <script src="http://code.jquery.com/jquery-latest.js"></script>
<script>
<?php for ($i = 0; $i < $q_num; $i++) { ?>
$("#toggleButton<?php echo $i; ?>").click(function () {
  $("#toggleText<?php echo $i; ?>").toggle();
});
<?php } ?>
</script>
    <?php } ?>
</body>
</html>
