"""Starter file for CS 61A Summer 2013 Lab 4b"""

class Rlist(object):
    """A mutable rlist class.

    >>> r = Rlist(3, Rlist(2, Rlist(1)))
    >>> len(r)
    3
    >>> len(r.rest)
    2
    >>> r[0]
    3
    >>> r[1]
    2
    """
    class EmptyList(object):
        def __len__(self):
            return 0

    empty = EmptyList()

    def __init__(self, first, rest=empty):
        self.first = first
        self.rest = rest

    # Q2
    def __len__(self):
        "*** YOUR CODE HERE ***"

    # Q2
    def __getitem__(self, index):
        "*** YOUR CODE HERE ***"

    # Q3
    def __repr__(self):
        "*** YOUR CODE HERE ***"

# Q1
def rlist_to_list(rlist):
    """Takes an RLIST and returns a Python list with the same elements.

    >>> rlist = Rlist(1, Rlist(2, Rlist(3, Rlist(4))))
    >>> rlist_to_list(rlist)
    [1, 2, 3, 4]
    >>> rlist_to_list(Rlist.empty)
    []
    """
    "*** YOUR CODE HERE ***"

# Q4
def insert(rlist, value, index):
    """Insert VALUE into RLIST at the given INDEX.

    >>> rlist = Rlist(1, Rlist(2, Rlist(3)))
    >>> insert(rlist, 9001, 0)
    >>> rlist
    Rlist(9001, Rlist(1, Rlist(2, Rlist(3))))
    >>> insert(rlist, 100, 2)
    >>> rlist
    Rlist(9001, Rlist(1, Rlist(100, Rlist(2, Rlist(3)))))
    """
    "*** YOUR CODE HERE ***"

# Q5
def reverse(rlist):
    """Returns an Rlist that is the reverse of the original.

    >>> Rlist(1).rest is Rlist.empty
    True
    >>> rlist = Rlist(1, Rlist(2, Rlist(3)))
    >>> reverse(rlist)
    Rlist(3, Rlist(2, Rlist(1)))
    >>> reverse(Rlist(1))
    Rlist(1)
    """
    "*** YOUR CODE HERE ***"

# Q6
def type_tag(x):
    return type_tag.tags[type(x)]

# notice that type_tag is kind of unnecessary here
type_tag.tags = {
    list  : 'list',
    Rlist : 'Rlist',
}

def extend(seq1, seq2):
    """Takes the elements of seq2 and adds them to the end of seq1.

    >>> rlist = Rlist(4, Rlist(5, Rlist(6)))
    >>> lst = [1, 2, 3]
    >>> extend(lst, rlist)
    >>> lst
    [1, 2, 3, 4, 5, 6]
    >>> rlist
    Rlist(4, Rlist(5, Rlist(6)))
    >>> extend(rlist, [7, 8])
    >>> rlist
    Rlist(4, Rlist(5, Rlist(6, Rlist(7, Rlist(8)))))
    >>> extend(lst, [7, 8, 9])
    >>> lst
    [1, 2, 3, 4, 5, 6, 7, 8, 9]
    """
    "*** YOUR CODE HERE ***"

extend.impl = {
    ('list', 'list')   : "*** YOUR CODE HERE ***" ,
    ('list', 'Rlist')  : "*** YOUR CODE HERE ***" ,
    ('Rlist', 'list')  : "*** YOUR CODE HERE ***" ,
    ('Rlist', 'Rlist') : "*** YOUR CODE HERE ***" ,
}

