<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"> 
  <head>
    <meta name="description" content ="CS61A: Structure and Interpretation of Computer Programs" /> 
    <meta name="keywords" content ="CS61A, Computer Science, CS, 61A, Programming, Berkeley, EECS" /> 
    <meta name="author" content ="Tom Magrino, Jon Kotker, Eric Kim, Steven Tang, Joy Jeng, Stephen Martinis, Allen Nguyen, Albert Wu" /> 
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 
    <style type="text/css">@import url("https://inst.eecs.berkeley.edu/~cs61a/su12/lab/lab_style.css");</style> 
    
    <title>CS 61A Summer 2013: User Interfaces</title>

    <?php
    /* So all of the PHP in this file is to allow for this nice little trick to 
     * help us avoid having two versions of the questions lying around in the 
     * repository, which often leads to the two versions going out of sync which 
     * leads to annoyance for students.
     *
     * The idea's pretty simple for the PHP part, just simply have two dates: 
     *
     *    1. The current date
     *    2. The date the solutions should be released
     *
     * Using these, we now wrap our solutions in a simple PHP if statement that 
     * checks if the date is past the release date and only includes the code on 
     * the page displayed (what the server gives back to the browser) if the 
     * solutions are supposed to be released.
     *
     * We also use some PHP to create unique IDs for each of the show/hide 
     * buttons and solution divs, which are then used in the PHP generated 
     * jQuery code that we use to create the nice toggling effect.
     *
     * I apologize if the PHP/jQuery is really offensively bad, this is 
     * literally the most I've written of either for a single project so far.
     * Comments/suggestions are most welcome!
     *
     * - Tom Magrino (tmagrino@berkeley.edu)
     */
    $BERKELEY_TZ = new DateTimeZone("America/Los_Angeles");
    $RELEASE_DATE = new DateTime("08/15/2013", $BERKELEY_TZ);
    $CUR_DATE = new DateTime("now", $BERKELEY_TZ);
    $q_num = 0; // Used to make unique ids for all solutions and buttons
    ?>
  </head> 
  
  <body style="font-family: Georgia,serif;"> 

<h1>CS61A Lab 08b: User Interfaces</h1>
<h3>August 14, 2013</h3>

<p>In this lab, you will be creating a Graphical User Interface (GUI)
for the game of Hog. To build the user interface, we'll be using
Tkinter, which is included in Python's standard library. (Tkinter
itself is Python binding for Tk, which is an open source,
cross-platform toolkit for creating GUIs.)</p>

<h3 class="section_title">Introduction</h3>

<p>Create a new file called <span class="code">my_ui.py</span> and type
the following:
</p>
<pre class="codemargin">
from tkinter import *

root = Frame()
root.pack()
root.mainloop()
</pre>

<p>What does this code mean?</p>
<ol>
    <li><span class="code">from tkinter import *</span></li>
    <p>This line imports everything inside of the 
    <span class="code">tkinter</span> library.</p>

    <li><span class="code">root = Frame()</span></li>
    <p>This line creates a <span class="code">Frame</span> object. A 
    <span class="code">Frame</span> is a type of <b>Widget</b>, which 
    is a just visual component of a GUI. Other widgets include entry 
    fields, checkbuttons, and labels. A <span class="code">Frame</span>
    is a special type of widget that just contains other widgets.</p>

    <li><span class="code">root.pack()</span></li>
    <p>This line makes the <span class="code">Frame</span> "visible" to
    the user -- but since <span class="code">Frame</span>s have no 
    content, it still looks like we can't see anything!</p>

    <li><span class="code">root.mainloop()</span></p>
    <p>This line starts the <b>event loop</b>. This loop will
    continuously wait for user input; when the user does something,
    the loop sends a message to components in the GUI. You can program
    the components to respond to the message however you want.</p>
</ol>
<p>Congratulations, you've just created your first GUI! However, if you
run your program with <span class="code">python3 my_ui.py</span>, 
you'll notice that it just creates an empty frame. Not a very 
interesting window yet...</p>

<h3 class="section_title">An Event-Driven Paradigm</h3>
<p>The programs we have been writing so far have all followed a
sequential structure -- we can arrange our code so that it will always
run in a certain order.</p>

<p>UI programs are different. They are instead based on <b>events</b>.
Events are "triggered" by various user inputs, such as mouse clicks,
keyboard inputs, and such. As you might have guessed, the user can
click, type, and move the cursor around in any random way -- and the
programmer won't be able to predict the order in which the events are
generated!</p>

<p>To handle the fact that events could occur in unpredictable 
sequences, we use <b>callback functions</b>. Whenever an event is 
triggered, the event loop notifies the relevant widget(s) and their
callback functions are called.</p>

<p>If that description is still unclear, try typing the following code
in your <span class="code">my_ui.py</span> file:</p>
<pre class="codemargin">
from tkinter import *

root = Frame()
root.pack()
<b>
def handle_button():
    print("Button pushed.")

button = Button(root, text="Press me!", command=handle_button)
button.pack()
root.config(padx=100, pady=100)
</b>
root.mainloop()
</pre>

<p>We've seen the code that's not bolded already. But what does the 
bolded code do?</p>
<ol>
    <li><span class="code">button = Button(root, text="Press me!", 
    command=handle_button)</span></li>
    <p>This line creates a <span class="code">Button</span> widget. A 
    <span class="code">Button</span> is what you expect a button to be 
    -- a clickable widget that does something when you click it. Each 
    argument in the <span class="code">Button</span> constructor means 
    something:</p>
    <ol>
        <li><span class="code">root</span>: the parent widget -- i.e. 
        the widget in which this <span class="code">Button</span> will 
        be contained</li>

        <li><span class="code">text</span>: the text displayed on the 
        <span class="code">Button</span></li>

        <li><span class="code">command</span>: the action that occurs 
        when the <span class="code">Button</span> is pressed</li>
    </ol>
    <p>The <span class="code">command</span> argument will be the 
    <b>callback function</b>, which is just a normal Python function.
    </p>

    <li><span class="code">def handle_button():</span></li>
    <p>These two lines just define the callback function. When the
    button is pressed, it will call this function, which prints
    "button pushed." to your terminal.</p>

    <li><span class="code">button.pack()</span></li>
    <p>This line just makes the <span class="code">Button</span>
    visible. By default, the <span class="code">pack</span> places
    widgets in the center of the parent widget.</p>

    <li><span class='code'>root.config(padx=100, pady=100)</span></li>
    <p>This line denotes how many pixels lie between the button and
    the edge of the main Frame.</p>
</ol>

<p>This is what our program will look like. Try pressing the button!
</p>

<img class="figure" src="images/simple.png" alt="Button UI"/>

<h3 class="section_title">Hog GUI</h3>
<p>For this section, you can copy the starter code by typing this into
your terminal:</p>

<pre class="codemargin">
cp ~cs61a/public_html/su13/lab/lab08b/hog_gui.py .
cp -r ~cs61a/public_html/su13/lab/lab08b/images .
</pre>
<p>Don't forget the "." at the end of the command!</p>

<p>Take a moment to read through the 
<span class="code">hog_gui.py</span>. The first half 
(<span class='code'>HogState</span>) just implements game logic -- 
which should be familiar to you (it's actually all of phase one!).</p>

<p>The second half of the code, the <span class='code'>HogGUI</span>
class, handles the GUI. The code might look a bit intimidating at first,
but let's break it down into parts:</p>
<ol>
    <li><b>Initialization:</b> the primary method is the 
    <span class='code'>__init__</span>, which you know (from OOP) is 
    what the constructor will call. Notice that the 
    <span class='code'>__init__</span> method is further divided
    into "Widget creation" and "Widget packing."</li>
    <ol>
        <li><b>Widget creation</b>: Here, we call widget constructors
        (like <span class="code">Label</span>). We also call three
        "init" helper methods -- don't worry about these, this is so 
        our <span class='code'>__init__</span> isn't too lengthy.</li>
        
        <li><b>Widget packing</b>: to make widgets visible, we have to
        "pack" them. The order in which you pack widgets determines
        their order on the GUI.</li>
    </ol>

    <li><b>Take turn:</b> When the "Roll" button is pressed, it calls
    the <span class='code'>take_turn</span> method. This method has 3
    sections:</li>
    <ol>
        <li>Extracting the number of rolls from the entry field</li>
        <li>Displaying the dice</li>
        <li>Displaying the state of the game and handling "game overs"
        </li>
    </ol>

    <li><b>Display methods</b>: these are helper methods that display
    various things to the screen:</li>
    <ol>
        <li>Display and hide individual dice</li>
        <li>Display state (i.e. display the scores of players</li>
        <li>Display status (e.g. how many points a player scored)</li>
        <li>Display at the end of a game</li>
        <li>Restarting behavior</li>
    </ol>
</ol>

<p>Next, try running the program with</p> 
<pre class="codemargin">
python3 hog_gui.py
</pre>
<p>You'll notice that a simple GUI pops up:</p>

<img class="figure" src="images/hog_before.png" alt="Hog GUI"/>
<p>... but nothing works. Here's where you come in!</p>

<p><strong>Q1: Getting the number of rolls</strong></p>
<p>Right now, the only thing that happens when we click "Roll" is an
error message appears in our terminal -- it complains that 
<span class='code'>num_rolls</span> is not defined!</p>

<p>Your job is to get the numerical value entered in the Entry field
and save it (as an int) in a local variable called
<span class='code'>num_rolls</span>. That way, we will be able to
tell the HogState how many rolls we want to make.</p>

<p>To do this, find the comment that says "Q1: get num_rolls from
self.entry_field" in the <span class='code'>take_turn</span> method.
Here, you will write code dealing with a widget called an <b>Entry</b> 
field. An Entry field is a widget that allows users to input text.</p>

<p>For our GUI, there is an instance variable conveniently named
<span class='code'>self.entry_field</span> that refers to the 
<span class='code'>Entry</span> object next to the "Roll" button. To 
get the text from an Entry field, you can call its 
<span class='code'>get</span> method. It will return the contents of 
the <span class='code'>Entry</span> as a string, so you will have to 
convert it into an int before storing it in 
<span class='code'>num_rolls</span>.</p>

<p>For example, if we have an <span class='code'>Entry</span> object
called <span class='code'>my_entry_field</span>, we can store the 
contents in another variable like this:</p>

<pre class='codemargin'>
content = my_entry_field.get()
</pre>

<p>When you finish this question, you should be able to see status 
messages like "Player 1 scored 2 points" when you enter a number into 
the entry field and click "Roll." Our game works!</p>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
        <pre>
...
def take_turn(self):
    <b>num_rolls = int(self.entry_field.get())</b>
    
    score, outcomes = self.state.take_turn(num_rolls)
    ...
        </pre>
      </p>
    </div>
    <?php } ?>



<p><strong>Q2: Updating scores</strong></p>
<p>Unfortunately, even though we can roll dice and score, our GUI
doesn't display the current game state -- we don't know who's winning!
</p>

<p>To fix this, implement the <span class='code'>display_state</span>
method. This question involves modifying 
<span class='code'>Label</span>s, which are widgets that just display
text. Our GUI has 2 labels called 
<span class='code'>self.p1_label</span> and
<span class='code'>self.p2_label</span> that display scores for each
player.</p>

<p>Every widget has a <span class="code">config</span> method, which
we can use to set a property for the widget. For
<span class='code'>Label</span>s, we can use it to set the text:</p>

<pre class='codemargin'>
my_label.config(text='new text')
</pre>

<p>One last hint: the <span class='code'>HogState</span> class has two
property methods, <span class='code'>p1</span> and 
<span class='code'>p2</span>, that each return the score for their 
respective player. These should be the strings you set for your labels.
</p>

<p>When you finish this question, you should see that the players' 
scores are updating!</p>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
        <pre>
...
def display_state(self):
    <b>self.p1_label.config(text=self.state.p1)
    self.p2_label.config(text=self.state.p2)</b>
    if self.state.p1turn:
        self.p1_label.config(**focused)
        self.p2_label.config(**unfocused)
    else:
        self.p1_label.config(**unfocused)
        self.p2_label.config(**focused)
...
        </pre>
      </p>
    </div>
    <?php } ?>


<p><strong>Q3: Displaying Dice</strong></p>
<p>Let's add more features! Right now, we can't see what outcomes are
being rolled. In the <span class='code'>take_turn</span> method, find
the comment that says "Q3: display or hide dice". Here, you will 
implement code that displays what each player rolls.</p>

<p>Fortunately, we've already created two methods,
<span class='code'>display_dice</span> and 
<span class='code'>hide_dice</span> that can display or hide individal
dice. All you have to do is call each method with the appropriate
<span class='code'>dice_id</span> (e.g. 0 for the first dice) and the 
<span class='code'>outcome</span> (what number came up on that die).
</p>

<p>Notice that, a few lines up in the source code, we have extracted
a dictionary of <span class='code'>outcomes</span> from the
<span class='code'>HogState</span>. The keys of this dictionary are
integers (specifying the dice_id of the dice we should display), and 
the values are the outcomes for those specific dice.</p>

<p>Your code in the <span class='code'>for</span> should look something
like this: for every <span class='code'>dice_id</span> from 0 to 9, you
will do one of two things:</p>
<ul>
    <li>if that <span class='code'>dice_id</span> is in
    <span class='code'>outcomes</span> dictionary, display it.</li>
    <li>else, hide the die.</li>
</ul>

<p>Once you finish this question, you should be able to see dice show
up on your GUI!</p>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
        <pre>
...
def take_turn(self):
    ...
    for dice_id in range(10):
        <b>if dice_id in outcomes.keys():
            self.display_dice(dice_id, outcomes[dice_id])
        else:
            self.hide_dice(dice_id)</b>
    ...
        </pre>
      </p>
    </div>
    <?php } ?>



<p><strong>Q4: Displaying the winner</strong></p>
<p>You might have noticed that the game doesn't stop once we hit our
goal. To fix this, we will have to modify two parts of our program.</p>

<p><b>PART A</b>, go to the <span class='code'>display_end</span> 
method and find the comment that says "Q4: displaying the winner in 
self.status". <span class='code'>self.status</span> is a Label -- but 
you already know (from Q2) how to change the text of a Label! Go ahead 
and add a line that configures the Label to display a message that 
announces the winner.</p>

<p><i>Hint:</i> <span class='code'>self.state</span>, our HogState, 
has a property method called <span class='code'>winner</span>.</p>

<p><b>PART B:</b> the solution to Part A displays a message, but it 
doesn't actually prevent the game from continuing if the user presses 
the "Roll" button. Go to the <span class='code'>take_turn</span> method
and find the comment that says "Q4: terminate the method if the game
is over". Here, you should add a couple of lines to immediately
terminate the method if the game is over.</p>

<p><i>Hint:</i> <span class='code'>self.state</span>, our HogState,
has a property method called <span class='code'>over</span> that is
<span class='code'>True</span> if the game is over.</p>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
        <pre>
...
def take_turn(self):
    <b>if self.state.over:
        return</b>
    ...
...
def display_end(self):
    assert self.state.over, 'Game is not actually over'
    <b>self.status.config(text="%s wins!" %self.state.winner)</b>
...
        </pre>
      </p>
    </div>
    <?php } ?>


<p><strong>Q5: Restart</strong></p>
<p>Finally, we would like to restart the game after it ends. This will
involve creating a new Button widget and registering its callback
method.</p>

<p>To start, go to the <span class='code'>__init__</span> method and
find the comment that says "Q5: initialize restart button". Here, 
create a Button widget by use the <span class='code'>Button</span>
constructor. We have provided a method called
<span class='code'>self.restart</span>, which you should register
to the Restart button as a callback. The parent widget (parent frame)
of the button you create is called <span class='code'>master</span>.</p>

<p>Finally, find the comment that says "Q5: pack restart button". 
Somewhere below that line (not necessarily right below), call the
button's <span class='code'>pack</span> method to make it visible.
Then, try moving the line around to see how this affects where on the
GUI it shows up!</p>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
        <pre>
...
def __init__(self):
    ...
    self.status = Label(master, font=text_font)
    <b>self.restart_button = Button(master, text="Restart", command=self.restart)</b>
    ...
    self.player_frame.pack()
    self.entry_frame.pack()
    self.status.pack()
    <b>self.restart_button.pack()</b> # this can be anywhere, depending on what you think looks pretty
    self.dice_frame1.pack()
    self.dice_frame2.pack()
    ...
        </pre>
      </p>
    </div>
    <?php } ?>


<p>You have now completed your Hog GUI! If you are interested in 
learning more about tkinter, Google it!</p>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Complete Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p><a href="hog_gui_sol.py">Solution here</a></p>
    </div>
    <?php } ?>

<img class="figure" src="images/hog_after.png" alt="Hog GUI"/>


    <?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script>
      <?php for ($i = 0; $i < $q_num; $i++) { ?>
    $("#toggleButton<?php echo $i; ?>").click(function () {
      $("#toggleText<?php echo $i; ?>").toggle();
    });
      <?php } ?>
    </script>
    <?php } ?>
  </body>
</html>
