"""An object-oriented version of Hog with a GUI."""

from random import randrange 
from tkinter import *

##############
# GAME LOGIC #
##############

def make_dice(sides):
    return lambda: randrange(sides) + 1

four_side = make_dice(4)
six_side = make_dice(6)

class HogState:
    def __init__(self, goal=100, p1='Player 1', p2='Player 2'):
        self.p1name, self.p2name = p1, p2
        self.p1turn, self.goal = True, goal
        self.p1score, self.p2score = 0, 0

    def take_turn(self, num_rolls):
        sum_scores = self.p1score + self.p2score
        dice = four_side if sum_scores % 7 == 0 else six_side
        max_rolls = 1 if sum_scores % 10 == 7 else 10

        outcomes, score = {}, 0 if num_rolls else self.free_bacon
        for roll in range(min(max_rolls, num_rolls)):
            outcomes[roll] = dice()
            if outcomes[roll] == 1:
                score = 1
            if score != 1:
                score += outcomes[roll]
        if self.p1turn:
            self.p1score += score
        else:
            self.p2score += score
        if not self.over:
            self.p1turn = not self.p1turn
        return (score, outcomes)

    @property
    def free_bacon(self):
        return self.p2score // 10 + 1 if self.p1turn else self.p1score // 10 + 1

    @property
    def p1(self):
        return 'P1: {} Pts: {}'.format(self.p1name, self.p1score)

    @property
    def p2(self):
        return 'P2: {} Pts: {}'.format(self.p2name, self.p2score)

    @property
    def winner(self):
        if not self.over:
            return None
        return self.p1name if self.p1turn else self.p2name

    @property
    def over(self):
        return max(self.p1score, self.p2score) >= self.goal

##################
# USER INTERFACE #
##################

class HogGUI:
    # YOU ONLY HAVE TO WORK WITH THIS STUFF BELOW #
    # vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv #
    def __init__(self, master, state):
        self.state = state

        self.init_player_labels(master)
        self.init_rolls_fields(master)
        self.init_dice_frame(master)
        self.status = Label(master, font=text_font)
        # Q5: initialize restart button
        
        # Q5: pack restart button somewhere below this line
        self.player_frame.pack()
        self.entry_frame.pack()
        self.status.pack()
        self.dice_frame1.pack()
        self.dice_frame2.pack()

        self.display_state()

    def take_turn(self):
        # Q4: terminate the method if the game is over

        # Q1: get num_rolls from self.entry_field
        
        score, outcomes = self.state.take_turn(num_rolls)
        self.display_status(not self.state.p1turn, score)

        for dice_id in range(10):
            # Q3: display or hide dice
            pass # replace this line with your solution

        self.status.after(500, self.display_state)
        if self.state.over:
            self.display_end()

    def display_dice(self, dice_id, outcome):
        """Helper method to display a single die. The die is specified
        with the DICE_ID, an int from 0 to 9, and a numeric outcome,
        which is an int from 1 to 6."""
        self.dice[dice_id].config(image=imgs[outcome])
        self.dice[dice_id].pack(side=LEFT)

    def hide_dice(self, dice_id):
        """Hides the specified die from the screen. DICE_ID is an int
        from 0 to 9."""
        self.dice[dice_id].pack_forget()

    def display_state(self):
        """Updates the scores of both players at the current state of
        the game."""
        # Q2: updating scores
        
        if self.state.p1turn:
            self.p1_label.config(**focused)
            self.p2_label.config(**unfocused)
        else:
            self.p1_label.config(**unfocused)
            self.p2_label.config(**focused)

    def display_status(self, p1_scored, score):
        """Displays the status (i.e. what a player scored)"""
        msg = '{} scored {} points'
        if p1_scored:
            self.status.config(text=msg.format(self.state.p1name, 
                                               score))
        else:
            self.status.config(text=msg.format(self.state.p2name, 
                                               score))

    def display_end(self):
        """Display the winner of the game in the status bar."""
        assert self.state.over, 'Game is not actually over'
        # Q4: displaying the winner in self.status

    def restart(self):
        """Restarts the game state"""
        for dice in self.dice:
            self.dice[dice].pack_forget()
        self.state = HogState(**game_start)
        self.display_state()
        self.status.config(text='')

    # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ #
    # YOU ONLY HAVE TO WORK WITH THE STUFF ABOVE #

    def init_player_labels(self, master):
        self.player_frame = Frame(master)
        self.p1_label = Label(self.player_frame,
                              font=text_font,
                              borderwidth=5,
                              padx=25,
                              text=str(state.p1))
        self.p2_label = Label(self.player_frame,
                              font=text_font,
                              borderwidth=5,
                              padx=25,
                              text=str(state.p2))
        self.p1_label.pack(side=LEFT)
        self.p2_label.pack(side=LEFT)

    def init_rolls_fields(self, master):
        self.entry_frame = Frame(master)
        self.entry_label = Label(self.entry_frame, 
                                 text='How many rolls?',
                                 font=text_font)
        self.entry_field = Entry(self.entry_frame,
                                 font=text_font,
                                 justify=CENTER)
        self.entry_button = Button(self.entry_frame,
                                   text='Roll',
                                   font=text_font,
                                   bg=button_bg,
                                   command=self.take_turn)
        self.entry_label.pack(side=LEFT)
        self.entry_field.pack(side=LEFT)
        self.entry_button.pack(side=LEFT)

    def init_dice_frame(self, master):
        self.dice_frame1 = Frame(master)
        self.dice_frame2 = Frame(master)
        self.dice = {}
        for i in range(5):
            self.dice[i] = Label(self.dice_frame1)
        for i in range(5, 10):
            self.dice[i] = Label(self.dice_frame2)

root = Tk()
root.title('Hog GUI')
root.minsize(525, 355)

imgs = {1: PhotoImage(file='images/die1.gif'),
        2: PhotoImage(file='images/die2.gif'),
        3: PhotoImage(file='images/die3.gif'),
        4: PhotoImage(file='images/die4.gif'),
        5: PhotoImage(file='images/die5.gif'),
        6: PhotoImage(file='images/die6.gif')}
text_font = ('Helvetica', 14, "bold")
button_bg = '#39AC3E'
focused = {'bg': '#39AC3E', 'relief': RAISED}
unfocused = {'bg': 'light grey', 'relief': FLAT}
game_start = {'goal': 100, 'p1': 'Player 1', 'p2': 'Player 2'}

state = HogState(**game_start)
gui = HogGUI(root, state)

root.mainloop()

