<head>
  <meta name="description" content ="CS61A: Structure and Interpretation of
  Computer Programs" />
  <meta name="keywords" content ="CS61A, Computer Science, CS, 61A, Programming,
  Berkeley, EECS" />
  <meta name="author" content ="Steven Tang, Eric Tzeng, Albert Wu,
  Mark Miyashita, Robert Huang, Andrew Huang, Brian Hou, Leonard Truong,
  Jeffrey Lu, Rohan Chitnis" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <style type="text/css">@import url("../lab_style.css");</style>
  <style type="text/css">@import url("../61a_style.css");</style>
    <title>CS 61A Summer 2013: Lab 7b</title>

    <?php
    /* So all of the PHP in this file is to allow for this nice little trick to
     * help us avoid having two versions of the questions lying around in the
     * repository, which often leads to the two versions going out of sync which
     * leads to annoyance for students.
     *
     * The idea's pretty simple for the PHP part, just simply have two dates:
     *
     *    1. The current date
     *    2. The date the solutions should be released
     *
     * Using these, we now wrap our solutions in a simple PHP if statement that
     * checks if the date is past the release date and only includes the code on
     * the page displayed (what the server gives back to the browser) if the
     * solutions are supposed to be released.
     *
     * We also use some PHP to create unique IDs for each of the show/hide
     * buttons and solution divs, which are then used in the PHP generated
     * jQuery code that we use to create the nice toggling effect.
     *
     * I apologize if the PHP/jQuery is really offensively bad, this is
     * literally the most I've written of either for a single project so far.
     * Comments/suggestions are most welcome!
     *
     * - Tom Magrino (tmagrino@berkeley.edu)
     */
    $BERKELEY_TZ = new DateTimeZone("America/Los_Angeles");
    $RELEASE_DATE = new DateTime("08/08/2013", $BERKELEY_TZ);
    $CUR_DATE = new DateTime("now", $BERKELEY_TZ);
    $q_num = 0; // Used to make unique ids for all solutions and buttons
    ?>
  </head>

  <body style="font-family: Georgia,serif;">

<h1>CS61A Lab 7b: Logic Programming</h1>
<h3>Week 7b, 2013</h3>

<h2 class="section_title">Logic</h2>
<p>
In Declarative Programming, we aim to define <b>facts</b> about our universe.
With these in place, we can make <b>queries</b> in the form of assertions. The
system will then check if the query is true, based on a database of facts. It
will inform us of what replacements for the variables will make the query true.
</p>

<p>
The language we will use is called Logic, and an interpreter is already setup for us
on the lab machines. To copy the folder to your current directory, run:
<pre class="codemargin">
cp -r ~cs61a/lib/lab/lab07b/logic .
</pre>

Just run <span class="code">python3 logic.py</span> after you move your Scheme
project files into the folder. Please note that you must have finished up to
at least problem 4 on your project in order to do this lab, as this lab depends
on your implementation of the <span class="code">Frame</span> class.
</p>

<p>
Let's review the basics. In Logic, the primitive data types are called <b>symbols</b>: these include numbers and
strings. Unlike other languages we have seen in this course, numbers are not evaluated: they
are still symbols, but they do not have their regular numerical meaning. Variables in Logic are
denoted with a <span class="code">?</span> mark preceding the name. So for example, <span class="code">?x</span>
represents the variable <span class="code">x</span>. A <b> relation </b> is a named tuple with a truth value.
</p>

<p>
The next thing we need to do is begin to define <b>facts</b> about our universe Facts are defined using
a combination that starts with the <tt>fact</tt> keyword. The first relation that follows is the
conclusion, and any remaining relations are hypotheses. All hypotheses
must be satisfied for the conclusion to be valid.
</p>

<pre class="codemargin">
logic> (fact (food-chain ?creature1 ?creature2) (eats ?creature1 ?creature3) (eats ?creature3 ?creature2))
</pre>

<p>
Here we have defined the fact for a food chain: If creature1 eats creature3, and creature3
eats creature2, then creature1 is higher on the food chain than creature2.
</p>

<p>
Simple facts contain only a conclusion relation, which is always true.
</p>

<pre class="codemargin">
logic> (fact (eats shark big-fish))
logic> (fact (eats big-fish small-fish))
logic> (fact (eats domo kittens))
logic> (fact (eats kittens small-fish))
logic> (fact (eats zombie brains))
logic> (fact (append (1 2) (3 4) (1 2 3 4)))
</pre>

<p>
Here we have defined a few simple facts: that in our universe, sharks eat
big-fish, big-fish eat small-fish, Domos eat
kittens, kittens eat small-fish, zombies eat brains, and that the list (1 2) appended to (3 4) is equivalent
to the list (1 2 3 4). Poor kittens.
</p>

<p>
Queries are combinations that start with the <tt>query</tt> keyword.
The interpreter prints the truth value (either <tt>Success!</tt> or
<tt>Failed.</tt>). If there are variables inside of the query, the
interpreter will print all possible mappings that satisfy the query.
</p>

<pre class="codemargin">
logic> (query (eats zombie brains))
Success!
logic> (query (eats domo zombie))
Failed.
logic> (query (eats zombie ?what))
Success!
what: brains
</pre>

<p>
We're first asking Logic whether a zombie eats brains (the answer is Success!) and if a domo eats
zombies (the answer is Failed). Then we ask
whether a zombie can eat something (the answer is Success!), and Logic will figure out for
us, based on predefined facts in our universe, what a zombie eats. If there are more
possible values for what a zombie can eat, then Logic will print out all of the possible values.</p>

<h3 class="section_title">Questions</h2>

<p>
1. Within your Logic interactive session, type in the <span class="code">food-chain</span>
fact, and enter in the facts mentioned from above. Issue a Logic query that
answers the following questions:

<ul>
<li>
a.) Do sharks eat big-fish?
</li>
<li>
b.) What animal is higher on the food chain than small-fish?
</li>
<li>
c.) What animals (if any, or multiple) eat small-fish?
</li>
<li>
d.) What animals (if any, or multiple) eat sharks?
</li>
<li>
e.) What animals (if any, or multiple) eat zombies?
</li>
</ul>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
<ul>
<li> <span class="code">logic> (query (eats shark big-fish))</span> </li>
<li> <span class="code">logic> (query (food-chain ?what small-fish))</span></li>
<li> <span class="code">logic> (query (eats ?what small-fish))</span></li>
<li> <span class="code">logic> (query (eats ?what sharks))</span></li>
<li> <span class="code">logic> (query (eats ?what zombie))</span></li>
</ul>
    </div>
<?php } ?>
</p>

<h3 class="section_title">More complicated facts</h3>

<p>
Currently, the <span class="code">food-chain</span> fact is a little lacking.
A query <span class="code">(query (food-chain A B))</span> will only output <span class="code">Success!</span>
if <span class="code">A</span> and <span class="code">B</span> are separated by only one animal.
For instance, if I added the following facts:

<pre class="codemargin">
logic> (fact (eats shark big-fish))
logic> (fact (eats big-fish small-fish))
logic> (fact (eats small-fish shrimp))
</pre>

I'd like the <span class="code">food-chain</span> to output that <span class="code">shark</span>
is higher on the food chain than <span class="code">shrimp</span>. Currently, the
<span class="code">food-chain</span> fact doesn't do this: </p>

<pre class="codemargin">
logic> (query (food-chain shark shrimp))
Failed
</pre>

<p>We will define the
<span class="code">food-chain-v2</span> fact that correctly handles arbitrary length
hierarchies. We'll use the following logic: </p>

<ul> Given animals <span class="code">A</span> and <span class="code">B</span>, <span class="code">A</span> is on
top of the food chain of <span class="code">B</span> if:
<ul>
  <li> - <span class="code">A</span> eats <span class="code">B</span></li>
  <li> or </li>
  <li> - There exists an animal <span class="code">C</span> such that <span class="code">A</span>
    dominates <span class="code">C</span>, and <span class="code">C</span> dominates <span class="code">B</span>.
  </li>
</ul>
</ul>

<p>
Notice we have two different cases for the <span class="code">food-chain-v2</span> fact.
We can express different cases of a fact simply by entering in each case
one at a time: </p>

<pre class="codemargin">
logic> (fact (food-chain-v2 ?a ?b) (eats ?a ?b))
logic> (fact (food-chain-v2 ?a ?b) (food-chain-v2 ?a ?c) (food-chain-v2 ?c ?b))
logic> (query (food-chain-v2 shark shrimp))
Success!
</pre>

<p>
Take a few moments and read through how the above facts work,
and how it implements the approach we outlined. In particular, make a
few queries to <span class="code">food-chain-v2</span> -- for instance, try retrieving
all animals that dominate shrimp!</p>

<p>
<u>Note:</u> In the Logic system, multiple 'definitions' of a fact can
exist at the same time (as in <span class="code">food-chain-v2</span>) - definitions
don't overwrite each other. </p>

<h3 class="section_title">Recursively-Defined Rules</h3>

<p>
Next, we will define <span class="code">append</span> in the
logic style.
</p>

<p>
As we've done in the past, let's try to explain how <span class="code">append</span>
recursively. For instance, given two lists <span class="code">[1, 2, 3]</span>,
<span class="code">[5, 7]</span>, the result of <span class="code">append([1, 2, 3], [5, 7])</span>
is:
<ul>
  <li><span class="code">[1] + append([2, 3], [5, 7]) => [1, 2, 3, 5, 7] </span></li>
</ul>

In Scheme, this would look like:

<pre class="codemargin">
(define (append a b) (if (null? a) b (cons (car a) (append (cdr a) b))))
</pre>

Thus, we've broken up <span class="code">append</span> into two different cases.
Let's start translating this idea into Logic! The first base case
is relatively straightforward:

<pre class="codemargin">
logic> (fact (append () ?b ?b))
logic> (query (append () (1 2 3) ?what))
Success!
what: (1 2 3)
</pre>

So far so good! Now, we have to handle the general (recursive) case:

<pre class="codemargin">
logic> (fact (append (?car . ?cdr) ?b (?car . ?partial)) (append ?cdr ?b ?partial))
</pre>

This translates to: the list <span class="code">A</span> appended to <span class="code">B</span>
is <span class="code">C</span> if <span class="code">C</span> is the result of sticking the
<span class="code">CAR</span> of <span class="code">A</span> to the result of appending the
<span class="code">CDR</span> of <span class="code">A</span> to <span class="code">B</span>. Do you see how
the Logic code corresponds to the recursive case of the Scheme function
definition? As a summary, here is the complete definition for <span class="code">append</span>:

<pre class="codemargin">
logic> (fact (append () ?b ?b ))
logic> (fact (append (?a . ?r) ?y (?a . ?z)) (append ?r ?y ?z))
</pre>
</p>

<p>
If it helps you, here's an alternate solution that might be a little
easier to read:

<pre class="codemargin">
logic> (fact (car (?car . ?cdr) ?car))
logic> (fact (cdr (?car . ?cdr) ?cdr))
logic> (fact (append () ?b ?b))
logic> (fact (append ?a ?b (?car-a . ?partial)) (car ?a ?car-a) (cdr ?a ?cdr-a) (append ?cdr-a ?b ?partial))
</pre>

Meditate on why this more-verbose solution is equivalent to the first
definition for the <span class="code">append</span> fact.
</p>


<h3 class="section_title">Exercises</h3>

<p>
1. Using the <span class="code">append</span> fact, issue the following queries,
and ruminate on the outputs. Note that some of these queries might
result in multiple possible outputs.

<pre class="codemargin">
logic> (query (append (1 2 3) (4 5) (1 2 3 4 5)))
logic> (query (append (1 2) (5 8) ?what))
logic> (query (append (a b c) ?what (a b c oh mai gawd)))
logic> (query (append ?what (so cool) (this is so cool)))
logic> (query (append ?what1 ?what2 (will this really work)))
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
Try it out, type it in the interpreter!
    </div>
<?php } ?>

<p>
2. Define a fact <span class="code">(fact (last-element ?lst ?x))</span> that outputs <span class="code">Success</span>
if <span class="code">?x</span> is the last element of the input list <span class="code">?lst</span>.
Check your facts on queries such as:
<pre class="codemargin">
logic> (query (last-element (a b c) c))
logic> (query (last-element (3) ?x))
logic> (query (last-element (1 2 3) ?x))
logic> (query (last-element (2 ?x) (3)))
</pre>

Does your solution work correctly on queries such as <span class="code">(query (last-element ?x (3)))</span>?
Why or why not?
</p>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
<pre class"codemargin">
(fact (last-element (?x) ?x))
(fact (last-element (?car . ?cdr) ?x) (last-element ?cdr ?x))
</pre>
    </div>
<?php } ?>

<p>
3. Define the fact <span class="code">(fact (contains ?elem ?lst))</span> that outputs
<span class="code">Success</span> if the <span class="code">?elem</span> is contained inside of
the input <span class="code">?lst</span>:

<pre class="codemargin">
logic> (query (contains 42 (1 2 42 5)))
Success.
logic> (query (contains (1 2) (a b (1) (1 2) bye)))
Success.
logic> (query (contains foo (bar baz garply)))
Failed.
</pre>

</p>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
<pre class"codemargin">
(fact (contains ?elem (?elem . ?cdr)))
(fact (contains ?elem (?car . ?cdr)) (contains ?elem ?cdr))
</pre>
    </div>
<?php } ?>


<!--
<p>
1. We said that everything in PyGic is a symbol (a string). But we would ideally like
to somehow represent numbers in our universe. HOW IS THIS POSSIBLE?!?!?!?!??! FIND
OUT SOON, IN THE NEXT INSTALLMENT OF PYGIC (now moved to Wednesday).
</p>
-->

    <?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script>
      <?php for ($i = 0; $i < $q_num; $i++) { ?>
    $("#toggleButton<?php echo $i; ?>").click(function () {
      $("#toggleText<?php echo $i; ?>").toggle();
    });
      <?php } ?>
    </script>
    <?php } ?>
  </body>
</html>
