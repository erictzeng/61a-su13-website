#This is just a handy function that clears the screen on windows.
import os
clear = lambda: os.system('cls')

def account(balance):
    """Return an account that is represented as a dispatch dictionary."""

    def withdraw(amount):
        if amount > dispatch['balance']:
            return 'Insufficient funds'
        dispatch['balance'] -= amount
        return dispatch['balance']

    def deposit(amount):
        dispatch['balance'] += amount
        return dispatch['balance']

    dispatch = {'balance': balance, 'withdraw': withdraw, 'deposit': deposit}

    return dispatch

class Account(object):
    """A bank account has a balance.
    """

    interest = 0.02

    def __init__(self, starting_balance):
        self.balance = starting_balance

    def deposit(self, amount):
        """Add amount to balance."""
        self.balance = self.balance + amount
        return self.balance

    def withdraw(self, amount):
        """Subtract amount from balance if funds are available."""
        if amount > self.balance:
            return 'Insufficient funds'
        self.balance = self.balance - amount
        return self.balance