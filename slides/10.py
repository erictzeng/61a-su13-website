# nonlocal used for local state

def make_withdraw(balance):
    """Return a withdraw function with a starting balance."""
    def withdraw(amount):
        nonlocal balance
        if amount > balance:
            return 'Insufficient funds'
        balance = balance - amount
        return balance
    return withdraw

# using a list instead of a nonlocal variable

def make_withdraw_list(balance):
    """Return a withdraw function with a starting balance."""
    b = [balance]
    def withdraw(amount):
        if amount > b[0]:
            return 'Insufficient funds'
        b[0] = b[0] - amount
        return b[0]
    return withdraw

# container using nonlocal

def container(contents):
    """Return a container that is manipulated by two functions.

    >>> get, put = container('hello')
    >>> get()
    'hello'
    >>> put('world')
    >>> get()
    'world'
    """
    def get():
        return contents

    def put(value):
        nonlocal contents
        contents = value

    return put, get

# container using message passing

def container_mp(contents):
    """Return a container that is manipulated by two functions.

    >>> get, put = container('hello')
    >>> get()
    'hello'
    >>> put('world')
    >>> get()
    'world'
    """
    def dispatch(message, value=None):
        nonlocal contents
        if message == 'get':
            return contents
        elif message == 'put':
            contents = value
    return dispatch

# mutable rlist using message passing

empty_rlist = None

def make_rlist(first, rest):
    return first, rest

def first(rlist):
    return rlist[0]

def rest(rlist):
    return rlist[1]

def len_rlist(rlist):
    if rlist is empty_rlist:
        return 0
    return 1 + len_rlist(rest(rlist))

def getitem_rlist(rlist, i):
    if i == 0:
        return first(rlist)
    return getitem_rlist(rest(rlist), i - 1)

def rlist_to_tuple(rlist):
    if rlist is empty_rlist:
        return ()
    return (first(rlist),) + rlist_to_tuple(rest(rlist))

def str_rlist(rlist):
    return '<rlist ' + str(rlist_to_tuple(rlist)) + '>'

def mutable_rlist():
    contents = empty_rlist
    def dispatch(message, value=None):
        nonlocal contents
        if message == 'len':
            return len_rlist(contents)
        elif message == 'getitem':
            return getitem_rlist(contents, value)
        elif message == 'push':
            contents = make_rlist(value, contents)
        elif message == 'pop':
            item = first(contents)
            contents = rest(contents)
            return item
        elif message == 'str':
            return str_rlist(contents)
    return dispatch

# implementing accounts using a dispatch dictionary

def account(balance):
    """Return an account that is represented as a dispatch dictionary."""

    def withdraw(amount):
        if amount > dispatch['balance']:
            return 'Insufficient funds'
        dispatch['balance'] -= amount
        return dispatch['balance']

    def deposit(amount):
        dispatch['balance'] += amount
        return dispatch['balance']

    dispatch = {'balance': balance, 'withdraw': withdraw, 'deposit': deposit}

    return dispatch
