# factorial
def factorial(n):
    if n == 0 or n == 1:
        return 1
    return n * factorial(n - 1)

# piglatin
def vowel(letter):
    return letter in 'aeiou'

def piglatin(word):
    if vowel(word[0]):
        return word + 'ay'
    return piglatin(word[1:] + word[0])

# reverse
def reverse(s):
    if not s:
        return s
    return reverse(s[1:]) + s[0]

# fibonacci
def fib(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    return fib(n - 1) + fib(n - 2)

# counting change
def next_coin(coin):
    if coin == 50:
        return 25
    elif coin == 25:
        return 10
    elif coin == 10:
        return 5
    elif coin == 5:
        return 1
    return 0

def count_change(amt, coin):
    if amt == 0:
        return 1
    elif amt < 0 or coin == 0:
        return 0
    return count_change(amt - coin, coin) + count_change(amt, next_coin(coin))
