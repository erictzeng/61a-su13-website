from math import ceil

class Range(object):
    def __init__(self, start, end=None, step=1):
        if end is None:
            start, end = 0, start
        self.start = start
        self.end = end
        self.step = step

    def __len__(self):
        return max(0, ceil(self.end - self.start) / self.step)

    def __getitem__(self, k):
        if k < 0:
            k = len(self) + k
        if k < 0 or k >= len(self):
            raise IndexError('index out of range')
        return self.start + k * self.step


class RangeIter(object):
    def __init__(self, start, end, step):
        self.current = start
        self.end = end
        self.step = step
        self.sign = 1 if step > 0 else -1

    def __next__(self):
        if self.current * self.sign >= self.end * self.sign:
            raise StopIteration
        result = self.current
        self.current += self.step
        return result

    def __iter__(self):
        return self


class FibIter(object):
    def __init__(self):
        self.prev = -1
        self.current = 1

    def __next__(self):
        self.prev, self.current = self.current, self.prev + self.current
        return self.current

    def __iter__(self):
        return self
