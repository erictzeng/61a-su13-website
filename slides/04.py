# make_adder

def make_adder(n):
    def adder(k):
        return n + k
    return adder

# Can bind the result of make_adder to a variable, then use that variable as a
# function
add_three = make_adder(3)
add_three(4)      # 7

# Or we can use the result of make_adder directly
make_adder(5)(4)  # 9

# compose1

def square(x):
    return x * x

def compose1(f, g):
    """
    Takes two functions, f and g, and returns a new function h such that
    applying h to a value returns the same result as applying g, then f in
    sequence to that value.

    >>> h = compose1(square, make_adder(3))
    >>> h(1)
    16
    """
    def h(x):
        return f(g(x))
    return h

h = compose1(square, make_adder(3))
h(3)  # 16
