class Account(object):
    """A bank account has a balance.
    """

    interest = 0.02

    def __init__(self, starting_balance):
        self.balance = starting_balance

    def deposit(self, amount):
        """Add amount to balance."""
        self.balance = self.balance + amount
        return self.balance

    def withdraw(self, amount):
        """Subtract amount from balance if funds are available."""
        if amount > self.balance:
            return 'Insufficient funds'
        self.balance = self.balance - amount
        return self.balance


class CheckingAccount(Account):
    """A bank account that charges for withdrawals."""

    withdraw_fee = 1
    interest = 0.01

    def withdraw(self, amount):
        return Account.withdraw(self, amount + self.withdraw_fee)


class Dog(object):

    def __init__(self, name):
        self.name = name
        self.hunger = 0

    def speak(self):
        self.hunger += 1
        return 'woof'

    def eat(self):
        self.hunger -= 1


class Collie(Dog):

    def speak(self):
        Dog.speak(self)
        return 'there is a boy trapped in the well'

    def eat(self):
        Dog.eat(self)
        return 'this food is exquisite'
