from operator import floordiv, mod

#This is just a handy function that clears the screen on windows.
import os
clear = lambda: os.system('cls')

def divide_exact(a, b):
	"""Return the quotient and remainder from dividing a by b

	>>> quotient, remainder = divide_exact(2013, 10)
	>>> quotient
	201
	>>> remainder
	3
	"""
	#comments 
	return floordiv(a, b), mod(a, b)

def greet(age = 50, name = 'eric'):
	print('hello, it is nice to see you ' + name + " and you are " + str(age) + " years old.")

def shape_area(r, shape_constant):
	return shape_constant * pow(r, 2)

def square_area(r):
	return shape_area(r, 1)

def circle_area(r):
	return shape_area(r, 3.142)

def identity(k):
	return k

def cube(k):
	return pow(k, 3)

def sum_integers(n):
	"""Return the sum of the integers from 1 through n"""
	total, k = 0, 1
	while k <= n:
		total = total + identity(k)
		k = k + 1
	return total

def sum_cubes(n):
	"""Return the sum of the cubes from 1 through n

	Arguments:
	n is a number
	"""
	total, k = 0, 1
	while k <= n:
		total = total + cube(k)
		k = k + 1
	return total

def summation(n, term):
	total, k = 0, 1
	while k <= n:
		total = total + term(k)
		k = k + 1
	return total

from operator import add
def make_adder(n):
	def adder(k):
		return add(n, k)
	return adder