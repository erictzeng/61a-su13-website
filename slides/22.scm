; normal and tail-recursive factorial

(define (factorial n)
  (if (= n 0)
      1
      (* n (factorial (- n 1)))))

(define (factorial-tail n)
  (define (factorial-iter n total)
    (if (= n 0)
        total
        (factorial-iter (- n 1) (* n total))))
  (factorial-iter n 1))

; normal and tail-recursive length

(define (length s)
  (if (null? s)
      0
      (+ 1 (length (cdr s)))))

(define (length-tail s)
  (define (length-iter s n)
    (if (null? s)
        n
        (length-iter (cdr s) (+ 1 n))))
  (length-iter s 0))

; tail-recursive reduce

(define (reduce procedure s start)
  (if (null? s)
      start
      (reduce procedure (cdr s) (procedure start (car s)))))

; tail-recursive map

(define (map procedure s)
  (define (map-iter procedure s m)
    (if (null? s)
        m
        (map-iter procedure
                  (cdr s)
                  (cons (procedure (car s)) m))))
  (reverse (map-iter procedure s nil)))

(define (reverse s)
  (define (reverse-iter s r)
    (if (null? s)
        r
        (reverse-iter (cdr s)
                      (cons (car s) r))))
  (reverse-iter s nil))

; Two functions, but one is tail recursive and one isn't! Which one crashes STk?

(define (boom) (+ 1 (boom)))

(define (not-boom) (not-boom))
